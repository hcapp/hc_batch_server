package com.hubciti.pushnotification.common.pojo;

import java.util.List;

public class Data {
	
	private List<RSSFeedMessage> rssFeedList;
	
	private String link;
	
	private List<DeviceId> deviceIdList;
	
	private String notiMgs;
	
	/**
	 * 
	 * @return get News List
	 */
	public List<RSSFeedMessage> getRssFeedList() {
		return rssFeedList;
	}

	/**
	 * 
	 * @param rssFeedList to set
	 */
	public void setRssFeedList(List<RSSFeedMessage> rssFeedList) {
		this.rssFeedList = rssFeedList;
	}
	
	/**
	 * 
	 * @return get Link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * 
	 * @param link set Link
	 */
	public void setLink(String link) {
		this.link = link;
	}
	
	/**
	 * 
	 * @return get android and iOS device Details
	 * 
	 */
	public List<DeviceId> getDeviceIdList() {
		return deviceIdList;
	}
	
	/**
	 * 
	 * @param deviceIdList to set.
	 */
	public void setDeviceIdList(List<DeviceId> deviceIdList) {
		this.deviceIdList = deviceIdList;
	}
	
	/**
	 * 
	 * @return get Notification Message.
	 */
	public String getNotiMgs() {
		return notiMgs;
	}

	/**
	 * 
	 * @param notiMgs to set
	 */
	public void setNotiMgs(String notiMgs) {
		this.notiMgs = notiMgs;
	}

}
