package com.hubciti.pushnotification.common.pojo;

/**
 * 
 * This instance used for holding data
 * 
 * @author kirankumar.garaddi
 *
 */
public class Configuration {

	private String configurationType;

	private String screenContent;

	/**
	 * 
	 * @return get Property Name
	 */
	public String getConfigurationType() {
		return configurationType;
	}

	/**
	 * 
	 * @param configurationType
	 *            set Configuration Property Name
	 */
	public void setConfigurationType(String configurationType) {
		this.configurationType = configurationType;
	}

	/**
	 * 
	 * @return the screen content.
	 * 
	 */
	public String getScreenContent() {
		return screenContent;
	}

	/**
	 * 
	 * @param screenContent
	 *            to fetch.
	 */
	public void setScreenContent(String screenContent) {
		this.screenContent = screenContent;
	}

}
