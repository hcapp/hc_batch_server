package com.hubciti.pushnotification.common.helper;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import com.hubciti.pushnotification.common.pojo.RSSFeed;

/**
 * Class to parse RSS feed for news.
 * 
 * @author dhruvanath_mm
 *
 */
public class RSSFeedParser {

	static final String TITLE = "title";
	static final String LINK = "link";
	static final String PUB_DATE = "pubDate";
	static final String DESCRIPTION = "description";
	static final String ITEM = "item";
	static final String MEDIA_TEXT = "text";
	static final String MEDIA_CONTENT = "content";

	private URL url;

	public RSSFeedParser(String feedUrl) {
		try {
			this.url = new URL(feedUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public RSSFeed readFeed() {
		RSSFeed feed = null;
		return feed;
	}

	private String getCharacterData(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException {
		String result = "";
		event = eventReader.nextEvent();
		if (event instanceof Characters) {
			result = event.asCharacters().getData();
		}
		return result;
	}

	private InputStream read() {
		try {
			return url.openStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
