package com.hubciti.pushnotification.common.helper;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hubciti.pushnotification.common.pojo.Data;
import com.hubciti.pushnotification.common.pojo.NotificationDetails;
import com.hubciti.pushnotification.common.pojo.RSSFeedMessage;
import com.hubciti.pushnotification.service.PushNotificationHelper;

/**
 * 
 * @author kirankumar.garaddi
 *
 */
public class Main {

	public static void main(String[] args) {
		
		String iPhoneAlertMsg = "here is the ios body breaking news";
		String payload = null;
		String jsonData = null;
		
		List<RSSFeedMessage> messages = new ArrayList<RSSFeedMessage>();
		RSSFeedMessage msg = new RSSFeedMessage();
		
		
		msg.setType("Breaking News");
		msg.setLink("https://www.scansee.net/TylerNotification/pushhome.htm?feedType=Breaking News");
		messages.add(msg);
		
		Data setData = new Data();
		setData.setRssFeedList(messages);
		
		NotificationDetails notiDetails = new NotificationDetails();
		notiDetails.setData(setData);
		
		Data dt = PushNotificationHelper.getNotificationData(notiDetails, "Tyler", Constants.PLATFORM_IOS);
		
		dt.setNotiMgs(null);
		
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		
		jsonData = gson.toJson(dt);
		
		payload = new PayLoadGenerator(1, jsonData, iPhoneAlertMsg).getPayload();
		
		System.out.println(payload);
		
	}

}
