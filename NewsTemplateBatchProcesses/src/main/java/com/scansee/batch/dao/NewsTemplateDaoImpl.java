package com.scansee.batch.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.scansee.batch.common.ApplicationConstants;

import com.scansee.batch.common.PropertiesReader;
import com.scansee.batch.exception.NewsTemplateBatchProcessesException;
import com.scansee.batch.pojo.Item;

/**
 * Insert data to staging table and main table, and send email to respective
 * user.
 * 
 * @author vaidehi.ne
 */

public class NewsTemplateDaoImpl implements NewsTemplateDao {
	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(NewsTemplateDaoImpl.class.getName());
	/**
	 * Variable for jdbcTemplate.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get database connection.
	 */
	public void setDataSource(DataSource dataSource) {

		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * Below method is used to insert rss news feed data to staging table.
	 * 
	 */
	public String insertData(List<Item> itemList, String newsType, String subcategory, String hubcitiId) throws NewsTemplateBatchProcessesException {
		LOG.info("Inside DAO Method Process Start @:" + Calendar.getInstance().getTime());

		String Response = null;
		Object[] values = null;
		String methodName = "insertData";
		List<Object[]> itemBatchList = new ArrayList<Object[]>();
		int[] updateCounts = { 0 };

		try {
			if (null != itemList && !itemList.isEmpty()) {
				for (Item items : itemList) {
					values = new Object[] { items.getTitle(), items.getImage(), items.getShortDesc(), items.getDescription(), items.getLink(), items.getDate(),
							newsType, items.getMessage(), hubcitiId, items.getAdcopy(), items.getSection(), items.getClassification(), items.getAuthor(),
							items.getThumbnail(), subcategory, items.getTime(), items.getVideoLink() };
					itemBatchList.add(values);
				}
				updateCounts = jdbcTemplate
						.batchUpdate(
								"INSERT INTO RssNewsFirstFeedNewsStagingTable(Title, ImagePath, ShortDescription, LongDescription, Link,PublishedDate, NewsType, Message, HcHubCitiID, Adcopy, Section, Classification, author, thumbnail, subcategory, PublishedTime, VideoLink) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
								itemBatchList);
				if (updateCounts.length > 0) {
					Response = ApplicationConstants.SUCCESS;
					LOG.info("News Template batch process excecuted successfully" + "News type :" + newsType + "HubCitiID : " + hubcitiId);
				} else {
					Response = ApplicationConstants.FAILURE;
					LOG.info("News Template batch process excecution failed " + "News type :" + newsType + "HubCitiID : " + hubcitiId);
				}
			} else {
				Response = ApplicationConstants.EMPTYLIST;
			}
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new NewsTemplateBatchProcessesException(exception);
		}
		LOG.info("Inside DAO Method Process Ends  @:" + Calendar.getInstance().getTime());
		return Response;
	}

	/**
	 * Below method is used to move data from staging table to main table.
	 * 
	 */
	public String NewsFeedPorting() throws NewsTemplateBatchProcessesException {
		final String methodName = "NewsFeedPorting";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Boolean result = null;
		String response = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRssNewsFirstFeedNewsInsertion");

			MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			result = (Boolean) resultFromProcedure.get("Status");
			if (null != result && result == false) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + ApplicationConstants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		} catch (Exception exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
		}
		return response;

	}

	/**
	 * The DAO method for sending list of items with empty information email
	 * details to database.
	 * 
	 * @return List of items with empty New(s) Feed(s) information.
	 */
	@SuppressWarnings("unchecked")
	public List<Item> emptyNewsListByEmail() throws NewsTemplateBatchProcessesException {
		LOG.info("Inside NewsTemplateDAOImpl : emptyNewsListByEmail ");
		List<Item> emptyItemsList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchRssNewsFirstFeedEmailNotification");
			simpleJdbcCall.returningResultSet("emptyItemsList",

			new BeanPropertyRowMapper<Item>(Item.class));
			final MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue("HcHubCitiID", PropertiesReader.getPropertyValue(ApplicationConstants.HUBCITI_ID));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(map);
			final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
			final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");

			if (null != resultFromProcedure) {
				if (null == errorNum) {
					emptyItemsList = (ArrayList<Item>) resultFromProcedure.get("emptyItemsList");
				} else {
					LOG.info("Inside NewsTemplateDAOImpl : emptyNewsListByEmail : " + errorNum + " errorMsg " + errorMsg);
				}

			}
		} catch (DataAccessException e) {
			LOG.error("Inside NewsTemplateDAOImpl : emptyNewsListByEmail : " + e);
			e.printStackTrace();
		}
		LOG.info("Exit NewsTemplateDAOImpl : emptyItemsListByEmail ");
		return emptyItemsList;
	}

	/**
	 * The DAO method to get HubCitiId, category and URL
	 * 
	 * @return HubCitiId, HubCitiName, categoryId, categoryName and URL
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Item> getHubitiInfo() throws NewsTemplateBatchProcessesException {
		final String methodName = "getHubitiInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Item> hubCitiList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_NewsFirstHubcitiCategory");
			MapSqlParameterSource feed = new MapSqlParameterSource();
			simpleJdbcCall.returningResultSet("hubcitilst", new BeanPropertyRowMapper<Item>(Item.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(feed);
			final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
			final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

			if (null != resultFromProcedure) {
				if (null == errorNum) {
					hubCitiList = (ArrayList<Item>) resultFromProcedure.get("hubcitilst");
				} else {
					LOG.info("Inside  : getHubitiInfo : " + errorNum + "errorMsg  .." + errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.info("Inside NewsTemplateDAOImpl : getHubitiInfo : " + e);
			e.printStackTrace();
		}
		LOG.info("Exit getHubitiInfo : NewsTemplateBatchProcesss ");

		return hubCitiList;
	}

	/**
	 * The DAO method to delete history from staging table
	 * 
	 */

	public String deleteFeedData() throws NewsTemplateBatchProcessesException {
		LOG.info("Inside NewsTemplateDAOImpl : deleteFeedData ");
		String strResponse = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRssNewsFirstFeedNewsDeletion");

			final MapSqlParameterSource map = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(map);
			final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
			final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");

			if (null != resultFromProcedure) {
				if (null == errorNum) {
					strResponse = ApplicationConstants.SUCCESS;
				} else {
					strResponse = ApplicationConstants.FAILURE;
					LOG.error("Inside NewsTemplateDAOImpl : deleteFeedData : " + errorNum + " errorMsg " + errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.error("Inside NewsTemplateDAOImpl : deleteFeedData : " + e);
			e.printStackTrace();
		}
		LOG.info("Exit NewsTemplateDAOImpl : deleteFeedData ");
		return strResponse;
	}
}