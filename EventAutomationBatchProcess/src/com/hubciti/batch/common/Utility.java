package com.hubciti.batch.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.hubciti.batch.common.pojos.BatchProcessStatus;
import com.hubciti.batch.common.pojos.Event;
import com.hubciti.batch.exception.EventAutomationBatchProcessException;

public class Utility
{

	private static final Logger LOG = Logger.getLogger(Utility.class);
	
	public static String getLatestEventFileFromFtp() throws EventAutomationBatchProcessException {

		final String methodName = "getLatestEventFileFromFtp";
		LOG.info(Constants.METHODSTART + methodName);
		
		FTPClient client = new FTPClient();
		FileOutputStream fos = null;
		ArrayList<FTPFile> ftpFiles = new ArrayList<FTPFile>();
		String downloadPath = null;
		FTPFile latestFile = null;
		String fileName = null;
		try {
			// Get FTP connectio
			client = getFTPConnection();
			// Change directory to active files directory
			client.changeWorkingDirectory(PropertiesReader.getPropertyValue("eventftppath"));
			client.enterLocalPassiveMode();
			
			FTPFile[] files = client.listFiles();

			// Extract only xlsx files
			for (FTPFile ftpFile : files) {
				String filename = ftpFile.getName();
				if (FilenameUtils.isExtension(filename, "xlsx")) {
					ftpFiles.add(ftpFile);
				}
			}

			// Get the latest xlsx file
			if (ftpFiles != null & !ftpFiles.isEmpty()) {
				Date lastMod = ftpFiles.get(0).getTimestamp().getTime();
				latestFile = ftpFiles.get(0);

				for (FTPFile ftpFile : ftpFiles) {
					if (ftpFile.getTimestamp().getTime().after(lastMod)) {
						latestFile = ftpFile;
						lastMod = ftpFile.getTimestamp().getTime();
					}
				}
				// Download the latest file local
				if (null != latestFile) {
					downloadPath = PropertiesReader.getPropertyValue("eventdownloadpath");
					fileName = latestFile.getName();
					File obj = new File(downloadPath);
					if (!obj.exists()) {
						obj.mkdir();
					}
					fos = new FileOutputStream(downloadPath + fileName);
					client.retrieveFile(fileName, fos);
				}
			}
		} catch (SocketException exception) {
			LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new EventAutomationBatchProcessException(exception);
		} catch (IOException exception) {
			LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new EventAutomationBatchProcessException(exception);
		}
		
		finally {
			try {
				client.disconnect();
			} catch (IOException exception) {
				LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception);
				throw new EventAutomationBatchProcessException(exception);
			}
		}

		LOG.info(Constants.METHODEND + methodName);
		return fileName;

	}
	
	public static FTPClient getFTPConnection() throws EventAutomationBatchProcessException {
		
		final String methodName = "getLatestEventFileFromFtp";
		LOG.info(Constants.METHODSTART + methodName);
		
		FTPClient client = new FTPClient();
		int reply;
		
		try {
			client.connect(PropertiesReader.getPropertyValue("ftp_url"));
			client.login(PropertiesReader.getPropertyValue("ftp_username"), PropertiesReader.getPropertyValue("ftp_password"));
			// After connection attempt, you should check the reply code to
			// verify
			// success.
			reply = client.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply)) {
				client.disconnect();
				LOG.error("****FTP server refused connection******");
				System.exit(1);
			} else {
				LOG.info("***FTP connection Established*****");
				client.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
				client.setFileTransferMode(FTP.BINARY_FILE_TYPE);

			}
		} catch (SocketException exception) {
			LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception.getMessage());
			throw new EventAutomationBatchProcessException(exception);
		} catch (IOException exception) {
			LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception.getMessage());
			throw new EventAutomationBatchProcessException(exception);
		}
		
		LOG.info(Constants.METHODEND + methodName);
		return client;
	}

	@SuppressWarnings("rawtypes")
	public static ArrayList<Event> getEventFromXLSXFile(InputStream inputStream) throws EventAutomationBatchProcessException {
		
		final String methodName = "getEventFromXLSXFile";
		LOG.info(Constants.METHODSTART + methodName);
		
		XSSFSheet sheet = null;		
		ArrayList<Event> events = new ArrayList<Event>();
		
		try {
			
			XSSFWorkbook wb_xssf = new XSSFWorkbook(inputStream);
			sheet = wb_xssf.getSheetAt(0);
			Iterator rowItr = sheet.rowIterator();

			while (rowItr.hasNext()) {
				XSSFRow row = (XSSFRow) rowItr.next();

				Event event = new Event();
				event.setHubCitiName(((row.getCell(0)) != null && !row.getCell(0).toString().equals("")) ? row.getCell(0).toString() : null);
				event.setEventName(((row.getCell(1)) != null && !row.getCell(1).toString().equals("")) ? row.getCell(1).toString() : null);
				event.setShortDescription(((row.getCell(2)) != null && !row.getCell(2).toString().equals("")) ? row.getCell(2).toString() : null);
				event.setLongDescription(((row.getCell(3)) != null && !row.getCell(3).toString().equals("")) ? row.getCell(3).toString() : null);
				event.setImagePath(((row.getCell(4)) != null && !row.getCell(4).toString().equals("")) ? row.getCell(4).toString() : null);
				event.setListImagePath(((row.getCell(5)) != null && !row.getCell(4).toString().equals("")) ? row.getCell(5).toString() : null);
				
				
				if (row.getRowNum() != 0) {
					event.setStartDate((row.getCell(6) != null && row.getCell(6).getDateCellValue() != null) ? formattedDate(row.getCell(6).getDateCellValue().toString()) : null);
					event.setEndDate((row.getCell(7) != null && row.getCell(7).getDateCellValue() != null) ? formattedDate(row.getCell(7).getDateCellValue().toString()) : null);
				} else {
					event.setStartDate((row.getCell(6) != null) ? row.getCell(6).toString() : null);
					event.setEndDate((row.getCell(7) != null) ? row.getCell(7).toString() : null);

				}
				
				event.setMoreInformationURL(((row.getCell(8)) != null && !row.getCell(8).toString().equals("")) ? row.getCell(8).toString() : null);
				event.setCategoryName(((row.getCell(9)) != null && !row.getCell(9).toString().equals("")) ? row.getCell(9).toString() : null);
				event.setAddress(((row.getCell(10)) != null && !row.getCell(10).toString().equals("")) ? row.getCell(10).toString() : null);
				event.setCity(((row.getCell(11)) != null && !row.getCell(11).toString().equals("")) ? row.getCell(11).toString() : null);
				event.setState(((row.getCell(12)) != null && !row.getCell(12).toString().equals("")) ? row.getCell(12).toString() : null);
				event.setPostalCode(((row.getCell(13)) != null && !row.getCell(13).toString().equals("")) ? row.getCell(13).toString() : null);
				event.setLatitude(((row.getCell(14)) != null && !row.getCell(14).toString().equals("")) ? row.getCell(14).toString() : null);
				event.setLongitude(((row.getCell(15)) != null && !row.getCell(15).toString().equals("")) ? row.getCell(15).toString() : null);
				event.setEventLocationTitle(((row.getCell(16)) != null && !row.getCell(16).toString().equals("")) ? row.getCell(16).toString() : null);
				events.add(event);
			}

			// Remove header
			if (!events.isEmpty()) {
				events.remove(0);
			}
		} catch (FileNotFoundException e) {

			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		} catch (IOException e) {
			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		} catch (ParseException e) {
			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		}
		
		LOG.info(Constants.METHODEND + methodName);
		return events;
	}

	public static String formattedDate(String enteredDate) throws java.text.ParseException {
		final String methodName = "formattedDate";
		LOG.info(Constants.METHODSTART + methodName);
		
		String cDate = null;
		if (null != enteredDate && !"".equals(enteredDate)) {
			DateFormat oldFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			cDate = formatter.format(convertedDate);
		}
		
		LOG.info(Constants.METHODEND + methodName);
		return cDate;
	}

	public static java.sql.Timestamp getFormattedDate() throws java.text.ParseException {
		final String methodName = "getFormattedDate";
		LOG.info(Constants.METHODSTART + methodName);

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());

		LOG.info(Constants.METHODEND + methodName);
		return sqltDate;
	}

	public static String formEmailBody(ArrayList<BatchProcessStatus> batchProcessStatusList) {

		final String methodName = "formEmailBody";
		LOG.info(Constants.METHODSTART + methodName);
		
		StringBuilder emailBody = new StringBuilder();
		emailBody.append("Hi,");
		emailBody.append("</br></br>");

		if (batchProcessStatusList != null) {

			if (batchProcessStatusList.isEmpty()) {
				emailBody.append("Event Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
				emailBody.append("Please Verify Event Batch processes.");
			} else {
				emailBody.append("Please Find the status of Event Batch Processes.");
				emailBody.append("</br></br>");
				emailBody.append("<table cellspacing='0' cellpadding='0' border='1'><tr style='background-color: yellow;'><th>Batch Program Name</th><th>Excecution Date</th>");
				emailBody.append("<th># Rows Received</th>");
				emailBody.append("<th># Rows Processed</th>");
				emailBody.append("<th>Duplicates Count</th>");
				emailBody.append("<th>Expired Count</th>");
				emailBody.append("<th>Mandatory Fields Missing Count</th>");
				emailBody.append("<th>Image Missing Count</th>");
				emailBody.append("<th>List Image Missing Count</th>");
				emailBody.append("<th>Status</th>");
				emailBody.append("<th>Reason</th>");
				emailBody.append("<th>ProcessedFileName</th>");

				emailBody.append("</tr>");
				for (int i = 0; i < batchProcessStatusList.size(); i++) {
					BatchProcessStatus batchProcessStatus = batchProcessStatusList.get(i);
					emailBody.append("<tr>");
					emailBody.append("<td>");
					emailBody.append("Events Automation");
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getExecutionDate());
					emailBody.append("</td>");
					emailBody.append("<td>");

					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals("")) {
						emailBody.append(batchProcessStatus.getRowsRecieved());
					} else {
						emailBody.append("0");
					}

					emailBody.append("</td>");
					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals("")) {
						emailBody.append(batchProcessStatus.getRowsProcessed());
					} else {
						emailBody.append("0");
					}

					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getDuplicatesCount());
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getExpiredCount());
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getMandatoryFieldsMissingCount());
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getImageMissingCount());
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getListingImageMissingCount());
					emailBody.append("</td>");
					
					if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("1")) {
						emailBody.append("<td>");
						emailBody.append("Success");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append("N/A");
						emailBody.append("</td>");
					} else if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("0")) {
						emailBody.append("<td>");
						emailBody.append("Failure");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append(batchProcessStatus.getReason());
						emailBody.append("</td>");
					}

					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals("")) {
						emailBody.append(batchProcessStatus.getProcessedFileName());
					} else {
						emailBody.append("N/A");
					}
					
					emailBody.append("</td>");
					emailBody.append("</tr>");
				}
				emailBody.append("</table>");
			}
		} else {
			emailBody.append("Event Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
			emailBody.append("Please Verify Event Batch processes.");
		}

		emailBody.append("</br></br>");
		emailBody.append("Regards</br>");
		emailBody.append("ScanSee Team");

		LOG.info(Constants.METHODEND + methodName);
		return emailBody.toString();
	}

	public static String getCurrentDate() {		
		final String methodName = "getCurrentDate";
		LOG.info(Constants.METHODEND + methodName);
		
		String currentDate = null;
		
		try {
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = new Date();
			currentDate = df.format(date1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		LOG.info(Constants.METHODEND + methodName);
		return currentDate;
	}
	
	public static String getImageFilePath(int hubcitiID) {
		String jbossPath = PropertiesReader.getPropertyValue(Constants.JBOSS_HOME);
		String fileSeparator = System.getProperty("file.separator");
		
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(jbossPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(Constants.STANDALONE);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(Constants.DEPLOYMENT);
		mediaPathBuilder.append(fileSeparator);

		mediaPathBuilder.append(Constants.ROOTWAR);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(Constants.IMAGES);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(Constants.HUBCITI);
		File obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(hubcitiID);
		obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		mediaPathBuilder.append(fileSeparator);
		return mediaPathBuilder.toString();
	}
	
	public static String getLatestEventImageFileFromFtp(String fileName, String filePath) throws EventAutomationBatchProcessException {

		final String methodName = "getLatestEventImageFileFromFtp";
		LOG.info(Constants.METHODSTART + methodName);
		
		FTPClient client = new FTPClient();
		FileOutputStream fos = null;
		String downloadPath = null;
		String[] names = null;
		String path = null;
		
		
		try {
			
			names = fileName.split("\\.");
			fileName = names[0] + filePath;
		
			client = getFTPConnection();
			client.changeWorkingDirectory(PropertiesReader.getPropertyValue("eventftppath") + fileName + "/");
			client.enterLocalPassiveMode();
			int replyCode = client.getReplyCode();			
			if(FTPReply.isPositiveCompletion(replyCode)) {
				FTPFile[] files = client.listFiles();
				
				downloadPath = PropertiesReader.getPropertyValue("eventdownloadpath");
				File obj = new File(downloadPath + fileName);
				
				if (!obj.exists()) {
					obj.mkdir();
				}
				
					
				path = downloadPath + fileName + "/";
				for (FTPFile ftpFile : files) {					
					String name = ftpFile.getName();
					File file = new File(path + name);
					fos = new FileOutputStream(file);		
					client.retrieveFile(name, fos);				
				}
				
			} else {
				
			}
			
		} catch (SocketException exception) {
			LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new EventAutomationBatchProcessException(exception);
		} catch (IOException exception) {
			LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new EventAutomationBatchProcessException(exception);
		}
		
		finally {
			try {
				client.disconnect();
			} catch (IOException exception) {
				LOG.info(Constants.EXCEPTIONOCCURRED + methodName + exception);
				throw new EventAutomationBatchProcessException(exception);
			}
		}

		LOG.info(Constants.METHODEND + methodName);
		return path;

	}
}