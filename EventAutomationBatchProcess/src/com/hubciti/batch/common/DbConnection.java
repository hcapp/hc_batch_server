package com.hubciti.batch.common;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.hubciti.batch.exception.EventAutomationBatchProcessException;

public class DbConnection {
		/**
		 * The method for getting connection.
		 * 
		 * @return JdbcTemplate.
		 * @throws EventAutomationBatchProcessException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application is thrown which is caught in the Controller
		 *             layer.
		 */
		public JdbcTemplate getConnection() throws EventAutomationBatchProcessException {
			
			JdbcTemplate jdbcTemplate = null;
			final Logger log = Logger.getLogger(DbConnection.class);
			
			try {
				
				SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
				singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
				singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
				singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
				singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
				jdbcTemplate = new JdbcTemplate(singleConnectionDataSource);
				
			} catch (DataAccessException e) {
				log.error("Exception occured in getConnection.." + e);
				throw new EventAutomationBatchProcessException(e);
			} catch (Exception e) {
				log.error("Exception occured in getConnection..." + e);
				throw new EventAutomationBatchProcessException(e);
			}
			
			return jdbcTemplate;
		}

		/**
		 * The method for getting connection.
		 * 
		 * @return SimpleJdbcTemplate.
		 * @throws EventAutomationBatchProcessException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application is thrown which is caught in the Controller
		 *             layer.
		 */
		public SimpleJdbcTemplate getSimpleJdbcTemplate() throws EventAutomationBatchProcessException {
			
			SimpleJdbcTemplate simpleJdbcTemplate = null;
			final Logger log = Logger.getLogger(DbConnection.class);
			
			try {
				
				SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
				singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
				singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
				singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
				singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
				simpleJdbcTemplate = new SimpleJdbcTemplate(singleConnectionDataSource);
				
			} catch (DataAccessException e) {
				log.error("Exception occured in getConnection" + e);
				throw new EventAutomationBatchProcessException(e);
			} catch (Exception e) {
				log.error("Exception occured in getConnection" + e);
				throw new EventAutomationBatchProcessException(e);
			}
			
			return simpleJdbcTemplate;
		}
}
