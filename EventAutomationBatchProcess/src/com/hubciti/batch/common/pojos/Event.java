/**
 * 
 */
package com.hubciti.batch.common.pojos;

/**
 * @author sangeetha.ts
 * 
 */
public class Event {

	private String hubCitiName;

	private String eventName;

	private String shortDescription;

	private String longDescription;

	private String imagePath;
	
	private String listImagePath;

	private String startDate;

	private String endDate;

	private String moreInformationURL;

	private String categoryName;

	private String address;

	private String city;

	private String state;

	private String postalCode;

	private String eventLocationTitle;

	private String latitude;

	private String longitude;
	
	private Integer hubCitiID;

	private Integer eventID;
	
	private Integer listImageMissingCount;
	
	private Integer imageMissingCount;

	/**
	 * @return the hubCitiName
	 */
	public String getHubCitiName() {
		return hubCitiName;
	}

	/**
	 * @param hubCitiName
	 *            the hubCitiName to set
	 */
	public void setHubCitiName(String hubCitiName) {
		this.hubCitiName = hubCitiName;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName
	 *            the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * @param shortDescription
	 *            the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	/**
	 * @return the longDescription
	 */
	public String getLongDescription() {
		return longDescription;
	}

	/**
	 * @param longDescription
	 *            the longDescription to set
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the moreInformationURL
	 */
	public String getMoreInformationURL() {
		return moreInformationURL;
	}

	/**
	 * @param moreInformationURL
	 *            the moreInformationURL to set
	 */
	public void setMoreInformationURL(String moreInformationURL) {
		this.moreInformationURL = moreInformationURL;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the eventLocationTitle
	 */
	public String getEventLocationTitle() {
		return eventLocationTitle;
	}

	/**
	 * @param eventLocationTitle
	 *            the eventLocationTitle to set
	 */
	public void setEventLocationTitle(String eventLocationTitle) {
		this.eventLocationTitle = eventLocationTitle;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the hubCitiID
	 */
	public Integer getHubCitiID() {
		return hubCitiID;
	}

	/**
	 * @param hubCitiID the hubCitiID to set
	 */
	public void setHubCitiID(Integer hubCitiID) {
		this.hubCitiID = hubCitiID;
	}

	/**
	 * @return the eventID
	 */
	public Integer getEventID() {
		return eventID;
	}

	/**
	 * @param eventID the eventID to set
	 */
	public void setEventID(Integer eventID) {
		this.eventID = eventID;
	}

	public String getListImagePath() {
		return listImagePath;
	}

	public void setListImagePath(String listImagePath) {
		this.listImagePath = listImagePath;
	}

	/**
	 * @return the listImageMissingCount
	 */
	public Integer getListImageMissingCount() {
		return listImageMissingCount;
	}

	/**
	 * @param listImageMissingCount the listImageMissingCount to set
	 */
	public void setListImageMissingCount(Integer listImageMissingCount) {
		this.listImageMissingCount = listImageMissingCount;
	}

	/**
	 * @return the imageMissingCount
	 */
	public Integer getImageMissingCount() {
		return imageMissingCount;
	}

	/**
	 * @param imageMissingCount the imageMissingCount to set
	 */
	public void setImageMissingCount(Integer imageMissingCount) {
		this.imageMissingCount = imageMissingCount;
	}

}
