/**
 * 
 */
package com.hubciti.batch.process;

import java.util.Calendar;

import org.apache.log4j.Logger;

import com.hubciti.batch.common.Constants;
import com.hubciti.batch.exception.EventAutomationBatchProcessException;
import com.hubciti.batch.service.EventBatchProcessService;
import com.hubciti.batch.service.EventBatchProcessServiceImpl;

/**
 * @author sangeetha.ts
 *
 */
public class EventAutomationBatchProcessMain {
	
	/**
	 * To get logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(EventAutomationBatchProcessMain.class);
	
	public static void main(String[] args) throws EventAutomationBatchProcessException {
		
		LOG.info("*************************** EventAutomationBatchProcess ******************************************");
		LOG.info("Inside Main Method Process Start @:" + Calendar.getInstance().getTime());
		
		EventBatchProcessService eventBatchProcessService = null;
		
		try {			
			eventBatchProcessService = new EventBatchProcessServiceImpl();
			eventBatchProcessService.processEventBatchData();
			
		} catch(EventAutomationBatchProcessException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + exception.getMessage());
			eventBatchProcessService.insertEventBatchStatus(0, exception.getMessage(), Calendar.getInstance().getTime());
			eventBatchProcessService.sendEventBatchProcessStatus(null);
		}		
		
		LOG.info("Inside Main Method Process Ends  @:" + Calendar.getInstance().getTime());
		LOG.info("############################## EventAutomationBatchProcess ########################################");
	}
}
