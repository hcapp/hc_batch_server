/**
 * 
 */
package com.hubciti.batch.service;

import java.util.Date;

import com.hubciti.batch.common.pojos.Event;
import com.hubciti.batch.exception.EventAutomationBatchProcessException;

/**
 * @author sangeetha.ts
 *
 */
public interface EventBatchProcessService {

	public String processEventBatchData() throws EventAutomationBatchProcessException;
	
	public void sendEventBatchProcessStatus(String fileName) throws EventAutomationBatchProcessException;
	
	public void updateEventBatchStatus(int statusMessage, String reason, Date date, String fileName, int imageMissingCount, int listImageMissingCount) throws EventAutomationBatchProcessException;
	
	public void insertEventBatchStatus(int statusMessage, String reason, Date date) throws EventAutomationBatchProcessException;
	
	public Event getBatchEventImageList(String fileName) throws EventAutomationBatchProcessException;
	
}
