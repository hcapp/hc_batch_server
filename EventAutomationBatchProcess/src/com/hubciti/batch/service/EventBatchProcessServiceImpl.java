/**
 * 
 */
package com.hubciti.batch.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.mail.MessagingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.batch.common.Constants;
import com.hubciti.batch.common.PropertiesReader;
import com.hubciti.batch.common.Utility;
import com.hubciti.batch.common.pojos.AppConfiguration;
import com.hubciti.batch.common.pojos.BatchProcessStatus;
import com.hubciti.batch.common.pojos.EmailComponent;
import com.hubciti.batch.common.pojos.Event;
import com.hubciti.batch.dao.EventBatchProcessDao;
import com.hubciti.batch.dao.EventBatchProcessDaoImpl;
import com.hubciti.batch.exception.EventAutomationBatchProcessException;

/**
 * @author sangeetha.ts
 *
 */
public class EventBatchProcessServiceImpl implements EventBatchProcessService {
	
	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(EventBatchProcessServiceImpl.class.getName());
	
	EventBatchProcessDao eventBatchProcessDao = new EventBatchProcessDaoImpl();

	public String processEventBatchData() throws EventAutomationBatchProcessException {
		
		final String methodName = "processEventBatchData";
		LOG.info(Constants.METHODSTART + methodName);
		
		ArrayList<Event> events = null;
		
		String eventdownloadpath = PropertiesReader.getPropertyValue("eventdownloadpath");
		InputStream inputStream = null;
		String response = null;
		int status = 0;
		Event event = null;
		
		String latestFileName = Utility.getLatestEventFileFromFtp();
		try {	
			
			if (null != latestFileName) {				
				LOG.info("Recieved Living Social File Name:" + latestFileName);
				File file = new File(eventdownloadpath + latestFileName);				
				inputStream = new FileInputStream(file);				
				events = Utility.getEventFromXLSXFile(inputStream);
				if (null != events && !events.isEmpty()) {
					
					eventBatchProcessDao.batchEventDataRefresh();
					// Insert living social data
					response = eventBatchProcessDao.insertEventBatch(events, latestFileName);
					// Move data from stage to production
					response = eventBatchProcessDao.eventAutomationDataPorting(latestFileName);
	
					if (null != response && response.equals("SUCCESS")) {
						status = 1;				
						event = getBatchEventImageList(latestFileName);						
						updateEventBatchStatus(status, response, Calendar.getInstance().getTime(), latestFileName, event.getImageMissingCount(), event.getListImageMissingCount());
						moveEventProcessedFile(latestFileName);						
					} else {						
						status = 0;		
						updateEventBatchStatus(status, response, Calendar.getInstance().getTime(), latestFileName, 0, 0);
					}					
					
					
					
				} else {
					LOG.info("Empty file");
				}
			} else {				
				LOG.info("No file to process");
				response = "No file to process";
				insertEventBatchStatus(0, response, Calendar.getInstance().getTime());				
			}
			
			sendEventBatchProcessStatus(latestFileName);
			
		} catch (FileNotFoundException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		}
		
		LOG.info(Constants.METHODEND + methodName);
		return response;
	}
	
	public void updateEventBatchStatus(int statusMessage, String reason, Date date, String fileName, int imageMissingCount, int listImageMissingCount) throws EventAutomationBatchProcessException {
		
		final String methodName = "updateEventBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		
		eventBatchProcessDao.updateEventBatchStatus(statusMessage, reason, fileName, imageMissingCount, listImageMissingCount);
		
		LOG.info(Constants.METHODEND + methodName);		
	}

	public void insertEventBatchStatus(int statusMessage, String reason, Date date) throws EventAutomationBatchProcessException {
		
		final String methodName = "insertEventBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		
		eventBatchProcessDao.insertEventBatchStatus(statusMessage, reason);
		
		LOG.info(Constants.METHODEND + methodName);
	}

	
	@SuppressWarnings("static-access")
	public void sendEventBatchProcessStatus(String fileName) throws EventAutomationBatchProcessException {
		
		final String methodName = "sendEventBatchProcessStatus";
		LOG.info(Constants.METHODSTART + methodName);
		
		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		ArrayList<AppConfiguration> appConfigurations = null;
		EmailComponent emailComponent = new EmailComponent();
		String smtpHost = null;
		String smtpPort = null;
		String emailrecipients[];
		
		try {
			batchProcessStatusList = eventBatchProcessDao.getEventBatchProcessStatus(fileName);
			appConfigurations = eventBatchProcessDao.getAppConfig(Constants.EMAIL);
			String mailContent = Utility.formEmailBody(batchProcessStatusList);
			ArrayList<AppConfiguration> emailConf = eventBatchProcessDao.getAppConfig(Constants.EMAILCONFIG);

			for (int j = 0; j < emailConf.size(); j++) {
				
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPHOST)) {
					smtpHost = emailConf.get(j).getScreenContent();
				}
				
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPPORT)) {
					smtpPort = emailConf.get(j).getScreenContent();
				}
			}

			if (appConfigurations != null && !appConfigurations.isEmpty()) {
				emailrecipients = appConfigurations.get(0).getScreenContent().split(",");
				emailComponent.multipleUsersmailingComponent("support@scansee.com", emailrecipients, "Event Automation Batch Process Status",
						mailContent, smtpHost, smtpPort);
			} else {
				LOG.error("Email recipients are not available. Please Configure");
			}
			
		} catch (MessagingException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		} catch (EventAutomationBatchProcessException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new EventAutomationBatchProcessException(e);		
		}
		
		LOG.info(Constants.METHODEND + methodName);
	}
	
	public static void moveEventProcessedFile(String latestFile) throws EventAutomationBatchProcessException {

		final String methodName = "moveEventProcessedFile";
		LOG.info(Constants.METHODSTART + methodName);
		
		FTPClient client = new FTPClient();
		String processedFilePath = null;
		String activeFilePath = null;
		Date date = new Date();
		String[] names = latestFile.split("\\.");
		String imageFolder = names[0] + "_Images";
		String listImageFolder = names[0] + "_ListImages";
		
		
		try {			
			client = Utility.getFTPConnection();
			processedFilePath = PropertiesReader.getPropertyValue("processedfilepath");
			activeFilePath = PropertiesReader.getPropertyValue("eventftppath");
			
			if (null != latestFile && !"".equals(latestFile)) {
				String renameFile = FilenameUtils.removeExtension(latestFile) + "_" + date.getTime() + ".xlsx";
				boolean status = client.rename(activeFilePath + latestFile, processedFilePath + renameFile);
				if (status) {
					LOG.info("Successfully moved process file::" + latestFile);
				}
				
				if(!"_Images".equalsIgnoreCase(imageFolder)) {
					processedFilePath = PropertiesReader.getPropertyValue("processedimagepath");
					renameFile = FilenameUtils.removeExtension(imageFolder) + "_" + date.getTime();
					status = client.rename(activeFilePath + imageFolder, processedFilePath + renameFile);
					if (status) {
						LOG.info("Successfully moved process image::" + imageFolder);
					}
				}
				
				if(!"_ListImages".equalsIgnoreCase(listImageFolder)) {
					processedFilePath = PropertiesReader.getPropertyValue("processedlistimagepath");
					renameFile = FilenameUtils.removeExtension(imageFolder) + "_" + date.getTime();
					status = client.rename(activeFilePath + listImageFolder, processedFilePath + renameFile);
					if (status) {
						LOG.info("Successfully moved process image::" + listImageFolder);
					}
				}
			}			
			
		} catch (SocketException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception.getMessage());
			throw new EventAutomationBatchProcessException(exception);
		} catch (IOException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception.getMessage());
			throw new EventAutomationBatchProcessException(exception);
		}
		
		finally {
			try {
				client.disconnect();
			} catch (IOException exception) {
				LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception.getMessage());
				throw new EventAutomationBatchProcessException(exception);
			}
		}
		
		LOG.info(Constants.METHODEND + methodName);
	}
	
	public Event getBatchEventImageList(String fileName) throws EventAutomationBatchProcessException {
		
		final String methodName = "getBatchEventImageList";
		LOG.info(Constants.METHODSTART + methodName);
		
		
		Event eventImageCount = new Event();
		ArrayList<Event> events = null;
		String serverPath = null;
		String imagePath = null;
		String listImagePath = null;
		int imageMissingCount = 0;
		int listImageMissingCount = 0;
		File[] imageFiles = null;
		File[] listImageFiles = null;
		String eventImageName = null;
		String eventListImageName = null;
		BufferedImage bImage = null;
		
		try {			
			
			events = eventBatchProcessDao.getBatchEventImageList(fileName);			
			
			if(null != events && !events.isEmpty()) {
				
				imagePath = Utility.getLatestEventImageFileFromFtp(fileName, "_Images");
				listImagePath = Utility.getLatestEventImageFileFromFtp(fileName, "_ListImages");
				
				if(null != imagePath && null != imagePath) {

					File file = new File(imagePath);
					
					File listFile = new File(listImagePath);
					
					imageFiles = file.listFiles();
					
					listImageFiles = listFile.listFiles();

					for(Event event : events) {
						boolean isImageExist = false;
						boolean isListImageExist = false;
						eventImageName = event.getImagePath();
						eventListImageName = event.getListImagePath();
						
						for(File images : imageFiles) {		
							if(eventImageName.equalsIgnoreCase(images.getName())) {								
								serverPath = Utility.getImageFilePath(event.getHubCitiID());								
								File initialImage = new File(imagePath + eventImageName);
								bImage = ImageIO.read(initialImage);
								if(eventImageName.endsWith(".gif")) {
									ImageIO.write(bImage, "gif", new File(serverPath + eventImageName));
								} else if(eventImageName.endsWith(".png")) {
									ImageIO.write(bImage, "PNG", new File(serverPath + eventImageName));
								} else if(eventImageName.endsWith(".jpeg")) {
									ImageIO.write(bImage, "JPEG", new File(serverPath + eventImageName));
								}
								
								isImageExist = true;
							}							
						}
						
						if(isImageExist == false) {
							imageMissingCount++;
						}
						
						for(File images : listImageFiles) {		
							if(eventListImageName.equalsIgnoreCase(images.getName())) {								
								serverPath = Utility.getImageFilePath(event.getHubCitiID());								
								File initialImage = new File(listImagePath + eventListImageName);
								bImage = ImageIO.read(initialImage);
								if(eventListImageName.endsWith(".gif")) {
									ImageIO.write(bImage, "gif", new File(serverPath + eventListImageName));
								} else if(eventListImageName.endsWith(".png")) {
									ImageIO.write(bImage, "PNG", new File(serverPath + eventListImageName));
								} else if(eventListImageName.endsWith(".jpeg")) {
									ImageIO.write(bImage, "JPEG", new File(serverPath + eventListImageName));
								}
								
								isListImageExist = true;
							}							
						}
						
						if(isListImageExist == false) {
							listImageMissingCount++;
						}
					}
				} else {
					imageMissingCount = events.size();
					listImageMissingCount = events.size();
				}				
			}
			
		} catch(EventAutomationBatchProcessException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		} catch (FileNotFoundException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		} catch (IOException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		}
		
		eventImageCount.setImageMissingCount(imageMissingCount);
		eventImageCount.setListImageMissingCount(listImageMissingCount);
		
		LOG.info(Constants.METHODEND + methodName);
		return eventImageCount;		
	}

}
