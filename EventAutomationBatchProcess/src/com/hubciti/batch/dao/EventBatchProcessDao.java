/**
 * 
 */
package com.hubciti.batch.dao;

import java.util.ArrayList;
import java.util.Date;

import com.hubciti.batch.common.pojos.AppConfiguration;
import com.hubciti.batch.common.pojos.BatchProcessStatus;
import com.hubciti.batch.common.pojos.Event;
import com.hubciti.batch.exception.EventAutomationBatchProcessException;

/**
 * @author sangeetha.ts
 *
 */
public interface EventBatchProcessDao {
	
	/**
	 * This Method to refresh the stage table before inserting new data to it.
	 * @throws EventAutomationBatchProcessException
	 */
	public void batchEventDataRefresh() throws EventAutomationBatchProcessException;
	
	/**
	 * This Method is to insert event details to Data Base Table.
	 * @param events
	 * @return Success or Failure.
	 * @throws EventAutomationBatchProcessException
	 */
	String insertEventBatch(ArrayList<Event> events, String fileName) throws EventAutomationBatchProcessException;
	
	/**
	 * This Method will port staging table data to actual table
	 * @param fileName
	 * @return
	 * @throws EventAutomationBatchProcessException
	 */
	public String eventAutomationDataPorting(String fileName) throws EventAutomationBatchProcessException;
	/**
	 * This Method will update batch process status.
	 * @param status
	 * @param reason
	 * @param date
	 * @param apiPartnerID
	 * @param fileName
	 * @return
	 * @throws EventAutomationBatchProcessException
	 */
	public String updateEventBatchStatus(int status, String reason, String fileName, int imageMissingCount, int listImageMissingCount) throws EventAutomationBatchProcessException;
	/**
	 * This Method will insert batch process status.
	 * @param status
	 * @param reason
	 * @param date
	 * @param apiPartnerID
	 * @return
	 * @throws EventAutomationBatchProcessException
	 */
	public String insertEventBatchStatus(int status, String reason) throws EventAutomationBatchProcessException;
	/**
	 * This Method to get the batch process status.
	 * @param fileName
	 * @return
	 * @throws EventAutomationBatchProcessException
	 */
	public ArrayList<BatchProcessStatus> getEventBatchProcessStatus(String fileName) throws EventAutomationBatchProcessException;
	
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws EventAutomationBatchProcessException;
	
	public ArrayList<Event> getBatchEventImageList(String fileName) throws EventAutomationBatchProcessException;

}
