/**
 * 
 */
package com.hubciti.batch.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.hubciti.batch.common.Constants;
import com.hubciti.batch.common.DbConnection;
import com.hubciti.batch.common.Utility;
import com.hubciti.batch.common.pojos.AppConfiguration;
import com.hubciti.batch.common.pojos.BatchProcessStatus;
import com.hubciti.batch.common.pojos.Event;
import com.hubciti.batch.exception.EventAutomationBatchProcessException;

/**
 * @author sangeetha.ts
 *
 */
public class EventBatchProcessDaoImpl implements EventBatchProcessDao {
	
	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(EventBatchProcessDaoImpl.class.getName());
	/**
	 * To get database connection.
	 */
	private DbConnection dbConnection = new DbConnection();
	/**
	 * Variable for jdbcTemplate.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	public void batchEventDataRefresh() throws EventAutomationBatchProcessException {
		
		final String methodName = "batchEventDataRefresh";
		LOG.info(Constants.METHODSTART + methodName + Constants.AT + Calendar.getInstance().getTime());
		
		int result;
		
		try {
			
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchEventsRefreshStagingTable");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
			result = (Integer) resultFromProcedure.get("Status");
			
			if (result == 1) {
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				LOG.error(Constants.EXCEPTIONOCCURRED + methodName, errorNum);
				throw new EventAutomationBatchProcessException(errorMsg);
			}
			
		} catch (Exception exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new EventAutomationBatchProcessException(exception);
		}		
		
		LOG.info(Constants.METHODEND + methodName + Constants.AT + Calendar.getInstance().getTime());
	}

	public String insertEventBatch(ArrayList<Event> events, String fileName) throws EventAutomationBatchProcessException {		

		final String methodName = "insertEventBatch";
		LOG.info(Constants.METHODSTART + methodName + Constants.AT + Calendar.getInstance().getTime());
		
		List<Object[]> batch = new ArrayList<Object[]>();
		int[] updateCounts = { 0 };
		String response = null;
		
		try {
			
			final SimpleJdbcTemplate simpleJdbcTemplate = dbConnection.getSimpleJdbcTemplate();
			
			for (Event event : events) {
				Object[] values = new Object[] {event.getHubCitiName(), event.getEventName(), event.getShortDescription(), event.getLongDescription(),
						event.getImagePath(), event.getListImagePath(), event.getStartDate(), event.getEndDate(), event.getMoreInformationURL(), event.getCategoryName(), event.getAddress(), 
						event.getCity(), event.getState(), event.getPostalCode(), event.getLatitude(), event.getLongitude(), event.getEventLocationTitle()};
				batch.add(values);
			}

			updateCounts = simpleJdbcTemplate.batchUpdate("INSERT INTO HcEventsStagingData(HubCitiName,EventName,ShortDescription,LongDescription,ImagePath,EventListingImagePath,StartDate,EndDate,MoreInformationURL,CategoryName,Address,City,State,PostalCode,Latitude,Longitude,EventLocationTitle,FileName) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,\'" + fileName + "\')", batch);
			
			if (updateCounts.length > 0) {
				response = Constants.SUCCESS;
			} else {
				response = Constants.FAILURE;
			}
			
		} catch (DataAccessException exception) {			
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new EventAutomationBatchProcessException(exception);			
		}
		
		LOG.info(Constants.METHODEND + methodName + Constants.AT + Calendar.getInstance().getTime());
		return response;
	}

	public String eventAutomationDataPorting(String fileName) throws EventAutomationBatchProcessException {
		
		final String methodName = "eventAutomationDataPorting";
		LOG.info(Constants.METHODSTART + methodName + Constants.AT + Calendar.getInstance().getTime());
		
		int result = 1;
		String response = null;
		
		try {
			
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchEventsDataPorting");
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("FileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(params);
			result = (Integer) resultFromProcedure.get("Status");
			
			if (result == 0) {
				response = Constants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
			
		} catch (Exception exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new EventAutomationBatchProcessException(exception);
		}
		
		LOG.info(Constants.METHODEND + methodName + Constants.AT + Calendar.getInstance().getTime());
		return response;
	}

	public String updateEventBatchStatus(int status, String reason, String fileName, int imageMissingCount, int listImageMissingCount) throws EventAutomationBatchProcessException {
		
		final String methodName = "updateEventBatchStatus";
		LOG.info(Constants.METHODSTART + methodName + Constants.AT + Calendar.getInstance().getTime());
		
		String response = null;
		Integer responseFromProc = null;
		
		try {

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchEventsLogUpdation");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("EventStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			externalAPIListParameters.addValue("FileName", fileName);
			externalAPIListParameters.addValue("ImageMissingCount", imageMissingCount);
			externalAPIListParameters.addValue("ListingImageMissingCount", listImageMissingCount);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				response = Constants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				throw new EventAutomationBatchProcessException(errorMsg);
			}
		} catch (EmptyResultDataAccessException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		} catch (DataAccessException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new EventAutomationBatchProcessException(exception);
		}
		
		LOG.info(Constants.METHODEND + methodName + Constants.AT + Calendar.getInstance().getTime());
		return response;
	}

	public String insertEventBatchStatus(int status, String reason) throws EventAutomationBatchProcessException {
		
		final String methodName = "insertEventBatchStatus";
		LOG.info(Constants.METHODSTART + methodName + Constants.AT + Calendar.getInstance().getTime());
		
		String response = null;
		Integer responseFromProc = null;
		
		try {
			
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchEventsLogInsertion");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("EventStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				response = Constants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				throw new EventAutomationBatchProcessException(errorMsg);
			}
		} catch (EmptyResultDataAccessException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		} catch (DataAccessException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new EventAutomationBatchProcessException(exception);
		}

		LOG.info(Constants.METHODEND + methodName + Constants.AT + Calendar.getInstance().getTime());
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<BatchProcessStatus> getEventBatchProcessStatus(String fileName) throws EventAutomationBatchProcessException {
		
		final String methodName = "getEventBatchProcessStatus";
		LOG.info(Constants.METHODSTART + methodName + Constants.AT + Calendar.getInstance().getTime());
		
		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		
		try {

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchEventsLogDisplay");
			simpleJdbcCall.returningResultSet("BatchProcessStatusList", ParameterizedBeanPropertyRowMapper.newInstance(BatchProcessStatus.class));
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("Date", Utility.getFormattedDate());
			externalAPIListParameters.addValue("FileName", fileName);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			Integer status = (Integer) resultFromProcedure.get("Status");

			if (status == 0) {
				batchProcessStatusList = (ArrayList<BatchProcessStatus>) resultFromProcedure.get("BatchProcessStatusList");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				throw new EventAutomationBatchProcessException(errorMsg);
			}			

		} catch (DataAccessException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e);
			throw new EventAutomationBatchProcessException(e);
		} catch (ParseException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e);
			throw new EventAutomationBatchProcessException(e);
		}
		
		LOG.info(Constants.METHODEND + methodName + Constants.AT + Calendar.getInstance().getTime());
		return batchProcessStatusList;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws EventAutomationBatchProcessException {
		
		final String methodName = "getAppConfig";
		LOG.info(Constants.METHODSTART + methodName + Constants.AT + Calendar.getInstance().getTime());
		
		ArrayList<AppConfiguration> appConfigurationList = null;
		
		try {
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent").returningResultSet("AppConfigurationList",
					ParameterizedBeanPropertyRowMapper.newInstance(AppConfiguration.class));
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			
			appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
			
			
		} catch (DataAccessException e) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new EventAutomationBatchProcessException(e);
		}
		
		LOG.info(Constants.METHODEND + methodName + Constants.AT + Calendar.getInstance().getTime());
		return appConfigurationList;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Event> getBatchEventImageList(String fileName) throws EventAutomationBatchProcessException {
		
		final String methodName = "getBatchEventImageList";
		LOG.info(Constants.METHODSTART + methodName);
		
		ArrayList<Event> events = null;
		Integer status = null;
		
		try {
			
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchEventsImageList");
			simpleJdbcCall.returningResultSet("EventImgList", ParameterizedBeanPropertyRowMapper.newInstance(Event.class));
			final MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("FileName", fileName);
			parameters.addValue("Date", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(parameters);
			status = (Integer) resultFromProcedure.get("Status");

			if (status == 0) {
				events = (ArrayList<Event>) resultFromProcedure.get("EventImgList");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				throw new EventAutomationBatchProcessException(errorMsg);
			}
		} catch (EmptyResultDataAccessException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		} catch (DataAccessException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new EventAutomationBatchProcessException(exception);
		} catch (ParseException exception) {
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new EventAutomationBatchProcessException(exception);
		}

		LOG.info(Constants.METHODEND + methodName);
		return events;
	}

}
