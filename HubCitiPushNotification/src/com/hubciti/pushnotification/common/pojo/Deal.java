package com.hubciti.pushnotification.common.pojo;

public class Deal {

	private Integer dealId;
	
	private String dealName;
	
	private String dealDesc;
	
	private String type;
	
	private String splUrl;
	
	private String hcName;
	
	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getDealDesc() {
		return dealDesc;
	}

	public void setDealDesc(String dealDesc) {
		this.dealDesc = dealDesc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSplUrl() {
		return splUrl;
	}

	public void setSplUrl(String splUrl) {
		this.splUrl = splUrl;
	}

	public String getHcName() {
		return hcName;
	}

	public void setHcName(String hcName) {
		this.hcName = hcName;
	}

}
