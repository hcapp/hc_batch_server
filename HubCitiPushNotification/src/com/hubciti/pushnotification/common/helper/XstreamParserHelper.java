package com.hubciti.pushnotification.common.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.exception.PushNotificationException;
import com.thoughtworks.xstream.XStream;

public class XstreamParserHelper {

	private static final Logger LOG = LoggerFactory.getLogger(XstreamParserHelper.class);
	
	private static XStream xstream;
	
	private static XStream getXstreamParser() throws PushNotificationException {

		try {
			if (xstream == null) {
				xstream = new XStream();
				xstream.alias("Data", com.hubciti.pushnotification.common.pojo.Data.class);
				xstream.alias("NotificationDetails", com.hubciti.pushnotification.common.pojo.NotificationDetails.class);
				xstream.alias("Deal", com.hubciti.pushnotification.common.pojo.Deal.class);
				xstream.alias("RSSFeedMessage", com.hubciti.pushnotification.common.pojo.RSSFeedMessage.class);
			}
		} catch (Exception e) {
			LOG.error("Exception in getXstreamParser(), class:XstreamParserHelper: Error:" + e.getMessage());
			throw new PushNotificationException();
		}

		return xstream;
	}
	
	public static String produceXmlFromObject(Object object) throws PushNotificationException {
		String outputXml = null;
		try	{
			xstream = getXstreamParser();
		} catch (Exception e)	{
			e.getMessage();
		}
		outputXml = xstream.toXML(object);
		return Utility.removeSpecialChars(outputXml);
	}
	
	public Object parseXmlToObject(String xml) throws PushNotificationException {
		Object obj = null;
		try {
			xstream = getXstreamParser();
			obj = xstream.fromXML(xml);
		} catch (ClassCastException classCastException) {
			LOG.error("ClassCastException:", classCastException);
			throw new PushNotificationException(classCastException);
		} catch (Exception exception) {
			LOG.error("Exception:", exception);
			throw new PushNotificationException(exception);
		}
		return obj;
	}
}
