package com.hubciti.pushnotification.service;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.helper.BatchTime;
import com.hubciti.pushnotification.exception.PushNotificationException;

public class PushNotificationMain {

	private static final Logger LOG = (Logger) LoggerFactory.getLogger(PushNotificationMain.class);
			
	public static void main(String[] args) {

		LOG.info("-----------------------------------------------------------------------------");
		Calendar startCalendar = Calendar.getInstance();
		LOG.info("HubCiti Push Notification Process Starts @:" + startCalendar.getTime().toString());
		LOG.info("-----------------------------------------------------------------------------");
		
		PushNotificationService pushNotificationService = new PushNotificationService();
		try {
			pushNotificationService.sendNotification();
		} catch (PushNotificationException e) {
			LOG.error("Error in PushNotificationMain: " + e.getMessage());
		}
		
		LOG.info("*****************************************************************************");
		Calendar endCalendar = Calendar.getInstance();
		LOG.info("HubCiti Push Notification  Process Ends @:" + endCalendar.getTime().toString());
		BatchTime.printTime(startCalendar, endCalendar);
		LOG.info("*****************************************************************************");
	}

}
