package com.hubciti.pushnotification.common.pojo;

public class DeviceId {

	private String deviceId;

	private String platform;

	private String hcName;

	private Integer badgeCount;

	public Integer getBadgeCount() {
		return badgeCount;
	}

	public void setBadgeCount(Integer badgeCount) {
		this.badgeCount = badgeCount;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getHcName() {
		return hcName;
	}

	public void setHcName(String hcName) {
		this.hcName = hcName;
	}

}
