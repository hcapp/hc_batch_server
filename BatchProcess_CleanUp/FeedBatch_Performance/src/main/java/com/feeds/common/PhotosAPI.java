package com.feeds.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.ObjectInputStream.GetField;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.feeds.exception.RssFeedBatchProcessException;
import com.feeds.pojo.Item;
import com.feeds.service.FeedsServiceImpl;

public class PhotosAPI {

	private static final Logger LOG = LoggerFactory.getLogger(PhotosAPI.class);

	public static ArrayList<Item> readJsonFromUrl(String url, String name, String hubCitiId) {

		JSONObject json = null;
		InputStream is = null;
		String title = null;
		JSONObject json1 = null;
		String webUri = null;
		String date = null;
		String thumbnailUrl = null;
		JSONObject albumImage = null;
		String dateString = null;
		ArrayList<Item> items = new ArrayList<Item>();

		final ApplicationContext context = new ClassPathXmlApplicationContext("feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context.getBean("feedsService");

		try {

			try {
				is = new URL(url).openStream();
			} catch (MalformedURLException e) {
				LOG.error("Inside Utility : getVideoList : " + e.getMessage());
			} catch (IOException e) {
				LOG.error("Inside Utility : getVideoList : " + e.getMessage());
			}
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			Document doc = Jsoup.parse(jsonText);
			String th = doc.body().getElementsByTag("pre").text();

			try {
				json = new JSONObject(th);
			} catch (ParseException e) {
				LOG.error("Inside Utility : readPhotosJsonFromUrl :", e.getMessage());
			}

			if (json != null) {

				json1 = json.getJSONObject("Response");

				if (json1 != null) {

					JSONArray array = json1.getJSONArray("Album");
					for (int i = 0; i < array.length(); i++) {

						if (array.getJSONObject(i).getString("Title") != null) {
							title = array.getJSONObject(i).getString("Title").replaceAll("([^\\w\\s\'.,-:&\"<>])", "").replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'").replaceAll("&ldquo", "\"").replaceAll("&rdquo", "\"").replaceAll("&mdash", "--")
									.replaceAll("&ndash", "-").replaceAll(":", "");
							String[] titleArray = title.split(" ");

							try {
								dateString = titleArray[0];
								DateFormat outputDateFormat = new SimpleDateFormat("MMM dd, yyyy");
								DateFormat inputDateFormat = new SimpleDateFormat("MM/dd/yy");
								Date dateOuput = inputDateFormat.parse(dateString);
								date = outputDateFormat.format(dateOuput).replaceAll("00", "20");
							} catch (Exception e) {
								LOG.error("Inside Utility : getPhotosList : " + e.getMessage());
							}

						}
						if (array.getJSONObject(i).getString("WebUri") != null) {
							webUri = array.getJSONObject(i).getString("WebUri");
						}

						JSONObject jsonObject = array.getJSONObject(i).getJSONObject("Uris");
						String image = jsonObject.getJSONObject("AlbumHighlightImage").getString("Uri");
						try {
							is = new URL("http://www.smugmug.com" + image + "?accept=application/json").openStream();
						} catch (MalformedURLException e) {
							LOG.error("Inside Utility : getPhotosList : " + e.getMessage());
						} catch (IOException e) {
							LOG.error("Inside Utility : getPhotosList : " + e.getMessage());
						}
						BufferedReader buffReader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
						String jsonResponseText = readAll(buffReader);
						Document document = Jsoup.parse(jsonResponseText);
						String theader = document.body().getElementsByTag("pre").text();

						try {
							json = new JSONObject(theader);
							if (json != null) {

								json1 = json.getJSONObject("Response");
								if (json1 != null) {
									try {
										albumImage = json1.getJSONObject("AlbumImage");
										thumbnailUrl = albumImage.getString("ThumbnailUrl");
									} catch (Exception e) {
										LOG.error("Inside Utility : getPhotosList : " + e.getMessage());
										thumbnailUrl = null;
									}

								}
							}
						} catch (ParseException e) {
							LOG.error("Inside Utility : readPhotosJsonFromUrl :", e.getMessage());
						}
						if (json1.has("AlbumImage")) {
							Item item = new Item(title, null, thumbnailUrl, webUri, date, null, null);
							item.setId(i + 1);
							items.add(item);
						}

					}

				}

			}

			feedsService.processDatabaseOperation(items, name, hubCitiId);
		}

		catch (Exception e) {
			LOG.error("Inside Utility : getPhotosList : " + e.getMessage());

			Item item = new Item(title, null, null, null, null, null, e.getMessage());
			items.add(item);

			try {
				feedsService.processDatabaseOperation(items, name, CommonConstants.TYLERHUBCITIID);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getPhotosList : " + e1.getMessage());
			}
		}

		finally {
			try {
				is.close();
			} catch (IOException e) {
				LOG.error("Inside Utility : readJsonFromUrl:", e.getMessage());
			}
		}
		return items;

	}

	private static String readAll(Reader rd) {
		StringBuilder sb = new StringBuilder();
		int cp;
		try {
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
		} catch (IOException e) {

			LOG.error("Inside READALL method :", e.getMessage());

		}

		return sb.toString();
	}

}
