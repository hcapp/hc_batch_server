package com.feeds.batchexecution;

import java.util.Calendar;
import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.feeds.common.ApplicationConstants;

import com.feeds.exception.RssFeedBatchProcessException;

import com.feeds.service.FeedsServiceImpl;

public class BatchExecution {
	private static Logger LOG = Logger.getLogger(BatchExecution.class.getName());

	public static void main(String[] args) throws RssFeedBatchProcessException {
		executeBatchFeeds();
	}

	private static void executeBatchFeeds() {

		LOG.info("START OF THE CATEGORIES BATCH PROCESS :->" + Calendar.getInstance().getTime());

		@SuppressWarnings("resource")
		final ApplicationContext context = new ClassPathXmlApplicationContext("feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context.getBean("feedsService");
		LOG.info("=============================RSS Batch Process Starts================================");

		try {

			feedsService.processDeletionOperation();
			
			LOG.info("TRUNCATING NEWS HISTORY COMPLETED FOR TYLER , ROCKWALL , AUSTIN AND KILLEEN ...");

			feedsService.getHubCitiId();

			LOG.info("NEWS INSERTION COMPLETED FOR TYLER , ROCKWALL , AUSTIN AND KILLEEN IN STAGING TABLE...");

			feedsService.NewsFeedPorting();
			LOG.info("NEWS INSERTION COMPLETED FOR TYLER , ROCKWALL , AUSTIN AND KILLEEN IN MAIN TABLE ...");

			LOG.info("Inside Tests : mailSending");
			feedsService.emptyNewsListByEmail();

			LOG.info("===============================RSS Batch Process Ends============================");

			LOG.info("END OF THE FEEDS BATCH PROCESS :-> " + Calendar.getInstance().getTime());
		} catch (RssFeedBatchProcessException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
		}
	}
}
