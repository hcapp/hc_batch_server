package com.feeds.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.feeds.common.ApplicationConstants;
import com.feeds.common.DBConnection;
import com.feeds.common.PropertiesReader;
import com.feeds.exception.RssFeedBatchProcessException;
import com.feeds.pojo.Item;

public class FeedsDaoImpl implements FeedsDao {
	private static Logger LOG = LoggerFactory.getLogger(FeedsDaoImpl.class.getName());

	/**
	 * Variable for jdbcTemplate.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get database connection.
	 */

	public void setDataSource(DataSource dataSource) {

		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void batchInsertRecordsIntoTable(List<Item> itemList, String newsType, String hubcitiId) throws SQLException, RssFeedBatchProcessException {

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO RssFeedNewsStagingTable(Title, ImagePath, ShortDescription, LongDescription, Link,PublishedDate,VideoLink, NewsType, Message, HcHubCitiID, Adcopy, Section, Classification) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {

			dbConnection = DBConnection.getDBConnection();
			if (null != dbConnection) {

				preparedStatement = dbConnection.prepareStatement(insertTableSQL);
				dbConnection.setAutoCommit(false);

				if (null != itemList && !itemList.isEmpty()) {

					for (Item news : itemList) {
						preparedStatement.setString(1, news.getTitle());
						preparedStatement.setString(2, news.getImage());
						preparedStatement.setString(3, news.getShortDesc());
						preparedStatement.setString(4, news.getDescription());
						preparedStatement.setString(5, news.getLink());
						preparedStatement.setString(6, news.getDate());
						preparedStatement.setString(7, news.getVideoLink());
						preparedStatement.setString(8, newsType);
						preparedStatement.setString(9, news.getMessage());
						preparedStatement.setString(10, hubcitiId);
						preparedStatement.setString(11, news.getAdcopy());
						preparedStatement.setString(12, news.getSection());
						preparedStatement.setString(13, news.getClassification());
						preparedStatement.addBatch();
					}

					preparedStatement.executeBatch();
					dbConnection.commit();

				}
			} else {
				LOG.info("Unable to establish Connection object");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			dbConnection.rollback();

		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	/**
	 * Below method is used to insert rss news feed data to staging table.
	 * 
	 */

	public String insertData(List<Item> itemList, String newsType, String hubcitiId) throws RssFeedBatchProcessException {
		LOG.info("Inside FeedsBatchProcssDAOImpl : insertData : " + newsType);
		String Response = null;
		LOG.info("Inside FeedsBatchProcssDAOImpl : insertData : " + newsType);

		try {

			batchInsertRecordsIntoTable(itemList, newsType, hubcitiId);

		} catch (SQLException exception) {
			LOG.error("Inside FeedsBatchProcssDAOImpl : insertData : " + exception);
			throw new RssFeedBatchProcessException(exception);
		}

		LOG.info("Exit RealEstateDAOImpl : insertData : " + newsType);
		return Response;
		/*
		 * String strResponse = null; Object[] values = null;
		 * 
		 * // Adding code for batch upload. List<Object[]> itemBatchList = new
		 * ArrayList<Object[]>(); int[] iTotalCnt = { 0 };
		 * 
		 * try {
		 * 
		 * if (null != itemList && !itemList.isEmpty()) { for (Item items :
		 * itemList) { values = new Object[] { items.getTitle(),
		 * items.getImage(), items.getShortDesc(), items.getDescription(),
		 * items.getLink(), items.getDate(),items.getVideoLink(), newsType,
		 * items.getMessage(), hubcitiId, items.getAdcopy(), items.getSection(),
		 * items.getClassification() }; itemBatchList.add(values); }
		 * 
		 * iTotalCnt = jdbcTemplate .batchUpdate(
		 * "INSERT INTO RssFeedNewsStagingTable(Title, ImagePath, ShortDescription, LongDescription, Link,PublishedDate,VideoLink, NewsType, Message, HcHubCitiID, Adcopy, Section, Classification) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)"
		 * , itemBatchList);
		 * 
		 * if (iTotalCnt.length > 0) { strResponse =
		 * ApplicationConstants.SUCCESSTEXT;
		 * LOG.info("News Feed batch process excecuted successfully" +
		 * "News type :" + newsType + "HubCitiID :" + hubcitiId); } else {
		 * strResponse = ApplicationConstants.FAILURETEXT;
		 * LOG.info("News Feed batch process excecution failed " + "News type :"
		 * + newsType + "HubCitiID :" + hubcitiId); }
		 * 
		 * } else {
		 * 
		 * strResponse = ApplicationConstants.EMPTYLIST; }
		 * 
		 * } catch (DataAccessException exception) {
		 * LOG.error("Inside FeedsBatchProcssDAOImpl : insertData : " +
		 * exception); throw new RssFeedBatchProcessException(exception); }
		 * 
		 * LOG.info("Exit RealEstateDAOImpl : insertData : " + newsType); return
		 * strResponse;
		 */
	}

	/**
	 * Below method is used to truncate news inserted table data to insert new
	 * data.
	 * 
	 */

	public void deleteData() throws RssFeedBatchProcessException {
		LOG.info("Inside FeedsBatchProcssDAOImpl : deleteData");

		final String sql = "TRUNCATE TABLE RssFeedNewsStagingTable";

		try {

			// final SimpleJdbcTemplate simpleJdbcTemplate = new
			// SimpleJdbcTemplate(dataSource);
			jdbcTemplate.update(sql);

		} catch (DataAccessException exception) {
			LOG.error("Inside FeedsBatchProcssDAOImpl : deleteData : " + exception.getMessage());
			throw new RssFeedBatchProcessException(exception);
		}
		LOG.info("Exit FeedsBatchProcssDAOImpl : deleteData ");
	}

	/**
	 * Below method is used to move data from staging table to main table.
	 * 
	 */

	public String NewsFeedPorting() throws RssFeedBatchProcessException {

		final String methodName = "NewsFeedPorting";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Boolean result = null;
		String response = null;
		try {
			// jdbcTemplate = new JdbcTemplate(dataSource);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			// simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRssFeedNewsInsertion");
			MapSqlParameterSource feed = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(feed);
			result = (Boolean) resultFromProcedure.get("Status");
			if (null != result && result == false) {
				response = ApplicationConstants.SUCCESSTEXT;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURETEXT;
			}
		} catch (Exception exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			response = ApplicationConstants.FAILURETEXT;
			throw new RssFeedBatchProcessException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * The DAO method for sending list of items with empty information email
	 * details to database.
	 * 
	 * @return List of items with empty New(s) Feed(s) information.
	 * @throws RssFeedBatchProcessException
	 *             throws if exception occurs.
	 */

	@SuppressWarnings("unchecked")
	public List<Item> emptyNewsListByEmail() throws RssFeedBatchProcessException {
		LOG.info("Inside RealEstateDAOImpl : emptyNewsListByEmail ");

		List<Item> emptyItemsList = null;

		try {

			// jdbcTemplate = new JdbcTemplate(dataSource);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_BatchRssFeedEmailNotification");

			simpleJdbcCall.returningResultSet("emptyItemsList", new BeanPropertyRowMapper<Item>(Item.class));

			final MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue("HcHubCitiID", PropertiesReader.getPropertyValue(ApplicationConstants.HUBCITI_ID));

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(map);
			final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
			final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");

			if (null != resultFromProcedure) {
				if (null == errorNum) {
					emptyItemsList = (ArrayList<Item>) resultFromProcedure.get("emptyItemsList");
				} else {
					LOG.info("Inside RealEstateDAOImpl : emptyNewsListByEmail : " + errorNum + " errorMsg " + errorMsg);
				}

			}

		} catch (DataAccessException e) {
			LOG.error("Inside RealEstateDAOImpl : emptyNewsListByEmail : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit RealEstateDAOImpl : emptyItemsListByEmail ");
		return emptyItemsList;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Item> getHubCitiId() throws RssFeedBatchProcessException {
		final String methodName = "getHubCitiID";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<Item> hubCitiList = null;
		try {
			// jdbcTemplate = new JdbcTemplate(dataSource);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRssFeedHubCitiList");
			MapSqlParameterSource feed = new MapSqlParameterSource();
			simpleJdbcCall.returningResultSet("hubcitilst", new BeanPropertyRowMapper<Item>(Item.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(feed);

			final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
			final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

			if (null != resultFromProcedure) {

				if (null == errorNum) {

					hubCitiList = (ArrayList<Item>) resultFromProcedure.get("hubcitilst");

				} else {
					LOG.info("Inside  : getHubCitiId : " + errorNum + "errorMsg  .." + errorMsg);
				}
			}

		} catch (DataAccessException e) {
			LOG.info("Inside RealEstateDAOImpl : getHubCitiId : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit getHubCitiId : FeedsBatchProcesss ");
		return hubCitiList;

	}

	public String deleteFeedData() throws RssFeedBatchProcessException {
		LOG.info("Inside RealEstateDAOImpl : deleteFeedData ");
		String strResponse = null;
		try {

			// jdbcTemplate = new JdbcTemplate(dataSource);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRssFeedNewsDeletion");

			final MapSqlParameterSource map = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(map);
			final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
			final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");

			if (null != resultFromProcedure) {
				if (null == errorNum) {
					strResponse = ApplicationConstants.SUCCESSTEXT;
				} else {
					strResponse = ApplicationConstants.FAILURETEXT;
					LOG.error("Inside RealEstateDAOImpl : deleteFeedData : " + errorNum + " errorMsg " + errorMsg);
				}

			}

		} catch (DataAccessException e) {
			LOG.error("Inside RealEstateDAOImpl : deleteFeedData : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit RealEstateDAOImpl : deleteFeedData ");
		return strResponse;
	}

}