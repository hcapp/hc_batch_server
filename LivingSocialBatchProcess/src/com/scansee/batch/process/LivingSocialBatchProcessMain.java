package com.scansee.batch.process;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.scansee.batch.common.pojos.ExternalAPIVendor;
import com.scansee.batch.exception.LivingSocialBatchProcessException;
import com.scansee.batch.service.LivingSocialService;
import com.scansee.batch.service.LivingSocialServiceImpl;

public class LivingSocialBatchProcessMain
{

	/**
	 * To get logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(LivingSocialBatchProcessMain.class);

	/**
	 * @param args
	 * @throws LivingSocialBatchProcessException
	 */
	public static void main(String[] args) throws LivingSocialBatchProcessException
	{

		LOG.info("***************************LivingSocialBatchProcess******************************************");
		LOG.info("Inside Main Method Process Start @:" + Calendar.getInstance().getTime());

		LivingSocialService livingSocialService = null;
		ArrayList<ExternalAPIVendor> objExternalAPIVendorList = new ArrayList<ExternalAPIVendor>();
		String apiPartnerID = null;
		try
		{
			livingSocialService = new LivingSocialServiceImpl();
			objExternalAPIVendorList = livingSocialService.getAPIList("HotDeals");
			for (int i = 0; i < objExternalAPIVendorList.size(); i++)
			{
				if (objExternalAPIVendorList.get(i).getVendorName().equalsIgnoreCase("Living Social"))
				{
					apiPartnerID = objExternalAPIVendorList.get(i).getApiPartnerID();
				}
			}

			livingSocialService.processLivingSocialData(apiPartnerID);

		}
		catch (LivingSocialBatchProcessException e)
		{
			LOG.error("Error occured in ProcessMain\n" + e);
			livingSocialService.insertBatchStatus(0, e.getMessage(), Calendar.getInstance().getTime(), apiPartnerID);
			livingSocialService.sendBatchProcessStatus();
		}

		LOG.info("Inside Main Method Process Ends  @:" + Calendar.getInstance().getTime());
		LOG.info("######################################################################");
	}

}
