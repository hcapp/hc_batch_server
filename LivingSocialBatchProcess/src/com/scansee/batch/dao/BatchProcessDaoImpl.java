package com.scansee.batch.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.batch.common.Constants;
import com.scansee.batch.common.DbConnection;
import com.scansee.batch.common.Utility;
import com.scansee.batch.common.pojos.AppConfiguration;
import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.ExternalAPIVendor;
import com.scansee.batch.common.pojos.LivingSocial;
import com.scansee.batch.exception.LivingSocialBatchProcessException;

/**
 * BatchProcessDaoImpl.
 * 
 * @author sangeetha.ts
 */
public class BatchProcessDaoImpl implements BatchProcessDao
{
	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(BatchProcessDaoImpl.class.getName());
	/**
	 * To get database connection.
	 */
	private DbConnection dbConnection = new DbConnection();
	/**
	 * Variable for jdbcTemplate.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	@Override
	public String insertBatch(ArrayList<LivingSocial> livingSocials) throws LivingSocialBatchProcessException
	{
		LOG.info("*********************************************************************");
		LOG.info("Inside DAO Method Process Start @:" + Calendar.getInstance().getTime());
		List<Object[]> batch = new ArrayList<Object[]>();
		int[] updateCounts = { 0 };
		String methodName = "insertBatch";
		String response = null;
		try
		{
			final SimpleJdbcTemplate simpleJdbcTemplate = dbConnection.getSimpleJdbcTemplate();
			for (LivingSocial livingSocial : livingSocials)
			{
				Object[] values = new Object[] { livingSocial.getLivingSocialId(), livingSocial.getPartnerName(), livingSocial.getProvider(),
						livingSocial.getPrice(), livingSocial.getDiscount(), livingSocial.getSalePrice(), livingSocial.getLivingSocialName(),
						livingSocial.getShortDescription(), livingSocial.getLongDescription(), livingSocial.getImageUrl(),
						livingSocial.getTermsAndCondtions(), livingSocial.getUrl(), livingSocial.getStartDate(), livingSocial.getEndDate(),
						livingSocial.getCategory(), livingSocial.getCreatedDate(), livingSocial.getModifiedDate(), livingSocial.getCity(),
						livingSocial.getState() };
				batch.add(values);
			}
			updateCounts = simpleJdbcTemplate.batchUpdate("INSERT INTO APILivingSocialData VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", batch);
			if (updateCounts.length > 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				response = Constants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new LivingSocialBatchProcessException(exception);
		}
		LOG.info("Inside DAO Method Process Ends  @:" + Calendar.getInstance().getTime());
		LOG.info("######################################################################");
		return response;
	}

	@Override
	public String livingSocialDataPorting(String fileName)
	{
		System.out.println("Inside livingSocialDataPorting()");
		final String methodName = "livingSocialDataPorting";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchLivingSocialDataPorting");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("FileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
		return response;

	}

	@Override
	public String livingSocialLocDataPorting()
	{
		System.out.println("Inside livingSocialLocDataPorting()");
		final String methodName = "livingSocialLocDataPorting";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchLivingSocialLocDataPorting");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
		return response;

	}

	@Override
	public void batchAPILivingSocialDataRefresh()
	{
		System.out.println("Inside batchAPILivingSocialDataRefresh()");
		final String methodName = "batchAPILivingSocialDataRefresh";
		LOG.info(Constants.METHODSTART + methodName);
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchAPILivingSocialRefreshStagingTables");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
	}

	@Override
	public String livingSocialCategoryXRefUpdation()
	{
		System.out.println("Inside livingSocialCategoryXRefUpdation()");
		final String methodName = "livingSocialCategoryXRefUpdation";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchLivingSocialCategoryXRefUpdation");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
		return response;

	}

	@Override
	public String updateBatchStatus(int status, String reason, Date date, String apiPartnerID, String fileName)
			throws LivingSocialBatchProcessException
	{
		System.out.println("Inside updateBatchStatus()");
		final String methodName = "insertBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = null;
		Integer responseFromProc = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UpdateBatchLogFiles");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("APIPartnerID", apiPartnerID);
			externalAPIListParameters.addValue("APIStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			externalAPIListParameters.addValue("ProcessedFileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_UpdateBatchLog Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new LivingSocialBatchProcessException(errorMsg);
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new LivingSocialBatchProcessException(exception);
		}

		return response;
	}

	@Override
	public String insertBatchStatus(int status, String reason, Date date, String apiPartnerID) throws LivingSocialBatchProcessException
	{
		System.out.println("Inside insertBatchStatus()");
		final String methodName = "insertBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		Integer responseFromProc = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_InsertBatchLog");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("APIPartnerID", apiPartnerID);
			externalAPIListParameters.addValue("APIStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{

				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_UpdateBatchLog Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new LivingSocialBatchProcessException(errorMsg);

			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new LivingSocialBatchProcessException(exception);
		}

		return response;
	}

	@Override
	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws LivingSocialBatchProcessException
	{
		final String methodName = "getExternalAPIList";
		LOG.info(Constants.METHODSTART + methodName);

		ArrayList<ExternalAPIVendor> externalAPIList = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetAPIList").returningResultSet("externalAPIListInfo",
					ParameterizedBeanPropertyRowMapper.newInstance(ExternalAPIVendor.class));
			final SqlParameterSource externalAPIListParameters = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIList = (ArrayList<ExternalAPIVendor>) resultFromProcedure.get("externalAPIListInfo");
			if (null != resultFromProcedure.get(Constants.ERRORNUMBER))
			{
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new LivingSocialBatchProcessException(errorMsg);
			}
			else if (null != externalAPIList && !externalAPIList.isEmpty())
			{
				return externalAPIList;
			}

		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new LivingSocialBatchProcessException(exception.getMessage());

		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new LivingSocialBatchProcessException(exception.getMessage());

		}
		LOG.info(Constants.METHODEND + methodName);
		return externalAPIList;
	}

	@Override
	public ArrayList<BatchProcessStatus> getBatchProcessStatus(String apiPartnerName) throws LivingSocialBatchProcessException
	{
		LOG.info("Inside EmailNotificationDAOImpl : getBatchProcessStatus ");
		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_BatchLogFiles").returningResultSet("BatchProcessStatusList",
					ParameterizedBeanPropertyRowMapper.newInstance(BatchProcessStatus.class));
			Date date = new Date();
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("Date", Utility.getFormattedDate());
			externalAPIListParameters.addValue("APIPartnerName", apiPartnerName);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			Integer status = (Integer) resultFromProcedure.get("Status");

			batchProcessStatusList = (ArrayList<BatchProcessStatus>) resultFromProcedure.get("BatchProcessStatusList");

		}
		catch (DataAccessException e)
		{
			LOG.error("Inside EmailNotificationDAOImpl : getBatchProcessStatus : " + e);
			throw new LivingSocialBatchProcessException(e);
		}
		catch (ParseException e)
		{
			LOG.error("Inside EmailNotificationDAOImpl : getBatchProcessStatus : " + e);
			throw new LivingSocialBatchProcessException(e);
		}
		// TODO Auto-generated method stub
		return batchProcessStatusList;
	}

	@Override
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws LivingSocialBatchProcessException
	{
		LOG.info("Inside EmailNotificationDAOImpl : getAppConfig ");
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_GetScreenContent").returningResultSet("AppConfigurationList",
					ParameterizedBeanPropertyRowMapper.newInstance(AppConfiguration.class));

			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("ConfigurationType", configType);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			Integer status = (Integer) resultFromProcedure.get("Status");

			appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");

		}
		catch (DataAccessException e)
		{
			LOG.error("Inside EmailNotificationDAOImpl : getAppConfig : " + e);
			throw new LivingSocialBatchProcessException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}
}