package com.scansee.batch.dao;

import java.util.ArrayList;
import java.util.Date;

import com.scansee.batch.common.pojos.AppConfiguration;
import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.ExternalAPIVendor;
import com.scansee.batch.common.pojos.LivingSocial;
import com.scansee.batch.exception.LivingSocialBatchProcessException;

/**
 * The Interface for BatchProcessDao. ImplementedBy {@link BatchProcessDaoImpl}
 * 
 * @author sangeetha.ts
 */
public interface BatchProcessDao
{

	/**
	 * The method for inserting LivingSocial data to product details table once
	 * batch processed .
	 * 
	 * @param
	 * @throws LivingSocialBatchProcessException
	 */
	String insertBatch(ArrayList<LivingSocial> livingSocials) throws LivingSocialBatchProcessException;

	public String livingSocialDataPorting(String fileName) throws LivingSocialBatchProcessException;

	public String livingSocialLocDataPorting() throws LivingSocialBatchProcessException;

	public void batchAPILivingSocialDataRefresh() throws LivingSocialBatchProcessException;

	public String livingSocialCategoryXRefUpdation() throws LivingSocialBatchProcessException;

	public String updateBatchStatus(int status, String reason, Date date, String apiPartnerID, String fileName)
			throws LivingSocialBatchProcessException;

	public String insertBatchStatus(int status, String reason, Date date, String apiPartnerID) throws LivingSocialBatchProcessException;

	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws LivingSocialBatchProcessException;

	public ArrayList<BatchProcessStatus> getBatchProcessStatus(String apiPartnerName) throws LivingSocialBatchProcessException;
	
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws LivingSocialBatchProcessException;
}
