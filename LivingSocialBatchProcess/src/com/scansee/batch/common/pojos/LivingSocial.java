package com.scansee.batch.common.pojos;

/**
 * @author sangeetha.ts
 *
 */
/**
 * @author sangeetha.ts
 *
 */
public class LivingSocial {

	/**
	 * Variable id declared as String.
	 */
	private String livingSocialId;
	/**
	 * Variable partner declared as String.
	 */
	private String partnerName;
	/**
	 * Variable provider declared as String.
	 */
	private String provider;
	/**
	 * Variable price declared as String
	 */
	private String price;
	/**
	 * Variable discount declared as String
	 */
	private String discount;
	/**
	 * Variable salePrice declared as String
	 */
	private String salePrice;
	/**
	 * Variable name declared as String
	 */
	private String livingSocialName;
	/**
	 * Variable shortDescription declared as String
	 */
	private String shortDescription;
	/**
	 * Variable longDescription declared as String
	 */
	private String longDescription;
	/**
	 * Variable imageURL declared as String
	 */
	private String imageUrl;
	/**
	 * Variable termsAndCondtions declared as String
	 */
	private String termsAndCondtions;
	/**
	 * Variable url declared as String
	 */
	private String url;
	/**
	 * Variable startDate declared as String
	 */
	private String startDate;
	/**
	 * Variable endDate declared as String
	 */
	private String endDate;
	/**
	 * Variable category declared as String
	 */
	private String category;
	/**
	 * Variable created declared as String
	 */
	private String createdDate;

	/**
	 * Variable modified declared as String
	 */
	private String modifiedDate;
	/**
	 * Variable city declared as String
	 */
	private String city;
	/**
	 * Variable state declared as String
	 */
	private String state;
	/**
	 * 
	 * @param livingSocialId
	 */
	public void setLivingSocialId(String livingSocialId) {
		this.livingSocialId = livingSocialId;
	}
	/**
	 * 
	 * @return
	 */
	public String getLivingSocialId() {
		return livingSocialId;
	}
	/**
	 * 
	 * @return
	 */
	public String getPartnerName() {
		return partnerName;
	}
	/**
	 * 
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	/**
	 * 
	 * @return
	 */
	public String getProvider() {
		return provider;
	}
	/**
	 * 
	 * @param provider
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}
	/**
	 * 
	 * @return
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * 
	 * @param price
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * 
	 * @return
	 */
	public String getDiscount() {
		return discount;
	}
	/**
	 * 
	 * @param discount
	 */
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	/**
	 * 
	 * @return
	 */
	public String getSalePrice() {
		return salePrice;
	}
	/**
	 * 
	 * @param salePrice
	 */
	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}
	/**
	 * 
	 * @return
	 */
	public String getLivingSocialName() {
		return livingSocialName;
	}
	/**
	 * 
	 * @param livingSocialName
	 */
	public void setLivingSocialName(String livingSocialName) {
		this.livingSocialName = livingSocialName;
	}
	/**
	 * 
	 * @return
	 */
	public String getShortDescription() {
		return shortDescription;
	}
	/**
	 * 
	 * @param shortDescription
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	/**
	 * 
	 * @return
	 */

	public String getLongDescription() {
		return longDescription;
	}
	/**
	 * 
	 * @param longDescription
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	/**
	 * 
	 * @return
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * 
	 * @param imageUrl
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * 
	 * @return
	 */
	public String getTermsAndCondtions() {
		return termsAndCondtions;
	}
	/**
	 * 
	 * @param termsAndCondtions
	 */
	public void setTermsAndCondtions(String termsAndCondtions) {
		this.termsAndCondtions = termsAndCondtions;
	}
	/**
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * 
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * 
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * 
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * 
	 * @return
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * 
	 * @param category
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * 
	 * @return
	 */
	public String getCreatedDate() {
		return createdDate;
	}
	/**
	 * 
	 * @param createdDate
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * 
	 * @return
	 */
	public String getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * 
	 * @param modifiedDate
	 */
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * 
	 * @return
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 
	 * @return
	 */
	public String getState() {
		return state;
	}
	/**
	 * 
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}
}
