package com.scansee.batch.common;

public class Constants {
	/**
	 * MethodStart declared as String for logger messages.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * MethodEnd declared as String for logger messages.
	 */
	public static final String METHODEND = " Exiting method >>> ";
	/**
	 * ExceptionOccurred declared as String for logger messages.
	 */
	public static final String EXCEPTIONOCCURRED = "Exception Occurred in  >>> ";
	/**
	 * ErrorOccurred declared as String for logger messages.
	 */
	public static final String ERROROCCURRED = "Error Occurred in  >>> ";
	/**
	 * TechnicalProblemErroCode declared as String for getting error code.
	 */
	public static final String TECHNICALPROBLEMERRORCODE = "10001";
	/**
	 * TechnicalProblemErrorText declared as String for getting error text.
	 */
	public static final String TECHNICALPROBLEMERRORTEXT = "There is a technical problem, please try after some time.";
	/**
	 * This status represents the success execution of external API.
	 */
	public static final String SUCCESS_STATUSCODE = "200";

	/**
	 * This represents a task SUCCESS execution status.
	 */
	public static final String SUCCESS = "SUCCESS";
	/**
	 * This represents a task FAILURE execution status.
	 */
	public static final String FAILURE = "FAILURE";
	/**
	 * This will be used to form URL.
	 */
	public static final String QUESTIONARK = "?";
	/**
	 * This will be used to form URL.
	 */
	public static final String AMPERSAND = "&";
	/**
	 * API vendor name.
	 */
	public static final String LIVINGSOCIAL = "LIVINGSOCIAL";
	/**
	 * API vendor name. testing
	 */
	public static final String DEALMAP = "DEALMAP";
	/**
	 * API module name.
	 */
	public static final String HOTDEALS = "HotDeals";

	/**
	 * This constant for retrieving Database error message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";
	/**
	 * This constant for retrieving Database error code.
	 */
	public static final String ERRORNUMBER = "ErrorNumber";
	/**
	 * SuccessCode declared as String for getting success response code.
	 */
	public static final String SUCCESSCODE = "10000";
	/**
	 * SuccessResponseText declared as String for getting success response text.
	 */
	public static final String SUCCESSRESPONSETEXT = "SUCCESS";
	
	/**
	 * DataAccessExceptionCode declared as String for getting error code.
	 */
	public static final String DATAACCESSEXCEPTIONCODE = "10008";
	/**
	 * STATUS declared as String for getting out put parameter from data base.
	 */
	public static final String STATUS = "Status";
	
	/**
	 * BATCHFAILED declared as String for getting batch fail status.
	 */
	public static final String BATCHFAILED = "Batch Failed..";
	/**
	 * BATCHSUCCESS declared as String for getting batch fail status.
	 */
	public static final String BATCHSUCCESS = "Batch processed Successfully..";
	/**
	 * SCHEMANAME declared as String for database schema name.
	 */
	public static final String SCHEMANAME= "dbo";
	/**
	 * constructor for Constants.
	 */
	/**
	 * constructor for Constants.
	 */
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPHOST = "SMTP_Host";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPPORT = "SMTP_Port";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String EMAIL = "BatchProcessEmailNotification";

	public static final String EMAILCONFIG = "Email";
	/**
	 * Constant for NOT APPLICABLE.
	 */
	public static final String NOTAPPLICABLE = "N/A";
	private Constants()
	{

	}

}
