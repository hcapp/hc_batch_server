package com.scansee.batch.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.LivingSocial;
import com.scansee.batch.exception.LivingSocialBatchProcessException;

public class Utility
{

	private static final Logger LOG = Logger.getLogger(Utility.class);

	@SuppressWarnings("rawtypes")
	public static ArrayList<LivingSocial> getLivingSocialFromXLSXFile(InputStream inputStream) throws LivingSocialBatchProcessException
	{
		LOG.info("INSIDE getLivingSocialFromXLSXFile METHOD");
		XSSFSheet sheet = null;
		ArrayList<LivingSocial> livingSocials = new ArrayList<LivingSocial>();
		try
		{
			XSSFWorkbook wb_xssf = new XSSFWorkbook(inputStream);
			sheet = wb_xssf.getSheetAt(0);
			Iterator rowItr = sheet.rowIterator();

			while (rowItr.hasNext())
			{
				XSSFRow row = (XSSFRow) rowItr.next();

				LivingSocial livingSocial = new LivingSocial();
				livingSocial.setLivingSocialId(((row.getCell(0)) != null) ? row.getCell(0).toString() : null);
				livingSocial.setPartnerName((row.getCell(1) != null) ? row.getCell(1).toString() : null);
				livingSocial.setProvider((row.getCell(2) != null) ? row.getCell(2).toString() : null);
				livingSocial.setPrice((row.getCell(3) != null) ? row.getCell(3).toString() : null);
				livingSocial.setDiscount((row.getCell(4) != null) ? row.getCell(4).toString() : null);
				livingSocial.setSalePrice((row.getCell(5) != null) ? row.getCell(5).toString() : null);
				livingSocial.setLivingSocialName((row.getCell(6) != null) ? row.getCell(6).toString() : null);
				livingSocial.setShortDescription((row.getCell(7) != null) ? row.getCell(7).toString() : null);
				livingSocial.setLongDescription((row.getCell(8) != null) ? row.getCell(8).toString() : null);
				livingSocial.setImageUrl((row.getCell(9) != null) ? row.getCell(9).toString() : null);
				livingSocial.setTermsAndCondtions((row.getCell(10) != null) ? row.getCell(10).toString() : null);
				livingSocial.setUrl((row.getCell(11) != null) ? row.getCell(11).toString() : null);

				if (row.getRowNum() != 0)
				{

					livingSocial.setStartDate((row.getCell(12) != null) ? formattedDate(row.getCell(12).getDateCellValue().toString()) : null);
					livingSocial.setEndDate((row.getCell(13) != null) ? formattedDate(row.getCell(13).getDateCellValue().toString()) : null);
				}
				else
				{
					livingSocial.setStartDate((row.getCell(12) != null) ? row.getCell(12).toString() : null);
					livingSocial.setEndDate((row.getCell(13) != null) ? row.getCell(13).toString() : null);

				}

				livingSocial.setCategory((row.getCell(14) != null) ? row.getCell(14).toString() : null);
				livingSocial.setCreatedDate((row.getCell(15) != null) ? row.getCell(15).toString() : null);
				livingSocial.setModifiedDate((row.getCell(16) != null) ? row.getCell(16).toString() : null);
				livingSocial.setCity((row.getCell(17) != null) ? row.getCell(17).toString() : null);
				livingSocial.setState((row.getCell(18) != null) ? row.getCell(18).toString() : null);
				livingSocials.add(livingSocial);
			}

			// Remove header
			if (!livingSocials.isEmpty())
			{
				livingSocials.remove(0);
			}
		}
		catch (FileNotFoundException e)
		{

			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new LivingSocialBatchProcessException(e);
		}
		catch (IOException e)
		{
			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new LivingSocialBatchProcessException(e);
		}
		catch (ParseException e)
		{
			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new LivingSocialBatchProcessException(e);
		}
		LOG.info("EXITING FROM getLivingSocialFromXLSXFile METHOD");
		return livingSocials;
	}

	@SuppressWarnings("unused")
	public static String formattedDate(String enteredDate) throws java.text.ParseException
	{
		final String methodName = "formattedDate";
		String cDate = null;
		if (null != enteredDate && !"".equals(enteredDate))
		{

			DateFormat oldFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			cDate = formatter.format(convertedDate);
		}
		return cDate;
	}

	public static java.sql.Timestamp getFormattedDate() throws java.text.ParseException
	{
		final String methodName = "getFormattedDate";
		// LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		/*
		 * formatting the current date.
		 */
		// final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM");
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return sqltDate;
	}

	public static String formEmailBody(ArrayList<BatchProcessStatus> batchProcessStatusList)
	{

		StringBuilder emailBody = new StringBuilder();
		emailBody.append("Hi,");
		emailBody.append("</br></br>");

		if (batchProcessStatusList != null)
		{

			if (batchProcessStatusList.isEmpty())
			{

				emailBody.append("Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
				emailBody.append("Please Verify Batch processes.");
			}
			else
			{

				emailBody.append("Please Find the status of Batch Processes.");
				emailBody.append("</br></br>");
				emailBody
						.append("<table cellspacing='0' cellpadding='0' border='1'><tr style='background-color: yellow;'><th>Batch Program Name</th><th>Excecution Date</th>");

				emailBody.append("<th># Rows Received</th>");
				emailBody.append("<th># Rows Processed</th>");
				emailBody.append("<th>Duplicates Count</th>");
				emailBody.append("<th>Expired Count</th>");
				emailBody.append("<th>Mandatory Fields Missing Count</th>");
				emailBody.append("<th>Status</th>");
				emailBody.append("<th>Reason</th>");
				emailBody.append("<th>ProcessedFileName</th>");

				emailBody.append("</tr>");
				for (int i = 0; i < batchProcessStatusList.size(); i++)
				{
					BatchProcessStatus batchProcessStatus = batchProcessStatusList.get(i);
					emailBody.append("<tr>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getBatchProcessName());
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getExecutionDate());
					emailBody.append("</td>");
					emailBody.append("<td>");

					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{
						emailBody.append(batchProcessStatus.getRowsRecieved());

					}
					else
					{

						emailBody.append("0");
					}

					emailBody.append("</td>");
					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{
						emailBody.append(batchProcessStatus.getRowsProcessed());
					}
					else
					{

						emailBody.append("0");
					}

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getDuplicatesCount());

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getExpiredCount());

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getMandatoryFieldsMissingCount());

					emailBody.append("</td>");

					if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("1"))
					{
						emailBody.append("<td>");
						emailBody.append("Success");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append("N/A");
						emailBody.append("</td>");

					}
					else if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("0"))
					{

						emailBody.append("<td>");
						emailBody.append("Failure");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append(batchProcessStatus.getReason());
						emailBody.append("</td>");
					}

					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{

						emailBody.append(batchProcessStatus.getProcessedFileName());
					}
					else
					{

						emailBody.append("N/A");
					}
					emailBody.append("</td>");
					emailBody.append("</tr>");

				}
				emailBody.append("</table>");
			}
		}
		else
		{

			emailBody.append("Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
			emailBody.append("Please Verify Batch processes.");
		}

		emailBody.append("</br></br>");
		emailBody.append("Regards</br>");
		emailBody.append("ScanSee Team");

		return emailBody.toString();
	}

	public static String getCurrentDate()
	{
		String currentDate = null;
		try
		{
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = new Date();
			currentDate = df.format(date1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// TODO: handle exception
		}
		return currentDate;
	}
}