package com.scansee.batch.service;

import java.util.ArrayList;
import java.util.Date;

import com.scansee.batch.common.pojos.ExternalAPIVendor;
import com.scansee.batch.exception.LivingSocialBatchProcessException;

public interface LivingSocialService
{

	String processLivingSocialData(String apiPartnerID) throws LivingSocialBatchProcessException;

	public String movingLivingSocialDataToProduction(String processedFile) throws LivingSocialBatchProcessException;

	public String updateBatchStatus(int statusMessage, String reason, Date date, String apiName,String fileName) throws LivingSocialBatchProcessException;

	public String insertBatchStatus(int statusMessage, String reason, Date date, String apiName) throws LivingSocialBatchProcessException;

	public ArrayList<ExternalAPIVendor> getAPIList(String moduleName) throws LivingSocialBatchProcessException;

	public String sendBatchProcessStatus() throws LivingSocialBatchProcessException;
}
