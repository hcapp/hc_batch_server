package com.scansee.batch.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.mail.MessagingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.batch.common.Constants;
import com.scansee.batch.common.PropertiesReader;
import com.scansee.batch.common.Utility;
import com.scansee.batch.common.pojos.AppConfiguration;
import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.EmailComponent;
import com.scansee.batch.common.pojos.ExternalAPIVendor;
import com.scansee.batch.common.pojos.LivingSocial;
import com.scansee.batch.dao.BatchProcessDao;
import com.scansee.batch.dao.BatchProcessDaoImpl;
import com.scansee.batch.exception.LivingSocialBatchProcessException;

public class LivingSocialServiceImpl implements LivingSocialService
{

	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(LivingSocialServiceImpl.class.getName());
	BatchProcessDao batchProcessDao = new BatchProcessDaoImpl();

	@Override
	public String processLivingSocialData(String apiPartnerID) throws LivingSocialBatchProcessException
	{

		LOG.info("Inside Service Method processLivingSocialData");
		ArrayList<LivingSocial> livingSocials = null;
		String plumdistrictdownloadpath = PropertiesReader.getPropertyValue("livingsocialdownloadpath");
		InputStream inputStream = null;
		String response = null;
		int status;
		LivingSocialService livingSocialService = new LivingSocialServiceImpl();
		try
		{

			// Get the latest living social data file from ftp
			String latestFile = getLatestLivingSocialFileFromFtp();
			if (null != latestFile)
			{
				LOG.info("Recieved Living Social File Name:" + latestFile);
				File file = new File(plumdistrictdownloadpath + latestFile);
				inputStream = new FileInputStream(file);
				livingSocials = Utility.getLivingSocialFromXLSXFile(inputStream);
				if (null != livingSocials && !livingSocials.isEmpty())
				{
					batchProcessDao.batchAPILivingSocialDataRefresh();
					// Insert living social data
					response = batchProcessDao.insertBatch(livingSocials);

					// Move data from stage to production
					response = livingSocialService.movingLivingSocialDataToProduction(latestFile);

					if (null != response && response.equals("SUCCESS"))
					{
						status = 1;
					}
					else
					{
						status = 0;
					}

					livingSocialService.updateBatchStatus(status, response, Calendar.getInstance().getTime(), apiPartnerID, latestFile);

				}
				else
				{
					LOG.info("Empty file");
				}

			}

			else
			{

				LOG.info("No file to process");
				response = "No file to process";
				livingSocialService.insertBatchStatus(0, response, Calendar.getInstance().getTime(), apiPartnerID);
			}

		}
		catch (FileNotFoundException e)
		{
			LOG.error("Exception occured in processLivingSocialData\n" + e);
			throw new LivingSocialBatchProcessException(e);
		}
		catch (LivingSocialBatchProcessException e)
		{
			LOG.error("Exception occured in processLivingSocialData\n" + e);
			throw new LivingSocialBatchProcessException(e);
		}

		LOG.info("Exit Service Method processLivingSocialData");
		// Email notification
		livingSocialService.sendBatchProcessStatus();
		return response;
	}

	/**
	 * This method is used to move data from staging table to production table.
	 */
	@Override
	public String movingLivingSocialDataToProduction(String processedFile) throws LivingSocialBatchProcessException
	{
		LOG.info("Inside Service Method movingLivingSocialDataToProduction");
		String response = null;
		try
		{
			response = batchProcessDao.livingSocialDataPorting(processedFile);
			if (response.equals("SUCCESS"))
			{
				response = batchProcessDao.livingSocialLocDataPorting();
				if (response.equals("SUCCESS"))
				{
					response = batchProcessDao.livingSocialCategoryXRefUpdation();
					// Move processed files
					moveProcessedFile(processedFile);
				}
			}
		}
		catch (LivingSocialBatchProcessException e)
		{
			LOG.info("Inside LivingSocialServiceImpl : movingLivingSocialDataToProduction : " + e);
			throw new LivingSocialBatchProcessException(e);

		}
		LOG.info("Exit Service Method movingLivingSocialDataToProduction");
		return response;
	}

	/**
	 * This method is used to update batch process.
	 */
	@Override
	public String updateBatchStatus(int statusMessage, String reason, Date date, String apiName, String fileName)
			throws LivingSocialBatchProcessException
	{
		final String methodName = "updateBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = batchProcessDao.updateBatchStatus(statusMessage, reason, date, apiName, fileName);
		LOG.info(Constants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method is used to insert batch process.
	 */
	@Override
	public String insertBatchStatus(int statusMessage, String reason, Date date, String apiName) throws LivingSocialBatchProcessException
	{
		final String methodName = "insertBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = batchProcessDao.insertBatchStatus(statusMessage, reason, date, apiName);
		LOG.info(Constants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method is used to get API Information.
	 */
	@Override
	public ArrayList<ExternalAPIVendor> getAPIList(String moduleName) throws LivingSocialBatchProcessException
	{
		ArrayList<ExternalAPIVendor> objExternalAPIVendor = new ArrayList<ExternalAPIVendor>();
		objExternalAPIVendor = batchProcessDao.getExternalAPIList(moduleName);
		return objExternalAPIVendor;

	}

	/**
	 * This method is used to connect FTP server and down load latest commission
	 * junction file.
	 * 
	 * @return Ftp file
	 * @throws CommissionJunctionBatchProcessException
	 */

	public static String getLatestLivingSocialFileFromFtp() throws LivingSocialBatchProcessException
	{

		LOG.info("Inside Service Method getLatestLivingSocialFromFtp ");
		FTPClient client = new FTPClient();
		FileOutputStream fos = null;
		ArrayList<FTPFile> ftpFiles = new ArrayList<FTPFile>();
		String downloadPath = null;
		FTPFile latestFile = null;
		String fileName = null;
		try
		{
			// Get FTP connectio
			client = getFTPConnection();
			// Change directory to active files directory
			client.changeWorkingDirectory(PropertiesReader.getPropertyValue("livingsocialftppath"));
			FTPFile[] files = client.listFiles();

			// Extract only xlsx files
			for (FTPFile ftpFile : files)
			{

				String filename = ftpFile.getName();

				if (FilenameUtils.isExtension(filename, "xlsx"))
				{

					ftpFiles.add(ftpFile);

				}

			}

			// Get the latest xlsx file
			if (ftpFiles != null & !ftpFiles.isEmpty())
			{

				Date lastMod = ftpFiles.get(0).getTimestamp().getTime();
				latestFile = ftpFiles.get(0);

				for (FTPFile ftpFile : ftpFiles)
				{
					if (ftpFile.getTimestamp().getTime().after(lastMod))
					{
						latestFile = ftpFile;
						lastMod = ftpFile.getTimestamp().getTime();
					}
				}
				// Download the latest file local
				if (null != latestFile)
				{

					downloadPath = PropertiesReader.getPropertyValue("livingsocialdownloadpath");
					fileName = latestFile.getName();
					File obj = new File(downloadPath);
					if (!obj.exists())
					{
						obj.mkdir();
					}
					fos = new FileOutputStream(downloadPath + fileName);
					client.retrieveFile(fileName, fos);

				}

			}

		}
		catch (SocketException exception)
		{
			LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
			throw new LivingSocialBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
			throw new LivingSocialBatchProcessException(exception);
		}
		finally
		{
			try
			{
				client.disconnect();

			}
			catch (IOException exception)
			{
				LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
				throw new LivingSocialBatchProcessException(exception);
			}
		}

		LOG.info("Exit Service Method getLatestLivingSocialFromFtp ");
		return fileName;

	}

	public static ArrayList<String> moveProcessedFile(String latestFile) throws LivingSocialBatchProcessException
	{

		LOG.info("Inside Service Method moveProcessedFile");
		FTPClient client = new FTPClient();
		String processedFilePath = null;
		String activeFilePath = null;
		Date date = new Date();
		try
		{

			client = getFTPConnection();
			processedFilePath = PropertiesReader.getPropertyValue("processedfilepath");
			activeFilePath = PropertiesReader.getPropertyValue("livingsocialftppath");
			client.changeWorkingDirectory(PropertiesReader.getPropertyValue("livingsocialppath"));
			if (null != latestFile && !"".equals(latestFile))
			{

				String renameFile = FilenameUtils.removeExtension(latestFile) + "_" + date.getTime() + ".xlsx";
				boolean status = client.rename(activeFilePath + latestFile, processedFilePath + renameFile);
				if (status)
				{

					LOG.info("Successfully moved process file::" + latestFile);
				}

			}

		}
		catch (SocketException exception)
		{
			LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
			throw new LivingSocialBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
			throw new LivingSocialBatchProcessException(exception);
		}
		finally
		{

			try
			{
				client.disconnect();

			}
			catch (IOException exception)
			{
				LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
				throw new LivingSocialBatchProcessException(exception);
			}
		}
		LOG.info("Exit Service Method moveProcessedFile");
		return null;
	}

	public static FTPClient getFTPConnection() throws LivingSocialBatchProcessException
	{
		LOG.info("Inside Service Method getFTPConnection");
		FTPClient client = new FTPClient();
		int reply;
		try
		{
			client.connect(PropertiesReader.getPropertyValue("ftp_url"));
			client.login(PropertiesReader.getPropertyValue("ftp_username"), PropertiesReader.getPropertyValue("ftp_password"));
			// After connection attempt, you should check the reply code to
			// verify
			// success.
			reply = client.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply))
			{
				client.disconnect();
				LOG.error("****FTP server refused connection******");
				System.exit(1);
			}
			else
			{
				LOG.info("***FTP connection Established*****");
				client.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
				client.setFileTransferMode(FTP.BINARY_FILE_TYPE);

			}
		}
		catch (SocketException exception)
		{
			LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
			throw new LivingSocialBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside LivingSocialServiceImpl : getLivingSocialFromFtp : " + exception);
			throw new LivingSocialBatchProcessException(exception);
		}
		LOG.info("Exit Service Method getFTPConnection");
		return client;
	}

	@SuppressWarnings("static-access")
	@Override
	public String sendBatchProcessStatus() throws LivingSocialBatchProcessException
	{
		LOG.info("Inside EmailNotificationServiceImpl : getBatchProcessStatus ");
		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		ArrayList<AppConfiguration> appConfigurations = null;
		EmailComponent emailComponent = new EmailComponent();
		String smtpHost = null;
		String smtpPort = null;
		String emailrecipients[];
		String response = null;
		try
		{
			batchProcessStatusList = batchProcessDao.getBatchProcessStatus("Living Social");
			appConfigurations = batchProcessDao.getAppConfig(Constants.EMAIL);
			String mailContent = Utility.formEmailBody(batchProcessStatusList);
			ArrayList<AppConfiguration> emailConf = batchProcessDao.getAppConfig(Constants.EMAILCONFIG);

			for (int j = 0; j < emailConf.size(); j++)
			{
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPHOST))
				{
					smtpHost = emailConf.get(j).getScreenContent();
				}
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPPORT))
				{
					smtpPort = emailConf.get(j).getScreenContent();
				}
			}

			if (appConfigurations != null && !appConfigurations.isEmpty())
			{
				emailrecipients = appConfigurations.get(0).getScreenContent().split(",");
				emailComponent.multipleUsersmailingComponent("support@scansee.com", emailrecipients, "Living Social Batch Process Status",
						mailContent, smtpHost, smtpPort);

			}
			else
			{
				response = "Email recipients are not available. Please Configure";
			}

		}

		catch (MessagingException e)
		{
			LOG.error("Exception Occurred::::::::::::::::::" + e.getMessage());
			response = Constants.FAILURE;
		}
		catch (LivingSocialBatchProcessException e)
		{
			response = Constants.FAILURE;
			LOG.error("Exception Occurred::::::::::::::::::" + e.getMessage());
		}
		return response;
	}
}
