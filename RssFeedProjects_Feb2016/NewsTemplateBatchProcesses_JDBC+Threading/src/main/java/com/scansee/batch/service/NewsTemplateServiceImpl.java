package com.scansee.batch.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.batch.common.ApplicationConstants;
import com.scansee.batch.common.EmailComponent;
import com.scansee.batch.common.PropertiesReader;
import com.scansee.batch.common.Utility;
import com.scansee.batch.dao.NewsTemplateDao;
import com.scansee.batch.exception.NewsTemplateBatchProcessesException;
import com.scansee.batch.pojo.Item;
import com.scansee.batch.threads.RssBatchThread;

public class NewsTemplateServiceImpl implements NewsTemplateService {

	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(NewsTemplateServiceImpl.class.getName());

	private NewsTemplateDao newsTemplateDao;

	public void setNewsTemplateDao(NewsTemplateDao newsTemplateDao) {
		this.newsTemplateDao = newsTemplateDao;
	}

	/**
	 * method to delete history of feed data from staging table
	 * 
	 */
	public String deleteFeedHistory() throws NewsTemplateBatchProcessesException {
		String strMethodName = "deleteFeedHistory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String response = null;
		try {
			response = newsTemplateDao.deleteFeedData();
		} catch (NewsTemplateBatchProcessesException e) {
			LOG.error("Inside Rss NewsTemplateServiceImpl : deleteFeedHistory : " + e);
			throw new NewsTemplateBatchProcessesException(e);
		}
		LOG.info("Exit Service method deleteFeedHistory");
		return response;
	}

	/**
	 * method to get rss feed data from XML Parser
	 * 
	 */
	public String getNewsFeedDetails() throws NewsTemplateBatchProcessesException {
		String strMethodName = "getNewsFeedDetails";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String response = null;
		ArrayList<Item> hubList = null;
		
		List<RssBatchThread> rssThreads = new ArrayList<RssBatchThread>();
		
		try {

			hubList = newsTemplateDao.getHubitiInfo();

			if (null != hubList && !hubList.isEmpty()) {
			
				for (Item item : hubList) {
					LOG.info("START : PARSING NEWS:->" + Calendar.getInstance().getTime());
				
					rssThreads.add(new RssBatchThread(item));
			
					LOG.info("END : PARSING NEWS :->" + Calendar.getInstance().getTime());
				}
				
				if( !rssThreads.isEmpty()){
					for( RssBatchThread rssThread :rssThreads){
						try {
							rssThread.getThread().join();
					
						} catch (InterruptedException e) {
							LOG.error("Inside NewsTemplateServiceImpl : getNewsFeedDetails() : " + e.getMessage());
						}
					}
					
				}
				
			}
		} catch (NewsTemplateBatchProcessesException e) {
			LOG.error("Inside NewsTemplateServiceImpl : getNewsFeedDetails() : " + e.getMessage());
			throw new NewsTemplateBatchProcessesException(e);
		}
		LOG.info("Exit Service method getNewsFeedDetails");
		return response;
	}
	/**
	 * method is used to insert rss news feed data to staging table.
	 * 
	 */
	public String processDatabaseOperation(List<Item> items, String category, String subcategory, String hubcitiId) throws NewsTemplateBatchProcessesException {
		String strMethodName = "processDatabaseOperation";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		String response = null;
		String portingResponse = null;

		try {

			if (null != items && !items.isEmpty()) {
				response = newsTemplateDao.insertData(items, category, subcategory, hubcitiId);
				if (null != response && response.equals(ApplicationConstants.SUCCESS)) {
					portingResponse = ApplicationConstants.SUCCESS;
				} else {
					portingResponse = ApplicationConstants.FAILURE;
				}
			} else {
				response = "No Items to insert";
				LOG.info("Inside NewsTemplateServiceImpl : processDatabaseOperation : " + " No Items in the List to insert into database.");
			}
		} catch (NewsTemplateBatchProcessesException e) {
			LOG.error("Inside NewsTemplateServiceImpl : processDatabaseOperation : " + e);
			throw new NewsTemplateBatchProcessesException(e);
		}
		LOG.info("Exit Service method processDatabaseOperation");
		return portingResponse;
	}

	/**
	 * method is used to insert rss news feed data to main table.
	 * 
	 */
	public String NewsFeedPorting() throws NewsTemplateBatchProcessesException {
		String response = null;
		String strMethodName = "NewsFeedPorting";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		try {

			response = newsTemplateDao.NewsFeedPorting();

			if (null != response && response.equals(ApplicationConstants.SUCCESS)) {
				response = ApplicationConstants.SUCCESS;
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (NewsTemplateBatchProcessesException e) {
			LOG.error("Inside NewsTemplateServiceImpl : processDatabaseOperation : " + e);
			throw new NewsTemplateBatchProcessesException(e);
		}
		LOG.info("Exit Service method NewsFeedPorting");
		return response;
	}

	/**
	 * The DAO method for sending list of items with empty information email
	 * details to database.
	 * 
	 */
	public String emptyNewsListByEmail() {
		LOG.info("Inside Rss NewsTemplateServiceImpl : emptyNewsListByEmail");

		String response = null;
		List<Item> newsItemList = null;

		final String strSmtpPort = PropertiesReader.getPropertyValue(ApplicationConstants.SMTP_PORT);
		final String strSmtpHost = PropertiesReader.getPropertyValue(ApplicationConstants.SMTP_SERVER);
		final String strSubject = PropertiesReader.getPropertyValue(ApplicationConstants.EMAIL_SUBJECT);
		final String strEmailrecipient = PropertiesReader.getPropertyValue(ApplicationConstants.SENDER_TO_LIST);
		final String strFromEmailId = PropertiesReader.getPropertyValue(ApplicationConstants.FROM_MAIL);
		final String strEmailrecipients[] = strEmailrecipient.split(",");

		try {
			newsItemList = newsTemplateDao.emptyNewsListByEmail();

			if (null != newsItemList && !newsItemList.isEmpty()) {
				final String strMailContent = Utility.emailBody(newsItemList);
				response = ApplicationConstants.SUCCESS;
				EmailComponent.multipleUsersmailingComponent(strFromEmailId, strEmailrecipients, strSubject, strMailContent, strSmtpHost, strSmtpPort);
			} else {
				response = ApplicationConstants.FAILURE;
			}

		} catch (MessagingException e) {
			LOG.error("Inside NewsTemplateServiceImpl : emptyNewsListByEmail :  MessagingException : " + e.getMessage());

		} catch (NewsTemplateBatchProcessesException e) {
			LOG.error("Inside NewsTemplateServiceImpl : emptyNewsListByEmail : " + e.getMessage());
		}

		LOG.info("Exit NewsTemplateServiceImpl : emptyNewsListByEmail : " + response);
		return response;
	}

}
