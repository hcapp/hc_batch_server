package com.feeds.common;

/**
 * This Class is used to store application lever constants.
 * 
 * @author shyamsundara_hm
 * 
 */
public class CommonConstants {

	// CURRENT FEEDS
	public static final String BREAKING_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/allbreaking/hours/192";
	// public static final String
	// breakingSilent="http://www.tylerpaper.com/rss/custom/type/breakingsilent/hours/192";
	public static final String SPORTS_NEWS_URL = "http://www.etfinalscore.com/rss/custom/type/all/hours/192";
	// public static final String
	// food="http://www.tylerpaper.com/rss/custom/type/food/hours/192";
	public static final String BUSINESS_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/business/hours/192";

	public static final String EDITORIAL_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/editorial/hours/192";
	public static final String HEALTH_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/health/hours/192";
	// public static final String
	// EDITORIAL_NEWS_URL="http://www.tylerpaper.com/rss/custom/type/editorial/hours/192";
	public static final String LIFE_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/life/hours/192";
	public static final String VIDEOS_SPORTS_URL = "http://link.brightcove.com/services/mrss/2322069287001/tags/sports/hours/192";
	public static final String VIDEOS_NEWS_URL = "http://api.brightcove.com/services/library?command=search_videos&page_size=30&all=tag:news&video_fields=id,name,shortDescription,videoStillURL&sort_by=CREATION_DATE:DESC&get_item_count=true&token=x95LXczyNI5-G9kX0cjsHM9edPFzaKFTE4PANJ7L2rQfuF-swGUxJg..";
	public static final String SMUGMUG_PHOTOS_URL = "http://www.smugmug.com/api/v2/user/focusinonme!albums?accept=application/json";
	
	public static final String BRIGHTCOVE_VIDEOS_URL="http://link.brightcove.com/services/player/bcpid3742235943001?bckey=AQ~~,AAACHKYdcFk~,IGxDpm7wjyHsFkDSEiehCuhZ0IVXD7vt&bctid=";

	// public static final String
	// employment="http://redadmin.thejobnetwork.com/API/api.asp?sp=API_Get_Active_PositionsWithLocation_RSS2&isRSS2=true&Parameters=1495,0";
	// public static final String
	// goguideEvents="http://www.tylerpaper.info/GoGuide/rss.php?days=90";
	// public static final String
	// top="http://www.tylerpaper.com/rss/custom/type/trendingstories/hours/72";
	public static final String TOP_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/others/hours/72";

	// NEW FEEDS

	// public static final String
	// breakingnew="http://www.tylerpaper.com/rss/custom/type/breakingsilent/hours/192";

	// public static final String
	// businessnew="http://www.tylerpaper.com/rss/category/id/TP-Business-549/hours/192";

	// public static final String
	// editorialnew="http://www.tylerpaper.com/rss/category/id/TP-Opinion-570/hours/192";

	public static final String ENTERTAINMENT_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/category/id/TP-Arts+Entertainment-538/hours/192";
	public static final String FOOD_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/category/id/TP-Food-555/hours/192";
	public static final String ALLARTICLES_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/all/hours/192";
	public static final String WEATHERTYLER_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/category/id/TP-Weather-586/hours/300";

	/*
	 * Constants for Rockwall News Feedd
	 */

	public static final String GOODEVENTS_NEWS_URL = "http://blueribbonnews.com/category/events/feed/";
	public static final String GOODCAUSE_NEWS_URL = "http://blueribbonnews.com/category/area-clubsorgs/feed/";
	public static final String GOODLIVING_NEWS_URL = "http://blueribbonnews.com/category/community/feed/  ";
	public static final String GOODCULTURE_NEWS_URL = "http://blueribbonnews.com/category/music-art-theater/feed/";
	public static final String GOODPEOPLE_NEWS_URL = "http://blueribbonnews.com/category/good-people/feed/";
	public static final String GOODPETS_NEWS_URL = "http://blueribbonnews.com/category/pets/feed/";
	public static final String GOODFAITH_NEWS_URL = "http://blueribbonnews.com/category/faith/feed/ ";
	public static final String GOODTHINKING_NEWS_URL = "http://blueribbonnews.com/category/schools/feed/";
	public static final String GOODBUSINESS_NEWS_URL = "http://blueribbonnews.com/category/business/feed/";
	public static final String GOODSPORTS_NEWS_URL = "http://blueribbonnews.com/category/sports-2/feed/";
	public static final String GOODNEIGHBOURS_NEWS_URL = "http://blueribbonnews.com/category/guest-columns/health-and-medical/feed/";
	public static final String GUESTCOLUMNS_NEWS_URL = "http://blueribbonnews.com/category/guest-columns/feed/ ";

	// Constants for Austin News Feed

	public static final String CITYWIDE_NEWS_URL = "http://austintexas.gov/site/news/rss.xml";

	public static final String IMAGINEAUSTIN_NEWS_URL = "http://www.austintexas.gov/department/news/1779/rss.xml";

	public static final String ABIA_NEWS_URL = "  https://www.austintexas.gov/department/news/341/rss.xml";

	public static final String ANIMALSERVICES_NEWS_URL = " https://www.austintexas.gov/department/news/334/rss.xml";

	public static final String MEDICALSERVICES_NEWS_URL = "http://austintexas.gov/department/news/353/rss.xml";

	public static final String FIRE_NEWS_URL = " https://www.austintexas.gov/department/news/355/rss.xml";

	public static final String HUMANSERVICES_NEWS_URL = " http://austintexas.gov/department/news/286/rss.xml";

	public static final String SUSTAINABILITY_NEWS_URL = " http://austintexas.gov/department/news/364/rss.xml";

	public static final String PARKS_NEWS_URL = "http://austintexas.gov/department/news/288/rss.xml";

	public static final String POLICE_NEWS_URL = "https://www.austintexas.gov/department/news/296/rss.xml";

	// urls for KDH

	public static final String TOPSTORIES_NEWS_URL = "http://kdhnews.com/search/?q=&nsa=eedition&t=&l=50&s=start_time&sd=desc&f=rss&d=&d1=&d2=&c%5b%5d=news%2Flocal*&t=";

	public static final String WEATHERKILEEN_NEWS_URL = "http://kdhnews.com/search/?q=&t=article&l=25&d=&d1=&d2=&s=start_time&sd=desc&c[]=news/weather*&f=rss";

	public static final String BUSINESSKILLEEN_NEWS_URL = "http://kdhnews.com/search/?q=&t=article&l=25&d=&d1=&d2=&s=start_time&sd=desc&c[]=business*&f=rss";

	public static final String TRAFFIC_NEWS_URL = "http://kdhnews.com/search/?q=&t=article&l=25&d=&d1=&d2=&s=start_time&sd=desc&c[]=news/traffic*&f=rss";

	public static final String CRIME_NEWS_URL = "http://kdhnews.com/search/?q=&t=article&l=25&d=&d1=&d2=&s=start_time&sd=desc&c[]=news/crime*&f=rss";

	public static final String OPINIONKILLEEN_NEWS_URL = "http://kdhnews.com/search/?q=&t=article&l=25&d=&d1=&d2=&s=start_time&sd=desc&c[]=opinion*&f=rss";

	public static final String OBITS_NEWS_URL = "http://kdhnews.com/search/?q=&t=article&l=10&d=&d1=&d2=&s=start_time&sd=desc&c[]=obituaries,obituaries/*&f=rss";

	public static final String WORLDNEWS_NEWS_URL = "http://kdhnews.com/search/?mode=null&q=&nsa=eedition&t=&l=50&s=start_time&sd=desc&f=rss&d=&d1=&d2=&c[]=news%2Fworld*";

	public static final String TYLERHUBCITIID = "10";
	public static final String ROCKWALLHUBCITIID = "2070";
	public static final String AUSTINHUBCITIID = "8";
	public static final String KILLEENHUBCITINAME = "KILLEEN DAILY HERALD";

	/**
	 * METHODSTART declared as string for logger message.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * METHODEND declared as string for logger message.
	 */
	public static final String METHODEND = "Exiting method >>> ";
	/**
	 * EXCEPTIONOCCURED declared as string for logger message.
	 */

	public static final String EXCEPTIONOCCURED = "Exception Occurred in  >>> ";
	public static final String URL_PATTERN = "^(https://|http://|ftp://|file://|www.)[-a-zA-Z0-9+&@#/%?=~_|!:;]+[.][-a-zA-Z0-9+&@#/%?=~_|!:;.()]+";
	public static final String ERRORNUMBER = "ErrorNumber";

	/**
	 * for database Error Message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";

}
