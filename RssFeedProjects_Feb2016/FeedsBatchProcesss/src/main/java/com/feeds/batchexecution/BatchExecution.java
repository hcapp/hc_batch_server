package com.feeds.batchexecution;

import java.util.Calendar;
import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.feeds.common.ApplicationConstants;

import com.feeds.exception.RssFeedBatchProcessException;

import com.feeds.service.FeedsServiceImpl;

public class BatchExecution {
	private static Logger LOG = Logger
			.getLogger(BatchExecution.class.getName());

	public static void main(String[] args) throws RssFeedBatchProcessException {
		executeBatchFeeds();
	}

	private static void executeBatchFeeds() {

		LOG.info("START OF THE CATEGORIES BATCH PROCESS :->"
				+ Calendar.getInstance().getTime());

		@SuppressWarnings("resource")
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");

		try

		{

			feedsService.processDeletionOperation();

			feedsService.getHubCitiId();
			/*
			 * for (int i = 0; i < hubList.size(); i++) {
			 * 
			 * if (PropertiesReader.getPropertyValue("tyler_hubcitiName")
			 * .equals(hubList.get(i).getHubCitiName())) {
			 * 
			 * LOG.info("START OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime()); LOG.info(
			 * "========================================================================================"
			 * ); LOG.info("TYLER :START OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime());
			 * LOG.info("Inside Tests : sports"); Utility.getTylerList("sports",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : health"); Utility.getTylerList("health",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : life"); Utility.getTylerList("life",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : top"); Utility.getTylerList("top",
			 * hubList.get(i).getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : opinion");
			 * Utility.getTylerList("opinion", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : business");
			 * Utility.getTylerList("business", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : videos"); Utility.getTylerList("videos",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : employment");
			 * Utility.getLatestClassifieldFromFtp("Classifields", hubList
			 * .get(i).getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : entertainment");
			 * Utility.getTylerList("entertainment", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : food"); Utility.getTylerList("food",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : weather");
			 * Utility.getTylerList("weather", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : allarticles");
			 * Utility.getTylerList("all", hubList.get(i).getHcHubCitiID());
			 * 
			 * LOG.info("TYLER :END OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime()); } else if
			 * (PropertiesReader.getPropertyValue(
			 * "rockwall_hubcitiName").equals( hubList.get(i).getHubCitiName()))
			 * { LOG.info(
			 * "========================================================================================"
			 * );
			 * 
			 * LOG.info("ROCKWALL :START OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime());
			 * 
			 * LOG.info("Inside Tests : good events/times");
			 * Utility.getRockwallList("good times", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good cause");
			 * Utility.getRockwallList("good cause", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good living");
			 * Utility.getRockwallList("good living", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good business");
			 * Utility.getRockwallList("good business", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good people");
			 * Utility.getRockwallList("good people", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good pets");
			 * Utility.getRockwallList("good pets", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good faith");
			 * Utility.getRockwallList("good faith", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good thinking");
			 * Utility.getRockwallList("good thinking", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good health");
			 * Utility.getRockwallList("good health", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good culture");
			 * Utility.getRockwallList("good culture", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : good sports");
			 * Utility.getRockwallList("good sports", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : columnists");
			 * Utility.getRockwallList("columnists", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("ROCKWALL :END OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime());
			 * 
			 * } else if (PropertiesReader.getPropertyValue(
			 * "austin_hubcitiName").equals( hubList.get(i).getHubCitiName())) {
			 * LOG.info("AUSTIN :START OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime());
			 * 
			 * LOG.info("Inside Tests : citywide");
			 * Utility.getAustinList("citywide", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : imagineaustin");
			 * Utility.getAustinList("imagineaustin", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : abia"); Utility.getAustinList("abia",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : animalservices");
			 * Utility.getAustinList("animalservices", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : medicalservices");
			 * Utility.getAustinList("medicalservices", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : fire"); Utility.getAustinList("fire",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : humanservices");
			 * Utility.getAustinList("humanservices", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : sustainability");
			 * Utility.getAustinList("sustainability", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : parks"); Utility.getAustinList("parks",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : police");
			 * Utility.getAustinList("police", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * } else if (CommonConstants.KILLEENHUBCITINAME.equals(hubList
			 * .get(i).getHubCitiName())) {
			 * LOG.info("KILLEEN :START OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime());
			 * 
			 * LOG.info("Inside Tests : topstories");
			 * Utility.getKilleenList("topstories", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : weather");
			 * Utility.getKilleenList("Weather", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : business");
			 * Utility.getKilleenList("Business", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : traffic");
			 * Utility.getKilleenList("Traffic", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : crime"); Utility.getKilleenList("Crime",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : opinion");
			 * Utility.getKilleenList("Opinion", hubList.get(i)
			 * .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : obits"); Utility.getKilleenList("obits",
			 * hubList.get(i) .getHcHubCitiID());
			 * 
			 * LOG.info("Inside Tests : worldnews");
			 * Utility.getKilleenList("worldnews", hubList.get(i)
			 * .getHcHubCitiID());
			 * LOG.info("KILLEEN :END OF THE FEEDS BATCH PROCESS :->" +
			 * Calendar.getInstance().getTime()); }
			 * 
			 * }
			 */

			LOG.info("NEWS INSERTION COMPLETED FOR TYLER , ROCKWALL , AUSTIN AND KILLEEN ...");

			LOG.info("TRUNCATING TABLE TO INSERT INTO STAGING TABLE STARTS ...");
			feedsService.NewsFeedPorting();
			LOG.info("TRUNCATING TABLE TO INSERT INTO STAGING TABLE ENDS ...");

			LOG.info("Inside Tests : mailSending");
			feedsService.emptyNewsListByEmail();

			LOG.info("========================================================================================");

			LOG.info("END OF THE FEEDS BATCH PROCESS :-> "
					+ Calendar.getInstance().getTime());
		} catch (RssFeedBatchProcessException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED
					+ exception.getMessage());
		}
	}
}
