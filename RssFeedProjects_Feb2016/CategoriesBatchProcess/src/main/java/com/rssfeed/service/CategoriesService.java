package com.rssfeed.service;

import java.util.List;

import com.rssfeed.exception.BatchProcessingCategoriesException;
import com.rssfeed.process.Category;


public interface CategoriesService {
	
	public void processTruncateOperation()throws BatchProcessingCategoriesException;
	
	public void getHubCitiId()throws BatchProcessingCategoriesException ;
	
	public void categoryPorting() throws BatchProcessingCategoriesException ;
	
	public String insertImage(String name, String image, String hubCitiId) throws BatchProcessingCategoriesException ;

	public void processCategoriesDatabaseOperation(List<Category> items,String hubcitiId);
	
	

}
