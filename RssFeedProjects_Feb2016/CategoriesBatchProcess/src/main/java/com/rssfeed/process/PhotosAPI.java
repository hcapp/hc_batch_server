package com.rssfeed.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import java.util.ArrayList;

import org.json.JSONArray;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rssfeed.service.CategoriesServiceImpl;

public class PhotosAPI {

	private static final Logger LOG = LoggerFactory.getLogger(PhotosAPI.class);

	public static void readJsonFromUrl(String url, String name, String hubCitiId) {

		JSONObject json = null;
		InputStream is = null;

		JSONObject json1 = null;

		String thumbnailUrl = null;

		ArrayList<Category> items = new ArrayList<Category>();

		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"categories-service.xml");
		final CategoriesServiceImpl categoriesService = (CategoriesServiceImpl) context
				.getBean("categoriesService");

		try {

			try {
				is = new URL(url).openStream();
			} catch (MalformedURLException e) {
				LOG.error("Inside Utility : getPhotosList : " + e.getMessage());
			} catch (IOException e) {
				LOG.error("Inside Utility : getPhotosList : " + e.getMessage());
			}
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			Document doc = Jsoup.parse(jsonText);
			String th = doc.body().getElementsByTag("pre").text();

			try {
				json = new JSONObject(th);
			} catch (Exception e) {
				LOG.error("Inside Utility : readPhotosJsonFromUrl :",
						e.getMessage());
			}

			if (json != null) {

				json1 = json.getJSONObject("Response");

				if (json1 != null) {

					JSONArray array = json1.getJSONArray("Album");
					for (int i = 0; i < array.length(); i++) {

						JSONObject jsonObject = array.getJSONObject(i)
								.getJSONObject("Uris");
						String image = jsonObject.getJSONObject(
								"AlbumHighlightImage").getString("Uri");
						// https://www.smugmug.com/api/v2/album/hnKxhX!highlightimage?accept=application/json
						try {
							is = new URL("https://www.smugmug.com" + image
									+ "?accept=application/json").openStream();
						} catch (MalformedURLException e) {
							LOG.error("Inside Utility : getPhotosList : "
									+ e.getMessage());
						} catch (IOException e) {
							LOG.error("Inside Utility : getPhotosList : "
									+ e.getMessage());
						}
						BufferedReader buffReader = new BufferedReader(
								new InputStreamReader(is,
										Charset.forName("UTF-8")));
						String jsonResponseText = readAll(buffReader);
						Document document = Jsoup.parse(jsonResponseText);
						String theader = document.body()
								.getElementsByTag("pre").text();

						try {
							json = new JSONObject(theader);
							if (json != null) {

								json1 = json.getJSONObject("Response");
								if (json1 != null) {

									if (json1.getJSONObject("AlbumImage") != null) {

										JSONObject albumImage = json1
												.getJSONObject("AlbumImage");
										thumbnailUrl = albumImage
												.getString("ThumbnailUrl");

										Category item = new Category(name,
												thumbnailUrl);
										items.add(item);

										break;
									}

								}

							}

						} catch (Exception e) {
							LOG.error(
									"Inside Utility : readPhotosJsonFromUrl :",
									e.getMessage());

						}

					}

				}

			}

			categoriesService.processCategoriesDatabaseOperation(items,
					hubCitiId);
		}

		catch (Exception e) {
			LOG.error("Inside Utility : getPhotosList : " + e.getMessage());

			Category item = new Category(name, null, e.getMessage());
			items.add(item);
			try {
				categoriesService.processCategoriesDatabaseOperation(items,
						hubCitiId);
			} catch (Exception e1) {
				LOG.error("Inside Utility : getList  : " + e1.getMessage());
			}
		}

		finally {
			try {
				is.close();
			} catch (IOException e) {
				LOG.error("Inside Utility : readJsonFromUrl:", e.getMessage());
			}
		}

	}

	private static String readAll(Reader rd) {
		StringBuilder sb = new StringBuilder();
		int cp;
		try {
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
		} catch (IOException e) {

			LOG.error("Inside READALL method :", e.getMessage());

		}

		return sb.toString();
	}

}
