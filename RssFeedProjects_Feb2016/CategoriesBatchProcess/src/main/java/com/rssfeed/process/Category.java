package com.rssfeed.process;

import java.util.ArrayList;

public class Category {

	private String categoryName;
	private String categoryImagePath;
	private String HcHubCitiID;
	private String HubCitiName;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<Category> getGetHubCitiIdLst() {
		return getHubCitiIdLst;
	}

	public void setGetHubCitiIdLst(ArrayList<Category> getHubCitiIdLst) {
		this.getHubCitiIdLst = getHubCitiIdLst;
	}

	public String getHcHubCitiID() {
		return HcHubCitiID;
	}

	public void setHcHubCitiID(String hcHubCitiID) {
		HcHubCitiID = hcHubCitiID;
	}

	private ArrayList<Category> getHubCitiIdLst;

	public Category() {

	}

	public ArrayList<Category> getHubCitiIdLst() {
		return getHubCitiIdLst;
	}

	public void setItemLst(ArrayList<Category> getHubCitiIdLst) {
		this.getHubCitiIdLst = getHubCitiIdLst;
	}

	public String getHubCitiName() {
		return HubCitiName;
	}

	public void setHubCitiName(String hubCitiName) {
		this.HubCitiName = hubCitiName;
	}

	public Category(String name, String path) {
		this.categoryName = name;
		this.categoryImagePath = path;
	}

	public Category(String categoryName, String path, String message) {
		this.categoryName = categoryName;
		this.categoryImagePath = path;
		this.message = message;

	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryImagePath() {
		return categoryImagePath;
	}

	public void setCategoryImagePath(String categoryImagePath) {
		this.categoryImagePath = categoryImagePath;
	}
}
