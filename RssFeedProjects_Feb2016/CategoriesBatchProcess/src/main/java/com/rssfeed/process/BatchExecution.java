package com.rssfeed.process;

import java.util.ArrayList;
import java.util.Calendar;

import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rssfeed.dao.CategoriesDaoImpl;
import com.rssfeed.process.PropertiesReader;
import com.rssfeed.service.CategoriesService;
import com.rssfeed.service.CategoriesServiceImpl;

public class BatchExecution {

	/**
	 * Logger instance.
	 */
	private static Logger LOG = Logger
			.getLogger(BatchExecution.class.getName());

	public static void main(String[] args) {

		executeBatchingCategories();
	}

	@SuppressWarnings("resource")
	private static void executeBatchingCategories() {

		LOG.info("START OF THE CATEGORIES BATCH PROCESS :->"
				+ Calendar.getInstance().getTime());

		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"categories-service.xml");
		final CategoriesServiceImpl categoriesService = (CategoriesServiceImpl) context
				.getBean("categoriesService");

		try {

			categoriesService.processTruncateOperation();
			categoriesService.getHubCitiId();

			LOG.info("========================================================================================");
			LOG.info("CATEGORIES COPIED FROM STAGING TABLE TO RSSFEEDCATEGORY TABLE STARTS  :->"
					+ Calendar.getInstance().getTime());
			LOG.info("========================================================================================");
			categoriesService.categoryPorting();
			LOG.info("CATEGORIES COPIED FROM STAGING TABLE TO RSSFEEDCATEGORY TABLE ENDS  :->"
					+ Calendar.getInstance().getTime());
			LOG.info("========================================================================================");
		} catch (Exception e) {
			LOG.info("END OF THE BATCH PROCESS  CATEGORIES :->"
					+ Calendar.getInstance().getTime());
			LOG.info("EXCEPTION OCCURED IN TESTS : MIAN METHOD :->"
					+ e.getMessage());

		}

	}

}
