package com.rssfeed.controller;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringEscapeUtils;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class Utility {

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(Utility.class);

	public static boolean validateURL(String strUrlLink) {
		Pattern pattern = null;
		Matcher matcher;
		if (!"".equals(Utility.checkNull(strUrlLink))) {
			strUrlLink = strUrlLink.trim();
			pattern = Pattern.compile(CommonConstants.URL_PATTERN);
			matcher = pattern.matcher(strUrlLink);
			return matcher.matches();
		}
		return true;
	}

	/*
	 * public static void rescale(BufferedImage bi,String fileOutputName) throws
	 * IOException {
	 * 
	 * int originalWidth = bi.getWidth(); int originalHeight = bi.getHeight();
	 * int type = bi.getType() == 0? BufferedImage.TYPE_INT_ARGB : bi.getType();
	 * 
	 * //rescale 50% BufferedImage resizedImage = new
	 * BufferedImage(originalWidth/2, originalHeight/2, type); Graphics2D g =
	 * resizedImage.createGraphics(); g.drawImage(bi, 0, 0, originalWidth/2,
	 * originalHeight/2, null); g.dispose(); g.setComposite(AlphaComposite.Src);
	 * g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.
	 * VALUE_INTERPOLATION_BILINEAR);
	 * g.setRenderingHint(RenderingHints.KEY_RENDERING
	 * ,RenderingHints.VALUE_RENDER_QUALITY);
	 * g.setRenderingHint(RenderingHints.KEY_ANTIALIASING
	 * ,RenderingHints.VALUE_ANTIALIAS_ON);
	 * 
	 * ImageIO.write(resizedImage, "jpg", new
	 * File("C:/Users/Sam Ghazaleh/Pictures/"+fileOutputName)); new
	 * File("C:/Users/Sam Ghazaleh/Pictures/"+fileOutputName).getName(); }
	 * 
	 * public static void compress(int compression, BufferedImage bi,String
	 * fileOutputName) throws FileNotFoundException, IOException {
	 * Iterator<ImageWriter> i = ImageIO.getImageWritersByFormatName("jpeg");
	 * ImageWriter jpegWriter = i.next();
	 * 
	 * // Set the compression quality ImageWriteParam param =
	 * jpegWriter.getDefaultWriteParam();
	 * param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	 * param.setCompressionQuality(0.1f * compression);
	 * 
	 * // Write the image to a file FileImageOutputStream out = new
	 * FileImageOutputStream(new
	 * File("C:/Users/Sam Ghazaleh/Pictures/"+fileOutputName));
	 * jpegWriter.setOutput(out); jpegWriter.write(null, new IIOImage(bi, null,
	 * null), param); jpegWriter.dispose(); out.close(); }
	 */

	/*
	 * public static void resize(String inputImagePath, String outputImagePath,
	 * double percent) throws IOException { File inputFile = new
	 * File(inputImagePath); BufferedImage inputImage = ImageIO.read(inputFile);
	 * int scaledWidth = (int) (inputImage.getWidth() * percent); int
	 * scaledHeight = (int) (inputImage.getHeight() * percent);
	 * resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight); }
	 */

	/**
	 * Resizes an image to a absolute width and height (the image may not be
	 * proportional)
	 * 
	 * @param inputImagePath
	 *            Path of the original image
	 * @param outputImagePath
	 *            Path to save the resized image
	 * @param scaledWidth
	 *            absolute width in pixels
	 * @param scaledHeight
	 *            absolute height in pixels
	 * @throws IOException
	 */
	/*
	 * public static void resize(String inputImagePath, String outputImagePath,
	 * int scaledWidth, int scaledHeight) throws IOException { // reads input
	 * image File inputFile = new File(inputImagePath); BufferedImage inputImage
	 * = ImageIO.read(inputFile);
	 * 
	 * // creates output image BufferedImage outputImage = new
	 * BufferedImage(scaledWidth, scaledHeight, inputImage.getType());
	 * 
	 * // scales the input image to the output image Graphics2D g2d =
	 * outputImage.createGraphics(); g2d.drawImage(inputImage, 0, 0,
	 * scaledWidth, scaledHeight, null); g2d.dispose();
	 * 
	 * // extracts extension of output file String formatName =
	 * outputImagePath.substring(outputImagePath .lastIndexOf(".") + 1);
	 * 
	 * // writes to output file ImageIO.write(outputImage, formatName, new
	 * File(outputImagePath)); }
	 */
	/**
	 * Method to Check if the String object is null
	 * 
	 * @param strValue
	 *            String
	 * @return strValue String
	 */
	public static String checkNull(String strValue) {
		if (null == strValue || "null".equals(strValue) || "".equals(strValue)
				|| "undefined".equals(strValue)) {
			return "";
		} else {
			return strValue.trim();
		}
	}

	public static Map<String, List<Item>> groupToList(List<Item> items) {

		String sDescription = null;
		Map<String, List<Item>> citiesByCountry = new HashMap<String, List<Item>>();
		Map<String, List<Item>> citiesByCountrySorted = new TreeMap<String, List<Item>>(
				Collections.reverseOrder());

		for (Item city : items) {

			if (city.getDescription().length() > 75) {
				sDescription = city.getDescription()
						.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
						.replaceAll("&mdash", "--").replaceAll("&rsquos", "'")
						.replaceAll("&lsquos", "'").replaceAll("&39", "")
						.substring(0, 75)
						+ "...";
			} else {
				// sDescription = city.getDescription().substring(0,
				// 50).replaceAll("([^\\w\\s\'.,-:&\"<>])",
				// "").replaceAll("&mdash", "--").replaceAll("&ldquo",
				// "'").replaceAll("&rdquo", "'").replaceAll("&rsquo",
				// "'").replaceAll("&lsquo", "'").replaceAll("&39", "")+"...";
				sDescription = city.getDescription()
						.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
						.replaceAll("&mdash", "--").replaceAll("&ldquo", "'")
						.replaceAll("&rdquo", "'").replaceAll("&rsquo", "'")
						.replaceAll("&lsquo", "'").replaceAll("&39", "")
						+ "...";
			}

			city.setShortDesc(sDescription);

			List citiesForCountry = citiesByCountry
					.get(city.getPublishedDate());

			if (citiesForCountry == null) {
				citiesForCountry = new ArrayList<Item>();
				citiesByCountry.put(city.getPublishedDate(), citiesForCountry);
				citiesByCountrySorted.putAll(citiesByCountry);
			}

			citiesForCountry.add(city);
		}

		ArrayList citLst = new ArrayList(citiesByCountrySorted.entrySet());
		SortRSSFeedByDate sortByDate = new SortRSSFeedByDate();
		Collections.sort(citLst, sortByDate);

		return citiesByCountrySorted;

		/*
		 * ArrayList<HashMap<String,Item>> list=new
		 * ArrayList<HashMap<String,Item>>(); for (Item a : activityList) {
		 * HashMap<String,Item> map = new HashMap<String,Item>(); String key =
		 * a.getDate(); Item group = map.get(key); if (group == null) { group =
		 * new Item(); map.put(key, group); list.add(map); }
		 */
	}

	public static ArrayList<Category> sortPhotos(
			ArrayList<Item> hotDealResultInputList) {
		final HashMap<String, Category> hotDealsAPIInfoMap = new HashMap<String, Category>();
		Category objHotDealAPIResultSet = null;
		ArrayList<Item> hotDealsDetailsList = null;
		Item objHdDetails2 = null;
		// Category
		String key = null;
		for (Item hotDealsDetails : hotDealResultInputList) {
			key = hotDealsDetails.getPubStrtDate();
			if (hotDealsAPIInfoMap.containsKey(key)) {
				objHotDealAPIResultSet = hotDealsAPIInfoMap.get(key);
				hotDealsDetailsList = objHotDealAPIResultSet.getItemLst();
				if (hotDealsDetailsList != null) {
					Item objHdDetails = new Item();
					objHdDetails.setImagePath(hotDealsDetails.getImagePath());

					objHdDetails.setId(hotDealsDetails.getId());

					objHdDetails.setTitle(hotDealsDetails.getTitle());

					objHdDetails.setShortDesc(hotDealsDetails.getShortDesc());
					objHdDetails.setDescription(hotDealsDetails
							.getDescription());

					objHdDetails.setLink(hotDealsDetails.getLink());
					hotDealsDetailsList.add(objHdDetails);
					objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);
				}
			} else {

				objHotDealAPIResultSet = new Category();
				objHotDealAPIResultSet.setPublishedDate(hotDealsDetails
						.getPubStrtDate());
				objHotDealAPIResultSet.setDisplayDate(hotDealsDetails
						.getPublishedDate());
				objHdDetails2 = new Item();

				hotDealsDetailsList = new ArrayList<Item>();

				objHdDetails2.setImagePath(hotDealsDetails.getImagePath());

				objHdDetails2.setId(hotDealsDetails.getId());
				objHdDetails2.setTitle(hotDealsDetails.getTitle());

				objHdDetails2.setShortDesc(hotDealsDetails.getShortDesc());
				objHdDetails2.setDescription(hotDealsDetails.getDescription());

				objHdDetails2.setLink(hotDealsDetails.getLink());
				hotDealsDetailsList.add(objHdDetails2);
				objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);
			}
			hotDealsAPIInfoMap.put(key, objHotDealAPIResultSet);
		}
		final Set<Map.Entry<String, Category>> set = hotDealsAPIInfoMap
				.entrySet();

		final ArrayList<Category> hotDealsAPIInfolst = new ArrayList<Category>();

		for (Map.Entry<String, Category> entry : set) {
			hotDealsAPIInfolst.add(entry.getValue());
		}

		SortPhotosByDate objSortHDbyAPIName = new SortPhotosByDate();
		Collections.sort(hotDealsAPIInfolst, objSortHDbyAPIName);

		return hotDealsAPIInfolst;
	}

	public static ArrayList<Category> sortHotdealsByAPINames(
			ArrayList<Item> hotDealResultInputList) {
		final HashMap<String, Category> hotDealsAPIInfoMap = new HashMap<String, Category>();
		Category objHotDealAPIResultSet = null;
		ArrayList<Item> hotDealsDetailsList = null;
		Item objHdDetails2 = null;
		// Category
		String key = null;
		for (Item hotDealsDetails : hotDealResultInputList) {
			key = hotDealsDetails.getPubStrtDate();
			if (hotDealsAPIInfoMap.containsKey(key)) {
				objHotDealAPIResultSet = hotDealsAPIInfoMap.get(key);
				hotDealsDetailsList = objHotDealAPIResultSet.getItemLst();
				if (hotDealsDetailsList != null) {
					Item objHdDetails = new Item();
					objHdDetails.setImagePath(hotDealsDetails.getImagePath());

					objHdDetails.setId(hotDealsDetails.getId());

					objHdDetails.setTitle(hotDealsDetails.getTitle());

					objHdDetails.setShortDesc(hotDealsDetails.getShortDesc());
					objHdDetails.setDescription(hotDealsDetails
							.getDescription());

					objHdDetails.setLink(hotDealsDetails.getLink());
					objHdDetails.setVideoLink(hotDealsDetails.getVideoLink());
					hotDealsDetailsList.add(objHdDetails);
					objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);
				}
			} else {

				objHotDealAPIResultSet = new Category();
				objHotDealAPIResultSet.setPublishedDate(hotDealsDetails
						.getPubStrtDate());
				objHotDealAPIResultSet.setDisplayDate(hotDealsDetails
						.getPublishedDate());
				objHdDetails2 = new Item();

				hotDealsDetailsList = new ArrayList<Item>();

				objHdDetails2.setImagePath(hotDealsDetails.getImagePath());

				objHdDetails2.setId(hotDealsDetails.getId());
				objHdDetails2.setTitle(hotDealsDetails.getTitle());

				objHdDetails2.setShortDesc(hotDealsDetails.getShortDesc());
				objHdDetails2.setDescription(hotDealsDetails.getDescription());

				objHdDetails2.setLink(hotDealsDetails.getLink());
				objHdDetails2.setVideoLink(hotDealsDetails.getVideoLink());
				hotDealsDetailsList.add(objHdDetails2);
				objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);
			}
			hotDealsAPIInfoMap.put(key, objHotDealAPIResultSet);
		}
		final Set<Map.Entry<String, Category>> set = hotDealsAPIInfoMap
				.entrySet();

		final ArrayList<Category> hotDealsAPIInfolst = new ArrayList<Category>();

		for (Map.Entry<String, Category> entry : set) {
			hotDealsAPIInfolst.add(entry.getValue());
		}

		SortRSSFeedByDate objSortHDbyAPIName = new SortRSSFeedByDate();
		Collections.sort(hotDealsAPIInfolst, objSortHDbyAPIName);

		return hotDealsAPIInfolst;
	}

	/**
	 * This method is used to validate string is empty or null.
	 * 
	 * @param arg
	 *            as input parameter
	 * @return true if string is empty or null otherwise false.
	 */

	public static boolean isEmptyOrNullString(String arg) {

		final String methodName = "isEmptyString";
		final boolean emptyString;

		if (null == arg || "".equals(arg.trim())) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		return emptyString;
	}

	/**
	 * Method Parse date string in format - Sat, 03 Jan 2015 20:27:16 -0600 and
	 * return date object.
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDateFromString(String strDate) {
		Date objDate = null;
		if (null != strDate && !"".equals(strDate)) {
			try {
				objDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.parse(strDate);
			} catch (ParseException e) {
				// LOG.error("Error in parsing date. Error message: " +
				// e.getMessage());
				return null;
			}
		}
		return objDate;
	}

	public static Date getDateFromStringPhotos(String strDate) {
		Date objDate = null;
		String outputFormat = null;
		// SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
		if (null != strDate && !"".equals(strDate)) {
			try {
				objDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.parse(strDate);

			} catch (ParseException e) {
				// LOG.error("Error in parsing date. Error message: " +
				// e.getMessage());
				return null;
			}
		}
		return objDate;
	}

	public static String stripNonValidXMLCharacters(String in) {
		StringBuilder out = new StringBuilder();
		char current;

		if (in == null || ("".equals(in)))
			return "";
		for (int i = 0; i < in.length(); i++) {
			current = in.charAt(i);
			if ((current == 0x9) || (current == 0xA) || (current == 0xD)
					|| ((current >= 0x20) && (current <= 0xD7FF))
					|| ((current >= 0xE000) && (current <= 0xFFFD))
					|| ((current >= 0x10000) && (current <= 0x10FFFF)))
				out.append(current);
		}
		return out.toString();
	}

	public static ArrayList<Category> sortClassfieldsBySection(
			ArrayList<Item> hotDealResultInputList) {
		final HashMap<String, Category> hotDealsAPIInfoMap = new HashMap<String, Category>();
		Category objHotDealAPIResultSet = null;
		ArrayList<Item> hotDealsDetailsList = null;
		Item objHdDetails2 = null;
		// Category
		String key = null;
		for (Item hotDealsDetails : hotDealResultInputList) {
			key = hotDealsDetails.getSection();
			if (hotDealsAPIInfoMap.containsKey(key)) {
				objHotDealAPIResultSet = hotDealsAPIInfoMap.get(key);
				hotDealsDetailsList = objHotDealAPIResultSet.getItemLst();
				if (hotDealsDetailsList != null) {
					Item objHdDetails = new Item();
					objHdDetails.setClassification(hotDealsDetails
							.getClassification());

					objHdDetails.setAdCopy(hotDealsDetails.getAdCopy());
					hotDealsDetailsList.add(objHdDetails);

					objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);

				}
			} else {

				objHotDealAPIResultSet = new Category();
				objHotDealAPIResultSet.setSection(hotDealsDetails.getSection());

				objHdDetails2 = new Item();

				hotDealsDetailsList = new ArrayList<Item>();

				objHdDetails2.setClassification(hotDealsDetails
						.getClassification());

				objHdDetails2.setAdCopy(hotDealsDetails.getAdCopy());
				hotDealsDetailsList.add(objHdDetails2);
				objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);
			}
			hotDealsAPIInfoMap.put(key, objHotDealAPIResultSet);

		}
		final Set<Map.Entry<String, Category>> set = hotDealsAPIInfoMap
				.entrySet();

		final ArrayList<Category> hotDealsAPIInfolst = new ArrayList<Category>();

		for (Map.Entry<String, Category> entry : set) {
			// System.out.println("Key"+entry.getKey()+"Value"+entry.getValue());

			hotDealsAPIInfolst.add(entry.getValue());

		}

		SortRssFeedBySection objSortHDbyAPIName = new SortRssFeedBySection();
		Collections.sort(hotDealsAPIInfolst, objSortHDbyAPIName);

		return hotDealsAPIInfolst;
	}

	public static ArrayList<Category> sortPhotosByDate(
			ArrayList<Item> hotDealResultInputList) {
		final HashMap<String, Category> hotDealsAPIInfoMap = new HashMap<String, Category>();
		Category objHotDealAPIResultSet = null;
		ArrayList<Item> hotDealsDetailsList = null;
		Item objHdDetails2 = null;
		// Category
		String key = null;
		for (Item hotDealsDetails : hotDealResultInputList) {
			key = hotDealsDetails.getPublishedDate();
			if (hotDealsAPIInfoMap.containsKey(key)) {
				objHotDealAPIResultSet = hotDealsAPIInfoMap.get(key);
				hotDealsDetailsList = objHotDealAPIResultSet.getItemLst();
				if (hotDealsDetailsList != null) {
					Item objHdDetails = new Item();
					objHdDetails.setTitle(hotDealsDetails.getTitle());
					objHdDetails.setImagePath(hotDealsDetails.getImagePath());
					objHdDetails.setLink(hotDealsDetails.getLink());
					objHdDetails.setId(hotDealsDetails.getId());
					hotDealsDetailsList.add(objHdDetails);

					objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);

				}
			} else {

				objHotDealAPIResultSet = new Category();
				objHotDealAPIResultSet.setPublishedDate(hotDealsDetails
						.getPublishedDate());

				objHdDetails2 = new Item();

				hotDealsDetailsList = new ArrayList<Item>();

				objHdDetails2.setTitle(hotDealsDetails.getTitle());
				objHdDetails2.setLink(hotDealsDetails.getLink());
				objHdDetails2.setId(hotDealsDetails.getId());
				objHdDetails2.setImagePath(hotDealsDetails.getImagePath());
				hotDealsDetailsList.add(objHdDetails2);
				objHotDealAPIResultSet.setItemLst(hotDealsDetailsList);
			}
			hotDealsAPIInfoMap.put(key, objHotDealAPIResultSet);

		}
		final Set<Map.Entry<String, Category>> set = hotDealsAPIInfoMap
				.entrySet();

		final ArrayList<Category> hotDealsAPIInfolst = new ArrayList<Category>();

		for (Map.Entry<String, Category> entry : set) {
			// System.out.println("Key"+entry.getKey()+"Value"+entry.getValue());

			hotDealsAPIInfolst.add(entry.getValue());

		}

		SortRSSFeedByDate objSortHDbyAPIName = new SortRSSFeedByDate();
		Collections.sort(hotDealsAPIInfolst, objSortHDbyAPIName);

		return hotDealsAPIInfolst;
	}

	public static List<Item> getSaveAlotBlogList() {
		LOG.info("Inside Utility : getSaveAlotBlogList ");

		ArrayList<Item> items = new ArrayList<Item>();

		URL url = null;

		try {

			url = new URL(CommonConstants.saveamomblog);

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					String title = null;
					String lDescription = null;
					String image = null;
					String link = null;
					String date = null;
					String sDescription = null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					try {

						if (fstElmnt.getElementsByTagName("enclosure") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("enclosure");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								image = mediaurl;
								// image = mediaurl;
							}

						} else {
							image = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getsaveamomblog :  "
								+ e.getMessage());

					}

					try {

						if (fstElmnt.getElementsByTagName("link") != null) {

							NodeList linkList = fstElmnt
									.getElementsByTagName("link");
							Element linkElement = (Element) linkList.item(0);
							linkList = linkElement.getChildNodes();

							link = ((Node) linkList.item(0)).getNodeValue();

						} else {
							link = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getsaveamomblog :  "
								+ e.getMessage());

					}
					try {
						if (fstElmnt.getElementsByTagName("title") != null) {

							NodeList titleList = fstElmnt
									.getElementsByTagName("title");
							Element titleElement = (Element) titleList.item(0);
							titleList = titleElement.getChildNodes();

							title = ((Node) titleList.item(0))
									.getNodeValue()

									.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
									.replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'")
									.replaceAll("&ldquo", "\"")
									.replaceAll("&rdquo", "\"")
									.replaceAll("&mdash", "--")
									.replaceAll("&ndash", "-")
									.replaceAll("&8211", "-")
									.replaceAll("&8216", "'")
									// left single
									// quotation
									// mark
									.replaceAll("&8217", "'")
									// right single
									// quotation
									// mark
									.replaceAll("&8230", "...")
									// horizontal
									// ellipsis
									.replaceAll("&38", "&")
									// for ampersand.
									.replaceAll("&8220", "\"")
									// left double
									// quotation
									// mark
									.replaceAll("&8221", "\"")
									// right double
									// quotation
									// mark

									.replaceAll(":", "")
									.replaceAll("&amp", "&");

							title = StringEscapeUtils.unescapeHtml(title);
						} else {
							title = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getsaveamomblog :  "
								+ e.getMessage());

					}

					try {
						if (fstElmnt.getElementsByTagName("pubDate") != null) {

							NodeList dateList = fstElmnt
									.getElementsByTagName("pubDate");
							Element dateElement = (Element) dateList.item(0);
							dateList = dateElement.getChildNodes();
							String datee = ((Node) dateList.item(0))
									.getNodeValue();
							// date=getDate(datee);
							String dates[] = datee.split(" ");
							date = dates[2] + " " + dates[1] + " " + " , "
									+ dates[3];

						} else {
							date = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getsaveamomblog :  "
								+ e.getMessage());

					}

					NodeList websiteList = fstElmnt
							.getElementsByTagName("description");

					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					if (null != websiteList.item(0)) {

						if (websiteList.item(0).toString() != null) {

							lDescription = ((Node) websiteList.item(0))
									.getNodeValue()
									.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
									.replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'")
									.replaceAll("&ldquo", "\"")
									.replaceAll("&rdquo", "\"")
									.replaceAll("&mdash", "--")
									.replaceAll("&ndash", "-")
									.replaceAll("&39", "")
									.replaceAll("&8211", ".")
									.replaceAll("&8216", "'")
									.replaceAll("&amp", "&")
									// left single
									// quotation
									// mark
									.replaceAll("&8217", "'") // right
																// single
																// quotation
																// mark
									.replaceAll("&8230", "...") // horizontal
																// ellipsis
									.replaceAll("&38", "&") // for
															// ampersand.
									.replaceAll("&8220", "\"")// left double
																// quotation
																// mark
									.replaceAll("&8221", "\""); // right
																// double
																// quotation
																// mark

							sDescription = StringEscapeUtils
									.unescapeHtml(sDescription);

						}

						Item item = new Item(title, lDescription, image, link,
								date);

						item.setId(i + 1);
						items.add(item);

					}
				}
			}

		} catch (Exception e) {
			LOG.error("Inside Utility : getSaveAlotBlogList :  "
					+ e.getMessage());
			Item item = new Item(null, null, null, null, null);
			items.add(item);

		}

		LOG.info("Exit Utility : getsaveamomblog ");
		return items;

	}

	private static String readAll(Reader rd) {
		StringBuilder sb = new StringBuilder();
		int cp;
		try {
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
		} catch (IOException e) {

			LOG.error("Inside READALL method :", e.getMessage());

		}

		return sb.toString();
	}

	public static String formatDate(
			Map<String, List<Item>> citiesByCountrySorted) {

		String newstring = null;

		for (Map.Entry<String, List<Item>> entry : citiesByCountrySorted
				.entrySet()) {

			String key = entry.getKey();

			try {
				Date date = new SimpleDateFormat("dd/mmm YYYY").parse(key);
				newstring = new SimpleDateFormat("MM-dd-YYYY").format(date);
				// System.out.println(newstring); // 2011-01-18
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return newstring;

	}

	public static Item getValues(Item item) {
		Item items = null;
		if (item.getId() > 0) {
			items.setId(item.getId());
			items.setImage(item.getImage());
			items.setTitle(item.getTitle());
			items.setDescription(item.getDescription());
		}
		return items;
	}

	public static String getMP4RssFeedVideo(long videoId) {
		String strMethodName = "getMP4RssFeedVideo";
		LOG.info(CommonConstants.METHODSTART + strMethodName);
		String strVideo = null;
		// Fetch MP4 video by passing video id to below URL

		String url = "http://api.brightcove.com/services/library?command=find_video_by_id&video_fields=name,length,FLVURL&media_delivery=http&token=x95LXczyNI5-G9kX0cjsHM9edPFzaKFTE4PANJ7L2rQfuF-swGUxJg.."
				+ "&video_id=" + videoId;

		try {

			InputStream is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);

			strVideo = json.getString("FLVURL");

		} catch (Exception exception) {
			LOG.info("Excpetion occured in Utility: getMP4RssFeedVideo"
					+ exception.getMessage());
		}
		LOG.info(CommonConstants.METHODEND + strMethodName);
		// System.out.println(CommonConstants.METHODEND + strMethodName);
		return strVideo;
	}

	public static String getDate(String s) {
		String currentDate = null;
		try {
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = df.parse(s);
			currentDate = df.format(date1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentDate;

	}

}
