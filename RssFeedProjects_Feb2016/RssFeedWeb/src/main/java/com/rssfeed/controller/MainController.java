package com.rssfeed.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.taglibs.standard.lang.jstl.parser.ParseException;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

	private static final Logger LOG = LoggerFactory
			.getLogger(MainController.class);

	private JdbcCustomerDAO customerDAO;

	public void setCustomerDAO(JdbcCustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	/**
	 * This method is used to fetch rss feeds based on feedtype.
	 * 
	 * @param item
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return feed list details
	 */
	@RequestMapping(value = "/videos.htm", method = RequestMethod.GET)
	public ModelAndView getVideos(@ModelAttribute("frontpageform") Item item,
			BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {

		final String strMethodName = "getVideos";

		LOG.info(CommonConstants.METHODSTART + strMethodName);
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("categoryName");

		if (!Utility.isEmptyOrNullString(item.getFeedType())) {

			ApplicationContext app = new ClassPathXmlApplicationContext(
					"database-config.xml");

			JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
					.getBean("customerDAO");

			ArrayList<Item> items;
			Category objCategory = null;
			try {
				objCategory = customerDAO.fetchAllFeeds(item.getFeedType(),
						item.getHcHubCitiID());
				items = objCategory.getItemLst();

				if (null != items && !items.isEmpty()) {

					session.setAttribute("videosList", items);

					session.setAttribute("defaultImage",
							CommonConstants.DEFAULTTYLERIMAGEPTH);
					session.setAttribute("categoryName", item.getFeedType()
							.toUpperCase());
					Item objItem = new Item();
					objItem.setItems(items);

				} else {

					LOG.info("Item list is empty :");
					session.setAttribute("videosList", null);

					request.setAttribute("noregfound", "You have no notifications at this time");
					return new ModelAndView("error");
				}
			}

			catch (Exception e) {
				LOG.error("Inside MainController : getVideos", e.getMessage());
			}
		} else {
			LOG.info("Feed type is null or empty");
			request.setAttribute("feedmessage", "No feed type");
		}
		model.put("frontpageform", item);

		LOG.info(CommonConstants.METHODEND + strMethodName);
		return new ModelAndView("indextest");

	}

	/**
	 * This method is used to fetch rss feeds based on feedtype.
	 * 
	 * @param item
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return feed list details
	 */
	@RequestMapping(value = "/photos.htm", method = RequestMethod.GET)
	public ModelAndView getPhotos(@ModelAttribute("frontpageform") Item item,
			BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {

		final String strMethodName = "getPhotos";

		LOG.info(CommonConstants.METHODSTART + strMethodName);
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("weather");
		session.removeAttribute("crime");
		session.removeAttribute("opinion");
		session.removeAttribute("business");
		session.removeAttribute("topstories");
		session.removeAttribute("obits");
		session.removeAttribute("worldnews");
		session.removeAttribute("city");
		session.removeAttribute("photosList");
		session.removeAttribute("categoryName");

		if (!Utility.isEmptyOrNullString(item.getFeedType())) {

			Category items = null;
			ArrayList<Item> photosList = null;

			try {

				ApplicationContext app = new ClassPathXmlApplicationContext(
						"database-config.xml");

				JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
						.getBean("customerDAO");

				items = customerDAO.fetchAllFeeds(item.getFeedType(),
						item.getHcHubCitiID());

				photosList = items.getItemLst();

				if (null != photosList && !photosList.isEmpty()) {

					ArrayList<Category> ctLst = null;
					ctLst = Utility.sortHotdealsByAPINames(photosList);
					/*
					 * for(Category i: ctLst){ DateFormat outputDateFormat=new
					 * SimpleDateFormat("MMM dd, yyyy"); DateFormat
					 * inputDateFormat=new
					 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); Date
					 * dateOuput=inputDateFormat.parse(i.getPublishedDate());
					 * String publishedDaate =
					 * outputDateFormat.format(dateOuput);
					 * i.setPublishedDate(publishedDaate); ctLst.add(i); }
					 */

					session.setAttribute("photosList", ctLst);

					session.setAttribute("defaultImage",
							CommonConstants.DEFAULTTYLERIMAGEPTH);
					session.setAttribute("categoryName", item.getFeedType()
							.toUpperCase());
				} else {

					LOG.info("Item list is empty :");
					session.setAttribute("photosList", null);

					request.setAttribute("noregfound", "You have no notifications at this time");
					return new ModelAndView("error");
				}
			}

			catch (Exception e) {
				LOG.error("Inside MainController : getPhotos", e.getMessage());
			}
		} else {
			LOG.info("Feed type is null or empty");
			request.setAttribute("feedmessage", "No feed type");
		}
		model.put("frontpageform", item);

		LOG.info(CommonConstants.METHODEND + strMethodName);
		return new ModelAndView("indextest");

	}

	@RequestMapping(value = "/getlist.htm", method = RequestMethod.GET)
	public ModelAndView getListSaveAMom(ModelMap map, HttpSession session,
			HttpServletRequest request) {
		LOG.info("Inside MainController : getListSaveAMom ");

		List<Item> items = null;
		session.removeAttribute("photosList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		session.removeAttribute("categoryName");

		try {
			items = Utility.getSaveAlotBlogList();

			if (items != null) {

				session.setAttribute("newsList", items);
				session.setAttribute("defaultImage",
						CommonConstants.DEFAULTKILLEENIMAGEPATH);

			} else {
				session.setAttribute("newsList", null);
				request.setAttribute("noregfound", "You have no notifications at this time");
				return new ModelAndView("error");
			}
		} catch (Exception e) {
			LOG.error("Inside getListSaveAMom : " + e.getMessage());

		}

		return new ModelAndView("savealotmom");
	}

	@RequestMapping(value = "/newsdetail.htm", method = RequestMethod.POST)
	public ModelAndView getDetails(@ModelAttribute("newslistform") Item item,
			BindingResult result, ModelMap map, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) {
		LOG.info("Inside MainController : getDetails ");

		session.removeAttribute("photosList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		session.removeAttribute("topList");
		session.removeAttribute("categoryName");

                response.setHeader("X-XSS-Protection", "0");
		if (item != null) {

			if (item.getImage() != null) {
				request.setAttribute("image", item.getImage());
			}

			if (item.getTitle() != null) {
				request.setAttribute("title", item.getTitle());
			}

			if (item.getDescription() != null) {
				request.setAttribute("description", item.getDescription());
			}
			
			if (item.getVideoLink() != null) {
				request.setAttribute("medialink", item.getVideoLink());
			}

			if (item.getHcHubCitiID().equalsIgnoreCase(
					CommonConstants.RockwallHubCitiId)
					|| item.getHcHubCitiID().equalsIgnoreCase(
							CommonConstants.killeenHubcitiId)) {

				if (item.getLink() != null) {
					request.setAttribute("link", item.getLink());
				}
			}

		}

		LOG.info("Exit MainController : getDetails ");
		return new ModelAndView("newsdetail");
	}

	@RequestMapping(value = "/photosdetail.htm", method = RequestMethod.POST)
	public ModelAndView getPhotosDetails(
			@ModelAttribute("photosform") Item item, BindingResult result,
			ModelMap map, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) {
		LOG.info("Inside MainController : getPhotosDetails ");

		session.removeAttribute("photosList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		session.removeAttribute("topList");
		session.removeAttribute("categoryName");
		if (item != null) {

			if (item.getImage() != null) {
				request.setAttribute("image", item.getImage());
			}

			if (item.getTitle() != null) {
				request.setAttribute("title", item.getTitle());
			}

			if (item.getLink() != null) {
				request.setAttribute("link", item.getLink());
			}

		}

		LOG.info("Exit MainController : getDetails ");
		return new ModelAndView("photosdetail");
	}

	@RequestMapping(value = "/videosdetail.htm", method = RequestMethod.POST)
	public ModelAndView getVideoDetails(@ModelAttribute("videoform") Item item,
			BindingResult result, ModelMap map, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) {

		final String strMethodName = "getVideoDetails";

		LOG.info(CommonConstants.METHODSTART + strMethodName);
		session.removeAttribute("photosList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		if (item != null) {

			LOG.info("link" + item.getLink());

			if (item.getLink() != null) {
				request.setAttribute("link", item.getLink());

			}
		}

		LOG.info(CommonConstants.METHODEND + strMethodName);
		return new ModelAndView("videoss");

	}

	@RequestMapping(value = "/home.htm", method = RequestMethod.GET)
	public ModelAndView getdata(@ModelAttribute("frontpageform") Item item,
			BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		LOG.info("Inside MainController : getdata");

		final String strViewName = "indextest";
		session.removeAttribute("photosList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		session.removeAttribute("topList");
		session.removeAttribute("weather");
		session.removeAttribute("crime");
		session.removeAttribute("opinion");
		session.removeAttribute("business");
		session.removeAttribute("topstories");
		session.removeAttribute("obits");
		session.removeAttribute("worldnews");
		session.removeAttribute("city");
		session.removeAttribute("categoryName");

		ApplicationContext app = new ClassPathXmlApplicationContext(
				"database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
				.getBean("customerDAO");

		Category itemd;
		Category objCategory = null;

		ArrayList<Item> feedsList;
		List<Item> topnewslst;

		try {

			objCategory = customerDAO.fetchAllFeeds(item.getFeedType(),
					item.getHcHubCitiID());

			feedsList = objCategory.getItemLst();

			topnewslst = objCategory.getTopNewsLst();

			if (null != topnewslst && !topnewslst.isEmpty()) {
				for (Item i : topnewslst) {
					i.setShortDesc(Jsoup.parse(i.getShortDesc()).text());
				}
				session.setAttribute("topList", topnewslst);

				session.setAttribute("defaultImage",
						CommonConstants.DEFAULTTYLERIMAGEPTH);
				session.setAttribute("HubCitiID", item.getHcHubCitiID());
				session.setAttribute("categoryName", item.getFeedType()
						.toUpperCase());

			}

			if (null != feedsList && !feedsList.isEmpty()) {
				for (Item i : feedsList) {
					i.setShortDesc(Jsoup.parse(i.getShortDesc()).text());
				}
				ArrayList<Category> ctLst = null;
				ctLst = Utility.sortHotdealsByAPINames(feedsList);

				session.setAttribute("itemsList", ctLst);

				session.setAttribute("categoryName", item.getFeedType()
						.toUpperCase());
				
				if (item.getFeedType().equalsIgnoreCase("All")) {
					session.setAttribute("categoryName", item.getFeedType()
							.toUpperCase() + "\t" + "ARTICLES");
				}
				if (item.getFeedType().equalsIgnoreCase("Top")) {
					session.setAttribute("categoryName", item.getFeedType()
							.toUpperCase() + "\t" + "NEWS");
				}
				if (item.getHcHubCitiID().equalsIgnoreCase(
						CommonConstants.RockwallHubCitiId)) {

					session.setAttribute("defaultImage",
							CommonConstants.DEFAULTROCKWALLIMAGEPTH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
					session.setAttribute("categoryName", item.getFeedType()
							.toUpperCase());
				} else if (item.getHcHubCitiID().equalsIgnoreCase(
						CommonConstants.AustinHubCitiId)) {

					session.setAttribute("defaultImage",
							CommonConstants.DEFAULTAUSTINIMAGEPTH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
					session.setAttribute("categoryName", item.getFeedType()
							.toUpperCase());
				}

				else if (item.getHcHubCitiID().equalsIgnoreCase(
						CommonConstants.killeenHubcitiId)) {

					session.setAttribute("defaultImage",
							CommonConstants.DEFAULTKILLEENIMAGEPATH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
					session.setAttribute("city", "KDH");
					if (item.getFeedType().equalsIgnoreCase("weather")) {
						session.setAttribute("weather",
								CommonConstants.weatherLink);
					} else if (item.getFeedType().equalsIgnoreCase("crime")) {
						session.setAttribute("crime", CommonConstants.crimeLink);
					} else if (item.getFeedType().equalsIgnoreCase("opinion")) {
						session.setAttribute("opinion",
								CommonConstants.opinionLink);
					} else if (item.getFeedType().equalsIgnoreCase("business")) {
						session.setAttribute("business",
								CommonConstants.businessLink);
					} else if (item.getFeedType()
							.equalsIgnoreCase("topstories")) {
						session.setAttribute("topstories",
								CommonConstants.topnewsLink);
					}

					else if (item.getFeedType().equalsIgnoreCase("obits")) {
						session.setAttribute("obits", CommonConstants.obitsLink);
					}

					else if (item.getFeedType().equalsIgnoreCase("worldnews")) {
						session.setAttribute("worldnews",
								CommonConstants.worldnewsLink);
					}
					session.setAttribute("categoryName", item.getFeedType()
							.toUpperCase());
				}

				else {

					session.setAttribute("defaultImage",
							CommonConstants.DEFAULTTYLERIMAGEPTH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
				}
			}

			else {

				LOG.info("Item list is empty :");
				session.setAttribute("feedsList", null);
				request.setAttribute("noregfound", "You have no notifications at this time");
				return new ModelAndView("error");
			}

		} catch (Exception e) {
			LOG.error("Inside MainController : getdata : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getdata");
		return new ModelAndView(strViewName);

	}

	@RequestMapping(value = "/hello.htm", method = RequestMethod.GET)
	public ModelAndView getHello(HttpServletRequest request,
			HttpSession session, ModelMap model) {
		return new ModelAndView("hello");

	}

	@RequestMapping(value = "/homepage.htm", method = RequestMethod.GET)
	public ModelAndView getHomeData(HttpServletRequest request,
			HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getHomeData");
		session.removeAttribute("photosList");
		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");

		ApplicationContext app = new ClassPathXmlApplicationContext(
				"database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
				.getBean("customerDAO");
		Category items;
		String hubcitiId = null;
		ArrayList<Category> itemsList;
		try {
			itemsList = customerDAO.getHcHubCitiID();
			try {
				for (int i = 0; i < itemsList.size(); i++) {
					if (itemsList
							.get(i)
							.getHubCitiName()
							.equalsIgnoreCase(
									CommonConstants.RockwallHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCiti"
						+ e.getMessage());
			}
			LOG.info("HubCitiId Rockwall" + hubcitiId);
			itemsList = customerDAO.fetchAllImages(hubcitiId);

			if (null != itemsList) {
				session.setAttribute("HcHubCitiID", hubcitiId);
				session.setAttribute("itemsList", itemsList);

				for (int i = 0; i < itemsList.size(); i++) {
					/*
					 * String inputImagePath =
					 * "C:/Users/Sam Ghazaleh/Pictures/tea-paty-flyer.jpg";
					 * String outputImagePath2 =
					 * "C:/Users/Sam Ghazaleh/Pictures/tea-paty-flyer_resized.jpg"
					 * ; try {
					 * 
					 * 
					 * // resize smaller by 50% double percent = 0.5;
					 * Utility.resize(inputImagePath, outputImagePath2,
					 * percent);
					 * 
					 * 
					 * 
					 * } catch (IOException ex) {
					 * System.out.println("Error resizing the image.");
					 * ex.printStackTrace(); }
					 */

					if ("good times News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("events", itemsList.get(i)
								.getImagePath());
					} else if ("good cause News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("cause", itemsList.get(i)
								.getImagePath());
					} else if ("good living News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("living", itemsList.get(i)
								.getImagePath());
					} else if ("good culture News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("culture", itemsList.get(i)
								.getImagePath());
					} else if ("good people News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("people", itemsList.get(i)
								.getImagePath());
					} else if ("good pets News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("pets", itemsList.get(i)
								.getImagePath());
					}

					else if ("good faith News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("faith", itemsList.get(i)
								.getImagePath());
					} else if ("good thinking News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("thinking", itemsList.get(i)
								.getImagePath());
					} else if ("good business News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("business", itemsList.get(i)
								.getImagePath());
					} else if ("good sports News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("sports", itemsList.get(i)
								.getImagePath());
					} else if ("good neighbours News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("health", itemsList.get(i)
								.getImagePath());
					} else if ("columnists News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("columnists", itemsList.get(i)
								.getImagePath());
					}
				}

			} else {
				session.setAttribute("imagesList", null);
				request.setAttribute("noregfound", "You have no notifications at this time");
				return new ModelAndView("error");
			}

		} catch (RssFeedWebSqlException e) {
			LOG.error("Inside MainController : getFrontData : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getFrontData");
		return new ModelAndView("homepage");
	}

	@RequestMapping(value = "/frontpage.htm", method = RequestMethod.GET)
	public ModelAndView getFrontData(HttpServletRequest request,
			HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getFrontData");
		session.removeAttribute("photosList");
		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("weather");
		session.removeAttribute("food");
		session.removeAttribute("entertainment");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");

		ApplicationContext app = new ClassPathXmlApplicationContext(
				"database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
				.getBean("customerDAO");

		String hubcitiId = null;
		ArrayList<Category> itemsList;

		try {
			itemsList = customerDAO.getHcHubCitiID();
			try {
				for (int i = 0; i < itemsList.size(); i++) {

					if (itemsList.get(i).getHubCitiName()
							.equalsIgnoreCase(CommonConstants.TylerHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
				LOG.info("HubCitiId Tyler in try" + hubcitiId);
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCiti"
						+ e.getMessage());
				LOG.info("HubCitiId Tyler in catch" + hubcitiId);

			}
			LOG.info("HubCitiId Tyler" + hubcitiId);
			itemsList = customerDAO.fetchAllImages(hubcitiId);

			if (null != itemsList) {
				session.setAttribute("HcHubCitiID", hubcitiId);
				session.setAttribute("itemsList", itemsList);

				for (int i = 0; i < itemsList.size(); i++) {

					if ("sports News"
							.equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("sports", itemsList.get(i)
								.getImagePath());
					} else if ("health News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("health", itemsList.get(i)
								.getImagePath());
					} else if ("top News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("top", itemsList.get(i)
								.getImagePath());
					}

					else if ("life News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("life", itemsList.get(i)
								.getImagePath());
					} else if ("opinion News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("opinion", itemsList.get(i)
								.getImagePath());
					}

					else if ("entertainment News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("entertainment", itemsList.get(i)
								.getImagePath());
					} else if ("business News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("business", itemsList.get(i)
								.getImagePath());
					} else if ("food News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("food", itemsList.get(i)
								.getImagePath());
					} else if ("weather News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("weather", itemsList.get(i)
								.getImagePath());
					} else if ("all News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("all", itemsList.get(i)
								.getImagePath());

					} else if ("videos News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("videos", itemsList.get(i)
								.getImagePath());
					} else if ("photos News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("photos", itemsList.get(i)
								.getImagePath());
					}

				}

			} else {
				session.setAttribute("imagesList", null);
				request.setAttribute("noregfound", "You have no notifications at this time");
				return new ModelAndView("error");
			}

		} catch (RssFeedWebSqlException e) {
			LOG.error("Inside MainController : getFrontData : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getFrontData");
		return new ModelAndView("frontpagetest");
	}

	@RequestMapping(value = "/frontpagetest.htm", method = RequestMethod.GET)
	public ModelAndView getFrontDatatest(HttpServletRequest request,
			HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getFrontData");
		session.removeAttribute("photosList");
		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");

		ApplicationContext app = new ClassPathXmlApplicationContext(
				"database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
				.getBean("customerDAO");

		String hubcitiId = null;
		ArrayList<Category> itemsList;

		try {
			itemsList = customerDAO.getHcHubCitiID();
			try {
				for (int i = 0; i < itemsList.size(); i++) {

					if (itemsList.get(i).getHubCitiName()
							.equalsIgnoreCase(CommonConstants.TylerHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
				LOG.info("HubCitiId Tyler in try" + hubcitiId);
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCiti"
						+ e.getMessage());
				LOG.info("HubCitiId Tyler in catch" + hubcitiId);

			}
			LOG.info("HubCitiId Tyler" + hubcitiId);
			itemsList = customerDAO.fetchAllImages(hubcitiId);

			if (null != itemsList) {
				session.setAttribute("HcHubCitiID", hubcitiId);
				session.setAttribute("itemsList", itemsList);

				for (int i = 0; i < itemsList.size(); i++) {

					if ("sports News"
							.equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("sports", itemsList.get(i)
								.getImagePath());
					} else if ("health News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("health", itemsList.get(i)
								.getImagePath());
					} else if ("businessnew News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("businessnew", itemsList.get(i)
								.getImagePath());
					} else if ("topnew News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("topnew", itemsList.get(i)
								.getImagePath());
					} else if ("life News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("life", itemsList.get(i)
								.getImagePath());
					} else if ("opinionnew News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("opinionnew", itemsList.get(i)
								.getImagePath());
					} else if ("opinion News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("opinion", itemsList.get(i)
								.getImagePath());
					}

					else if ("entertainment News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("entertainment", itemsList.get(i)
								.getImagePath());
					} else if ("food News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("food", itemsList.get(i)
								.getImagePath());
					} else if ("weather News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("weather", itemsList.get(i)
								.getImagePath());
					} else if ("all News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("all", itemsList.get(i)
								.getImagePath());
					} else if ("videos News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("videos", itemsList.get(i)
								.getImagePath());
					} else if ("photos News".equals(itemsList.get(i)
							.getCategoryName())) {
						session.setAttribute("photos", itemsList.get(i)
								.getImagePath());
					}

				}

			} else {
				session.setAttribute("imagesList", null);
				request.setAttribute("noregfound", "You have no notifications at this time");
				return new ModelAndView("error");
			}

		} catch (RssFeedWebSqlException e) {
			LOG.error("Inside MainController : getFrontData : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getFrontData");
		return new ModelAndView("frontpagetest");
	}

	// Request mapping for Austin

	@RequestMapping(value = "/firstpage.htm", method = RequestMethod.GET)
	public ModelAndView getFirstPageData(HttpServletRequest request,
			HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getFirstPageData");
		session.removeAttribute("photosList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");

		String hubcitiId = null;
		ArrayList<Category> itemsList;
		ApplicationContext app = new ClassPathXmlApplicationContext(
				"database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
				.getBean("customerDAO");

		try {
			itemsList = customerDAO.getHcHubCitiID();

			try {
				for (int i = 0; i < itemsList.size(); i++) {
					if (itemsList
							.get(i)
							.getHubCitiName()
							.equalsIgnoreCase(CommonConstants.KillenHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCitiInner"
						+ e.getMessage());
			}
			session.setAttribute("HcHubCitiID", hubcitiId);
			LOG.info("HubCitiId Austin" + hubcitiId);
		} catch (RssFeedWebSqlException e1) {
			LOG.info("Excpetion occured in Utility: getHubCitiOuter"
					+ e1.getMessage());
		}

		LOG.info("Exit MainController : getFirstPageData");
		return new ModelAndView("firstpage");
	}

	// Request mapping for Austin

	@RequestMapping(value = "/mainpage.htm", method = RequestMethod.GET)
	public ModelAndView getMainPageData(HttpServletRequest request,
			HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getMainPageData");
		session.removeAttribute("photosList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");

		String hubcitiId = null;
		ArrayList<Category> itemsList;
		ApplicationContext app = new ClassPathXmlApplicationContext(
				"database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
				.getBean("customerDAO");

		try {
			itemsList = customerDAO.getHcHubCitiID();

			try {
				for (int i = 0; i < itemsList.size(); i++) {
					if (itemsList
							.get(i)
							.getHubCitiName()
							.equalsIgnoreCase(CommonConstants.AustinHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCitiInner"
						+ e.getMessage());
			}
			session.setAttribute("HcHubCitiID", hubcitiId);
			LOG.info("HubCitiId Austin" + hubcitiId);
		} catch (RssFeedWebSqlException e1) {
			LOG.info("Excpetion occured in Utility: getHubCitiOuter"
					+ e1.getMessage());
		}

		LOG.info("Exit MainController : getMainPageData");
		return new ModelAndView("mainpage");
	}

	@RequestMapping(value = "/employment.htm", method = RequestMethod.GET)
	public ModelAndView getEmployment(
			@ModelAttribute("frontpageform") Item item, BindingResult result,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		LOG.info("Inside MainController : employment");

		final String strViewName = "indextest";

		// LOG.info(CommonConstants.METHODSTART +strMethodName );

                session.removeAttribute("photosList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		session.removeAttribute("topList");
		session.removeAttribute("weather");
		session.removeAttribute("crime");
		session.removeAttribute("opinion");
		session.removeAttribute("business");
		session.removeAttribute("topstories");
		session.removeAttribute("obits");
		session.removeAttribute("worldnews");
		session.removeAttribute("city");
		session.removeAttribute("categoryName");



		

		ApplicationContext app = new ClassPathXmlApplicationContext(
				"database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
				.getBean("customerDAO");
		ArrayList<Item> classifieldsList;

		Category objCategory;

		try {

			objCategory = customerDAO.fetchAllFeeds(item.getFeedType(),
					item.getHcHubCitiID());

			classifieldsList = objCategory.getItemLst();

			if (classifieldsList != null && !classifieldsList.isEmpty()) {

				ArrayList<Category> classifieldsSortedLst = null;
				classifieldsSortedLst = Utility
						.sortClassfieldsBySection(classifieldsList);

				session.setAttribute("employmentList", classifieldsSortedLst);
				session.setAttribute("categoryName", item.getFeedType()
						.toUpperCase());

				session.setAttribute("defaultImage",
						CommonConstants.DEFAULTTYLERIMAGEPTH);

			} else {
				session.setAttribute("employmentList", null);
				request.setAttribute("noregfound", "You have no notifications at this time");
				return new ModelAndView("error");
			}

		} catch (Exception e) {
			LOG.info("Inside employment : " + e);
			e.printStackTrace();
		}
		return new ModelAndView(strViewName);
	}

}
