package com.rssfeed.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PhotosAPI {

	private static final Logger LOG = LoggerFactory.getLogger(Utility.class);

	public static ArrayList<Item> readJsonFromUrl(String url) {

		JSONObject json = null;
		InputStream is = null;
		String title = null;
		JSONObject json1 = null;
		String webUri = null;
		String date = null;
		ArrayList<Item> items = new ArrayList<Item>();

		try {
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			Document doc = Jsoup.parse(jsonText);
			String th = doc.body().getElementsByTag("pre").text();

			try {
				json = new JSONObject(th);
				if (json != null) {

					json1 = json.getJSONObject("Response");

					if (json1 != null) {

						JSONArray array = json1.getJSONArray("Album");
						for (int i = 0; i < array.length(); i++) {

							if (array.getJSONObject(i).getString("Title") != null) {
								title = array
										.getJSONObject(i)
										.getString("Title")
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll(":", "");

								// System.out.println("Title for photos" +
								// title);
							}
							if (array.getJSONObject(i).getString("WebUri") != null) {
								webUri = array.getJSONObject(i).getString(
										"WebUri");

								// System.out.println("WebUri for photos" +
								// webUri);
							}

							Item item = new Item(title, webUri);
							item.setId(i + 1);
							items.add(item);

						}
					}

				}

			} catch (JSONException e) {
				Item item = new Item(null, null);
				items.add(item);
				LOG.error("Inside Utility : readPhotosJsonFromUrl  : "
						+ e.getMessage());

			}

		} catch (MalformedURLException e) {
			LOG.error("Inside Utility : readPhotosJsonFromUrl :",
					e.getMessage());
		} catch (IOException ex) {
			LOG.error("Inside Utility : readPhotosJsonFromUrl  : "
					+ ex.getMessage());

		}

		finally {
			try {
				is.close();
			} catch (IOException e) {
				LOG.error("Inside Utility : readJsonFromUrl:", e.getMessage());
			}
		}
		return items;

	}

	private static String readAll(Reader rd) {
		StringBuilder sb = new StringBuilder();
		int cp;
		try {
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
		} catch (IOException e) {

			LOG.error("Inside READALL method :", e.getMessage());

		}

		return sb.toString();
	}

}
