package com.rssfeed.controller;

import java.util.Comparator;

public class SortRssFeedBySection implements Comparator<Category> {

	public int compare(Category arg0, Category arg1) {

		String section1, section2;
		Integer sectionCompare = 0;
		try {
			section1 = arg0.getSection();
			section2 = arg1.getSection();

			if (null != section1 && null != section2) {
				sectionCompare = section1.compareTo(section2);
			} else if (null == section2 && null != section1) {
				// LOG.error("PubDate is empty for title: " + item1.getTitle() +
				// ", link: " + item1.getLink());
				sectionCompare = 1;
			} else if (null == section1 && null != section2) {
				sectionCompare = -1;
				// LOG.error("PubDate is empty for title: " + item2.getTitle() +
				// ", link: " + item2.getLink());
			} else {
				sectionCompare = 0;
				if (sectionCompare == 0) {
					sectionCompare = arg0.getClassification().compareTo(
							arg1.getClassification());
				}
				// LOG.error("PubDate is empty for title: " + item1.getTitle() +
				// ", " + item2.getTitle() + ", link: " + item1.getLink() + ", "
				// + item2.getLink());
			}
		} catch (Exception e) {
			// LOG.error("Exception in SortRSSFeedByDate: Message: " +
			// e.getMessage());
		}
		return sectionCompare;
	}

}
