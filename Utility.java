package com.realestate.batch.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

//import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.realesate.batch.dao.RealEstateDAO;
import com.realesate.batch.dao.RealEstateDAOImpl;
import com.realestate.batch.pojo.Item;
import com.realestate.batch.process.RssFeedBatchProcessException;
import com.realestate.batch.service.RssFeedServiceImpl;

public class Utility {

	private static Logger LOG = LoggerFactory.getLogger(Utility.class.getName());

	RssFeedServiceImpl service = new RssFeedServiceImpl();

	static RealEstateDAO realEstateDAOImpl = new RealEstateDAOImpl();

	public static boolean validateURL(String strUrlLink) {
		Pattern pattern = null;
		Matcher matcher;
		if (!"".equals(Utility.checkNull(strUrlLink))) {
			strUrlLink = strUrlLink.trim();
			pattern = Pattern.compile(CommonConstants.URL_PATTERN);
			matcher = pattern.matcher(strUrlLink);
			return matcher.matches();
		}
		return true;
	}

	/**
	 * Method to Check if the String object is null
	 * 
	 * @param strValue
	 *            String
	 * @return strValue String
	 */
	public static String checkNull(String strValue) {
		if (null == strValue || "null".equals(strValue) || "".equals(strValue)
				|| "undefined".equals(strValue)) {
			return "";
		} else {
			return strValue.trim();
		}
	}

/*	public static ArrayList<Item> getEmployment(String name) {
		LOG.info("Inside Utility : getEmployment ");

		ArrayList<Item> empItems = new ArrayList<Item>();
		RssFeedServiceImpl service = new RssFeedServiceImpl();
		URL url = null;

		try {

			if (name.equalsIgnoreCase("employment")) {
				url = new URL(CommonConstants.employment);
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					String tit = null;
					String longDesc = null;
					String category = null;
					String link = null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					NodeList linkList = fstElmnt.getElementsByTagName("link");
					Element linkElement = (Element) linkList.item(0);
					linkList = linkElement.getChildNodes();
					if (validateURL(((Node) linkList.item(0)).getNodeValue())) {
						link = ((Node) linkList.item(0)).getNodeValue();
					}

					NodeList titleList = fstElmnt.getElementsByTagName("title");
					Element titleElement = (Element) titleList.item(0);
					titleList = titleElement.getChildNodes();

					tit = ((Node) titleList.item(0)).getNodeValue()
							.replaceAll("[^\\w\\s\'.,&:\"]", "")
							.replaceAll("&rsquo", "'")
							.replaceAll("&lsquo", "'")
							.replaceAll("&ldquo", "\"")
							.replaceAll("&rdquo", "\"")
							.replaceAll("&mdash", "--")
							.replaceAll("&ndash", "-").replaceAll(":", "");

					NodeList websiteList = fstElmnt
							.getElementsByTagName("description");

					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					longDesc = ((Node) websiteList.item(0)).getNodeValue()
							.replaceAll("&rsquo", "'")
							.replaceAll("&lsquo", "'")
							.replaceAll("&ldquo", "\"")
							.replaceAll("&rdquo", "\"")
							.replaceAll("&mdash", "--")
							.replaceAll("&ndash", "-")
							.replaceAll("([^\\w\\s\'.,-:&\"])", "");

					*//**
					 * longDesc = websiteList.item(0).toString(); if (null !=
					 * longDesc && !"".equals(longDesc)) { longDesc =
					 * longDesc.replace("[#text:", ""); longDesc =
					 * longDesc.substring(0, longDesc.length() - 1).trim(); }
					 *//*

					Item item = new Item(tit, longDesc, null, link, category, null);
					item.setId(i + 1);
					empItems.add(item);
				}
			}

			service.processDatabaseOperation(empItems, name,  CommonConstants.TylerHubCitiId);
		}

		catch (Exception e) {
			LOG.info("Inside Utility : getEmployment  : " + e);
		}

		LOG.info("Exit Utility : getEmployment ");
		return empItems;
	}*/

	public static List<Item> getVideoList(String name) {
		LOG.info("Inside Utility : getVideoList");

		List<Item> videoItems = new ArrayList<Item>();
		RssFeedServiceImpl service = new RssFeedServiceImpl();

//		URL url = null;

		String tit = null;
		String shortDesc = null;
		try {
			if (name.equalsIgnoreCase("videos")) {

				JSONObject json = readJsonFromUrl(CommonConstants.newsVideos);

				JSONArray array = json.getJSONArray("items");

				for (int i = 0; i < array.length(); i++) {
					if (array.getJSONObject(i).getString("name") != null) {
						tit = array.getJSONObject(i).getString("name")
								.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
								.replaceAll("&rsquo", "'")
								.replaceAll("&lsquo", "'")
								.replaceAll("&ldquo", "\"")
								.replaceAll("&rdquo", "\"")
								.replaceAll("&mdash", "--")
								.replaceAll("&ndash", "-").replaceAll(":", "");
					}

					String videoId = array.getJSONObject(i).getString("id");
					BigDecimal bd = new BigDecimal(videoId);

					if (!array.getJSONObject(i).getString("shortDescription")
							.equalsIgnoreCase("null")) {
						shortDesc = array.getJSONObject(i)
								.getString("shortDescription")
								.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
								.replaceAll("&rsquo", "'")
								.replaceAll("&lsquo", "'")
								.replaceAll("&ldquo", "\"")
								.replaceAll("&rdquo", "\"")
								.replaceAll("&mdash", "--")
								.replaceAll("&ndash", "-")
								.replaceAll("&39", "");

					} else {
						shortDesc = "";
					}

					// link="http://link.brightcove.com/services/player/bcpid3742235943001?bckey=AQ~~,AAACHKYdcFk~,IGxDpm7wjyHsFkDSEiehCuhZ0IVXD7vt"+"&bctid="+bd.longValue();
					final String link = getMP4RssFeedVideo(bd.longValue());

					Item item = new Item(tit, null, null, link, null, shortDesc, null);

					item.setId(i + 1);
					videoItems.add(item);
				}
			}

			service.processDatabaseOperation(videoItems, name,  CommonConstants.TylerHubCitiId);

		} catch (Exception e) {
			LOG.info("Inside Utility : getVideoList : " + e.getMessage());
			
			Item item = new Item(tit, null, null, null, null, null, e.getMessage());
			videoItems.add(item);
			
			try {
				service.processDatabaseOperation(videoItems, name,  CommonConstants.TylerHubCitiId);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getVideoList : " + e1.getMessage());
			}
			
		}

		LOG.info("Exit Utility : getVideoList ");
		return videoItems;
	}

	public static List<Item> getFeedList(String name) {
		LOG.info("Inside Utility : getFeedList ");

		List<Item> items = new ArrayList<Item>();
		RssFeedServiceImpl service = new RssFeedServiceImpl();

		URL url = null;
		URL url1 = null;
		int iCount = 0;
		
		
	
		
		try {

			if (name.equalsIgnoreCase("top")) {

				url = new URL(CommonConstants.top);

				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				dbf.setCoalescing(true);
				DocumentBuilder db = dbf.newDocumentBuilder();

				if (url.openStream().available() > 0) {

					Document doc = db.parse(new InputSource(url.openStream()));
					doc.getDocumentElement().normalize();
					NodeList nodeList = doc.getElementsByTagName("item");

					for (int i = 0; i < nodeList.getLength(); i++) {
						
						String lDescription = null;
						String image = null;
						String link = null;
						String date = null;
						String sDescription = null;
						String title=null;
					

						Node node = nodeList.item(i);
						Element fstElmnt = (Element) node;

						try {

							if (fstElmnt.getElementsByTagName("media:content") != null) {

								NodeList media = fstElmnt
										.getElementsByTagName("media:content");
								if (media.item(0) != null
										&& media.item(0).getAttributes() != null
										&& media.item(0).getAttributes()
												.getNamedItem("url") != null) {
									String mediaurl = media.item(0)
											.getAttributes()
											.getNamedItem("url").getNodeValue();

									image = mediaurl;
								}

							} else {
								image = null;
							}

						} catch (Exception e) {
							LOG.info("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
					
						try{
						if (fstElmnt.getElementsByTagName("link") != null) {
						NodeList linkList = fstElmnt
								.getElementsByTagName("link");
						Element linkElement = (Element) linkList.item(0);
						linkList = linkElement.getChildNodes();

						link = ((Node) linkList.item(0)).getNodeValue();
						}else{
							link=null;
						}
					}
					 catch (Exception e) {
							LOG.info("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try{
						if (fstElmnt.getElementsByTagName("title") != null) {
						NodeList titleList = fstElmnt
								.getElementsByTagName("title");
						Element titleElement = (Element) titleList.item(0);
						titleList = titleElement.getChildNodes();

						title = ((Node) titleList.item(0)).getNodeValue()
								.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
								.replaceAll("&rsquo", "'")
								.replaceAll("&lsquo", "'")
								.replaceAll("&ldquo", "\"")
								.replaceAll("&rdquo", "\"")
								.replaceAll("&mdash", "--")
								.replaceAll("&8211", "-")
								.replaceAll("&8216", "'") // left single
															// quotation mark
								.replaceAll("&8217", "'") // right single
															// quotation mark
								.replaceAll("&8230", "...") // horizontal
															// ellipsis
								.replaceAll("&ndash", "-").replaceAll(":", "");
						}
						else{
							title=null;
						}
					}
					 catch (Exception e) {
							LOG.info("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
                      try{
						if (fstElmnt.getElementsByTagName("pubDate") != null) {
						NodeList dateList = fstElmnt
								.getElementsByTagName("pubDate");
						Element dateElement = (Element) dateList.item(0);
						dateList = dateElement.getChildNodes();
						String datee = ((Node) dateList.item(0)).getNodeValue();

						String dates[] = datee.split(" ");
						date = dates[2] + " " + dates[1] + " " + " , "
								+ dates[3];
						}
						else{
							date=null;
						}
					}
					 catch (Exception e) {
							LOG.info("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try{
						if (fstElmnt.getElementsByTagName("media:text") != null) {
						NodeList websiteList = fstElmnt
								.getElementsByTagName("media:text");
						Element websiteElement = (Element) websiteList.item(0);
						websiteList = websiteElement.getChildNodes();

						if ( null != websiteList.item(0)) {
							if (websiteList.item(0).toString() != null) {

								if (((Node) websiteList.item(0)).getNodeValue()
										.length() > 75) {
									sDescription = ((Node) websiteList.item(0))
											.getNodeValue()
											.replaceAll("([^\\w\\s\'.,-:&\"<>])",
													"").replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'") // left single
													// quotation
													// mark
													.replaceAll("&8217", "'") // right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...") // horizontal
													// ellipsis
													.replaceAll("&39", "").substring(0, 75)
													+ "...";
								} else {

									sDescription = ((Node) websiteList.item(0))
											.getNodeValue()
											.replaceAll("([^\\w\\s\'.,-:&\"<>])",
													"").replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'") // left single
													// quotation
													// mark
													.replaceAll("&8217", "'") // right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...") // horizontal
													// ellipsis
													.replaceAll("&39", "")
													+ "...";
								}

								// lDescription = websiteList.item(0).toString();
								lDescription = websiteList.item(0).getNodeValue()
										.toString();

								if (null != lDescription
										&& !"".equals(lDescription)) {
									lDescription = lDescription.replace("[#text:",
											"");
									lDescription = lDescription
											.substring(0, lDescription.length() - 1)
											.trim()
											.replaceAll("([^\\w\\s\'.,-:&\"<>])",
													"").replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replace("&39", "'")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'")
													// left single quotation mark
													.replaceAll("&8217", "'")
													// right single quotation mark
													.replaceAll("&8230", "...")
													// horizontal ellipsis
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-");
								}
							}

								Item item = new Item(title, lDescription, image, link, date, sDescription, null);

								item.setId(iCount++);
								items.add(item);
							}

						} else {
							lDescription = "";
							sDescription = "";
						}
						
					}
					 catch (Exception e) {
							LOG.info("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
					}
				}

				url1 = new URL(CommonConstants.breaking);

				DocumentBuilderFactory dbf1 = DocumentBuilderFactory
						.newInstance();
				dbf1.setCoalescing(true);
				DocumentBuilder db1 = dbf1.newDocumentBuilder();

				if (url1.openStream().available() > 0) {

					Document doc = db1
							.parse(new InputSource(url1.openStream()));
					doc.getDocumentElement().normalize();
					NodeList nodeList = doc.getElementsByTagName("item");

					for (int i = 0; i < nodeList.getLength(); i++) {

String title=null;
						String lDescription = null;
						String image = null;
						String link = null;
						String date = null;
						String sDescription = null;

						Node node = nodeList.item(i);

						Element fstElmnt = (Element) node;

						try {

							if (fstElmnt.getElementsByTagName("media:content") != null) {

								NodeList media = fstElmnt
										.getElementsByTagName("media:content");
								if (media.item(0) != null
										&& media.item(0).getAttributes() != null
										&& media.item(0).getAttributes()
												.getNamedItem("url1") != null) {
									String mediaurl = media.item(0)
											.getAttributes()
											.getNamedItem("url").getNodeValue();

									image = mediaurl;
								}

							} else {
								image = null;
							}

						} catch (Exception e) {
							LOG.info("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try{
						
						if(fstElmnt
								.getElementsByTagName("link")!=null){

						NodeList linkList = fstElmnt
								.getElementsByTagName("link");
						Element linkElement = (Element) linkList.item(0);
						linkList = linkElement.getChildNodes();

						link = ((Node) linkList.item(0)).getNodeValue();
						}else{
							link=null;
						}
						}
						 catch (Exception e) {
								LOG.info("Inside Utility : getFeedList  : "
										+ e.getMessage());

							}
						try{
						
						if(fstElmnt
						.getElementsByTagName("title")!=null){

						NodeList titleList = fstElmnt
								.getElementsByTagName("title");
						Element titleElement = (Element) titleList.item(0);
						titleList = titleElement.getChildNodes();

						title = ((Node) titleList.item(0)).getNodeValue()
								.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
								.replaceAll("&rsquo", "'")
								.replaceAll("&lsquo", "'")
								.replaceAll("&ldquo", "\"")
								.replaceAll("&rdquo", "\"")
								.replaceAll("&mdash", "--")
								.replaceAll("&8211", "-")
								.replaceAll("&8216", "'") // left single
															// quotation mark
								.replaceAll("&8217", "'") // right single
															// quotation mark
								.replaceAll("&8230", "...") // horizontal
															// ellipsis
								.replaceAll("&ndash", "-").replaceAll(":", "");
						}else{
							title=null;
						}
						}
						 catch (Exception e) {
								LOG.info("Inside Utility : getFeedList  : "
										+ e.getMessage());

							}
						try{
						if(fstElmnt
								.getElementsByTagName("pubDate")!=null){
						NodeList dateList = fstElmnt
								.getElementsByTagName("pubDate");
						Element dateElement = (Element) dateList.item(0);
						dateList = dateElement.getChildNodes();
						String datee = ((Node) dateList.item(0)).getNodeValue();

						String dates[] = datee.split(" ");
						date = dates[2] + " " + dates[1] + " " + " , "
								+ dates[3];
						}
						else{
							date=null;
						}
						}
						 catch (Exception e) {
								LOG.info("Inside Utility : getFeedList  : "
										+ e.getMessage());

							}
						try{
						if(fstElmnt
								.getElementsByTagName("media:text")!=null){
						NodeList websiteList = fstElmnt
								.getElementsByTagName("media:text");
						Element websiteElement = (Element) websiteList.item(0);
						websiteList = websiteElement.getChildNodes();

						if ( null != websiteList.item(0)) {
							if (websiteList.item(0).toString() != null) {

								if (((Node) websiteList.item(0)).getNodeValue()
										.length() > 75) {
									sDescription = ((Node) websiteList.item(0))
											.getNodeValue()
											.replaceAll("([^\\w\\s\'.,-:&\"<>])",
													"").replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'") // left single
													// quotation
													// mark
													.replaceAll("&8217", "'") // right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...") // horizontal
													// ellipsis
													.replaceAll("&39", "").substring(0, 75)
													+ "...";
								} else {

									sDescription = ((Node) websiteList.item(0))
											.getNodeValue()
											.replaceAll("([^\\w\\s\'.,-:&\"<>])",
													"").replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'") // left single
													// quotation
													// mark
													.replaceAll("&8217", "'") // right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...") // horizontal
													// ellipsis
													.replaceAll("&39", "")
													+ "...";
								}

								// lDescription = websiteList.item(0).toString();
								lDescription = websiteList.item(0).getNodeValue().toString();

								if (null != lDescription && !"".equals(lDescription)) {
									lDescription = lDescription.replace("[#text:","");
									lDescription = lDescription
											.substring(0, lDescription.length() - 1)
											.trim()
											.replaceAll("([^\\w\\s\'.,-:&\"<>])",
													"").replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replace("&39", "'")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'")
													// left single quotation mark
													.replaceAll("&8217", "'")
													// right single quotation mark
													.replaceAll("&8230", "...")
													// horizontal ellipsis
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-");
								}

								Item item = new Item(title, lDescription, image, link, date, sDescription, null);

								item.setId(iCount++);
								items.add(item);
							}

						} else {
							lDescription = "";
							sDescription = "";
						}
						}
						}
						 catch (Exception e) {
								LOG.info("Inside Utility : getFeedList  : "
										+ e.getMessage());

							}
					}
				}

			}

			service.processDatabaseOperation(items, name,  CommonConstants.TylerHubCitiId);

		} catch (Exception e) {
			LOG.info("Inside Utility : getFeedList  : " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null, e.getMessage());
			items.add(item);
			try {
				service.processDatabaseOperation(items, name,  CommonConstants.TylerHubCitiId);
			} catch (RssFeedBatchProcessException e1) {
				LOG.info("Inside Utility : getFeedList  : " + e.getMessage());
			}
		}

		LOG.info("Exit Utility : getFeedList ");
		return items;
	}

	// Added for Rockwall

	public static List<Item> getRockwallList(String name) {
		LOG.info("Inside Utility : getRockwallList ");

		List<Item> items = new ArrayList<Item>();
		RssFeedServiceImpl service = new RssFeedServiceImpl();

		URL url = null;

		try {

			if (name.equalsIgnoreCase("good times")) {
				url = new URL(CommonConstants.goodevents);
			}

			else if (name.equalsIgnoreCase("good cause")) {
				url = new URL(CommonConstants.goodcause);
			}

			else if (name.equalsIgnoreCase("good living")) {
				url = new URL(CommonConstants.goodliving);
			}

			else if (name.equalsIgnoreCase("good business")) {
				url = new URL(CommonConstants.goodbusiness);
			}

			else if (name.equalsIgnoreCase("good people")) {
				url = new URL(CommonConstants.goodpeople);
			}

			else if (name.equalsIgnoreCase("good pets")) {
				url = new URL(CommonConstants.goodpets);
			}

			else if (name.equalsIgnoreCase("good faith")) {
				url = new URL(CommonConstants.goodfaith);
			}

			else if (name.equalsIgnoreCase("good thinking")) {
				url = new URL(CommonConstants.goodthinking);
			}

			else if (name.equalsIgnoreCase("good health")) {
				url = new URL(CommonConstants.goodneighbours);
			}

			else if (name.equalsIgnoreCase("good culture")) {
				url = new URL(CommonConstants.goodculture);
			}

			else if (name.equalsIgnoreCase("good sports")) {
				url = new URL(CommonConstants.goodsports);
			}

			else if (name.equalsIgnoreCase("columnists")) {
				url = new URL(CommonConstants.guestcolumns);
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					String title = null;
					String lDescription = null;
					String image = null;
					String link = null;
					String date = null;
					String sDescription = null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					try {

						if (fstElmnt.getElementsByTagName("media:content") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("media:content");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								//Adding changes
								int dot = mediaurl.lastIndexOf(".");
						    	int index = mediaurl.lastIndexOf("-");
						    	image = mediaurl.substring(0, index)+mediaurl.substring(dot);
								//image = mediaurl;
							}

						} else {
							image = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}

					try {

						if (fstElmnt.getElementsByTagName("link") != null) {

							NodeList linkList = fstElmnt
									.getElementsByTagName("link");
							Element linkElement = (Element) linkList.item(0);
							linkList = linkElement.getChildNodes();

							link = ((Node) linkList.item(0)).getNodeValue();

						} else {
							link = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}
					try {
						if (fstElmnt.getElementsByTagName("title") != null) {

							NodeList titleList = fstElmnt
									.getElementsByTagName("title");
							Element titleElement = (Element) titleList.item(0);
							titleList = titleElement.getChildNodes();

							title = ((Node) titleList.item(0)).getNodeValue()

							.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
									.replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'")
									.replaceAll("&ldquo", "\"")
									.replaceAll("&rdquo", "\"")
									.replaceAll("&mdash", "--")
									.replaceAll("&ndash", "-")
									.replaceAll("&8211", "-")
									.replaceAll("&8216", "'") // left single
																// quotation
																// mark
									.replaceAll("&8217", "'") // right single
																// quotation
																// mark
									.replaceAll("&8230", "...") // horizontal
																// ellipsis
									.replaceAll("&38", "&") // for ampersand.
									.replaceAll("&8220", "\"")// left double
																// quotation
																// mark
									.replaceAll("&8221", "\"") // right double
																// quotation
																// mark

									.replaceAll(":", "");

							//title = StringEscapeUtils.unescapeHtml(title);
						} else {
							title = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}

					try {
						if (fstElmnt.getElementsByTagName("pubDate") != null) {

							NodeList dateList = fstElmnt
									.getElementsByTagName("pubDate");
							Element dateElement = (Element) dateList.item(0);
							dateList = dateElement.getChildNodes();
							String datee = ((Node) dateList.item(0))
									.getNodeValue();
							// date=getDate(datee);
							String dates[] = datee.split(" ");
							date = dates[2] + " " + dates[1] + " " + " , "
									+ dates[3];

						} else {
							date = null;
						}

					} catch (Exception e) {
						LOG.info("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}

					NodeList websiteList = fstElmnt
							.getElementsByTagName("description");

					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					if (null != websiteList.item(0)) {

						if (websiteList.item(0).toString() != null) {
							if (((Node) websiteList.item(0)).getNodeValue()
									.length() > 75) {
								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&39", "")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'") // left single
																	// quotation
																	// mark
										.replaceAll("&8217", "'") // right
																	// single
																	// quotation
																	// mark
										.replaceAll("&8230", "...") // horizontal
																	// ellipsis
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"") // right
																	// double
																	// quotation
																	// mark
										.substring(0, 75)
										+ "...";
								//sDescription = StringEscapeUtils
									//	.unescapeHtml(sDescription);
							} else {

								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&39", "")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"") // right
																	// double
																	// quotation
																	// mark
										+ "...";
								//sDescription = StringEscapeUtils
								//	.unescapeHtml(sDescription);
							}
							lDescription = websiteList.item(0).toString();

							if (null != lDescription
									&& !"".equals(lDescription)) {
								lDescription = lDescription.replace("[#text:",
										"");
								lDescription = lDescription
										.substring(0, lDescription.length() - 1)
										.trim()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"")
										.replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replace("&39", "'")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\""); // right
																	// double
																	// quotation
																	// mark
								//lDescription = StringEscapeUtils
									//	.unescapeHtml(lDescription);
							}

							Item item = new Item(title, lDescription, image, link, date, sDescription, null);

							item.setId(i + 1);
							items.add(item);
						}
					}
				}

				service.processDatabaseOperation(items, name, CommonConstants.RockwallHubCitiId);
			}
			
		} catch (Exception e) {
			LOG.error("Inside Utility : getRockwallList :  " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null, e.getMessage());
			items.add(item);
		
			try {
				service.processDatabaseOperation(items, name, CommonConstants.RockwallHubCitiId);
			} catch (RssFeedBatchProcessException e1) {
				LOG.info("Inside Utility : getTylerList :  " + e.getMessage());
			}
		}

		LOG.info("Exit Utility : getRockwallList ");
		return items;

	}

	public static List<Item> getTylerList(String name) {
		LOG.info("Inside Utility : getTylerList ");

		List<Item> items = new ArrayList<Item>();
		RssFeedServiceImpl service = new RssFeedServiceImpl();
		
		
	
		
		URL url = null;
		

		try {

			if (name.equalsIgnoreCase("sports")) {
				url = new URL(CommonConstants.sports);
			} else if (name.equalsIgnoreCase("food")) {
				url = new URL(CommonConstants.food);
			} else if (name.equalsIgnoreCase("health")) {
				url = new URL(CommonConstants.health);
			} else if (name.equalsIgnoreCase("business")) {
				url = new URL(CommonConstants.business);
			} else if (name.equalsIgnoreCase("top")) {
				getFeedList("top");
			} else if (name.equalsIgnoreCase("life")) {
				url = new URL(CommonConstants.life);
			} else if (name.equalsIgnoreCase("opinion")) {
				url = new URL(CommonConstants.editorial);
			}

			else if (name.equalsIgnoreCase("videos")) {
				getVideoList("videos");
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {
					
					String title = null;
					String lDescription = null;
					String image = null;
					String link = null;
					String date = null;
					String sDescription = null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					try {

						if (fstElmnt.getElementsByTagName("media:content") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("media:content");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								image = mediaurl;
							}

						} else {
							image = null;
						}
						

					} catch (Exception e) {
						LOG.info("imageurl exception", image);
						e.printStackTrace();
					}
					
					try{

					if(fstElmnt.getElementsByTagName("link")!=null){
					NodeList linkList = fstElmnt.getElementsByTagName("link");
					Element linkElement = (Element) linkList.item(0);
					linkList = linkElement.getChildNodes();

					link = ((Node) linkList.item(0)).getNodeValue();

					NodeList titleList = fstElmnt.getElementsByTagName("title");
					Element titleElement = (Element) titleList.item(0);
					titleList = titleElement.getChildNodes();

					title = ((Node) titleList.item(0)).getNodeValue()
							.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
							.replaceAll("&rsquo", "'")
							.replaceAll("&lsquo", "'")
							.replaceAll("&ldquo", "\"")
							.replaceAll("&rdquo", "\"")
							.replaceAll("&mdash", "--")
							.replaceAll("&ndash", "-").replaceAll("&8211", "-")
							.replaceAll("&8216", "'") // left single quotation
														// mark
							.replaceAll("&8217", "'") // right single quotation
														// mark
							.replaceAll("&8230", "...") // horizontal ellipsis
							.replaceAll(":", "");
					}else{
						title=null;
					}
					
					} catch (Exception e) {
						LOG.info("Inside Utility : getTylerList  : "
								+ e.getMessage());

					}
					try{
					if(fstElmnt
							.getElementsByTagName("pubDate")!=null){

					NodeList dateList = fstElmnt
							.getElementsByTagName("pubDate");
					Element dateElement = (Element) dateList.item(0);
					dateList = dateElement.getChildNodes();
					String datee = ((Node) dateList.item(0)).getNodeValue();
					// date=getDate(datee);
					String dates[] = datee.split(" ");
					date = dates[2] + " " + dates[1] + " " + " , " + dates[3];
				}else{
					date=null;
				}
					} catch (Exception e) {
						LOG.info("Inside Utility : getTylerList  : "
								+ e.getMessage());

					}
					
					try{
						
					if(fstElmnt.getElementsByTagName("media:text")!=null){

					NodeList websiteList = fstElmnt
							.getElementsByTagName("media:text");
					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					

					if (null != websiteList.item(0)) {

						if (websiteList.item(0).toString() != null) {
							if (((Node) websiteList.item(0)).getNodeValue()
									.length() > 75) {
								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&8211", "-")
										.replaceAll("&8216", "'") // left single
																	// quotation
																	// mark
										.replaceAll("&8217", "'") // right
																	// single
																	// quotation
																	// mark
										.replaceAll("&8230", "...") // horizontal
																	// ellipsis
										.replaceAll("&39", "").substring(0, 75)
										+ "...";
							} else {

								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&8211", "-")
										.replaceAll("&8216", "'") // left single
																	// quotation
																	// mark
										.replaceAll("&8217", "'") // right
																	// single
																	// quotation
																	// mark
										.replaceAll("&8230", "...") // horizontal
																	// ellipsis
										.replaceAll("&39", "")
										+ "...";
							}
							lDescription = websiteList.item(0).toString();

							if (null != lDescription
									&& !"".equals(lDescription)) {
								lDescription = lDescription.replace("[#text:",
										"");
								lDescription = lDescription
										.substring(0, lDescription.length() - 1)
										.trim()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replace("&39", "'")
										.replaceAll("&8211", "-")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-");
							}

							Item item = new Item(title, lDescription, image, link, date, sDescription, null);
							item.setId(i + 1);
							items.add(item);
						}
						
					} else {
						lDescription = null;
						sDescription = null;
					}
					}
					} catch (Exception e) {
						LOG.info("Inside Utility : getTylerList  : "
								+ e.getMessage());

					}
					System.out.println("Title : "+title+","+"imageurl : "+image);
					
				}
			}

			service.processDatabaseOperation(items, name, CommonConstants.TylerHubCitiId);

		} catch (Exception e) {
			LOG.info("Inside Utility : getTylerList :  " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null, e.getMessage());
			items.add(item);
			try {
				service.processDatabaseOperation(items, name, CommonConstants.TylerHubCitiId);
			} catch (RssFeedBatchProcessException e1) {
				LOG.info("Inside Utility : getTylerList :  " + e.getMessage());
			}
		}
		
	

		LOG.info("Exit Utility : getTylerList ");
		return items;
	}

	public static String getDate(String s) {
		String currentDate = null;
		try {
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = df.parse(s);
			currentDate = df.format(date1);
		} catch (Exception e) {
			LOG.info("Exception in getDate method"
					+ e.getMessage());
		}
		return currentDate;

	}

	@SuppressWarnings("unchecked")
	public static Map<String, List<Item>> groupToList(List<Item> activityList) {

		Map<String, List<Item>> citiesByCountry = new HashMap<String, List<Item>>();

		/*
		 * Map<String,List<Item>> citiesByCountrySorted = new
		 * TreeMap<String,List<Item>>(Collections.reverseOrder());
		 */

		for (Item city : activityList) {

			@SuppressWarnings("rawtypes")
			List citiesForCountry = citiesByCountry.get(city.getDate());
			if (citiesForCountry == null) {
				citiesForCountry = new ArrayList<Item>();
				citiesByCountry.put(city.getDate(), citiesForCountry);

				// citiesByCountrySorted.putAll(citiesByCountry);
			}
			citiesForCountry.add(city);
		}
		return citiesByCountry;

		/*
		 * ArrayList<HashMap<String,Item>> list=new
		 * ArrayList<HashMap<String,Item>>(); for (Item a : activityList) {
		 * HashMap<String,Item> map = new HashMap<String,Item>(); String key =
		 * a.getDate(); Item group = map.get(key); if (group == null) { group =
		 * new Item(); map.put(key, group); list.add(map); }
		 */
	}

	/**
	 * This method is used to validate string is empty or null.
	 * 
	 * @param arg
	 *            as input parameter
	 * @return true if string is empty or null otherwise false.
	 */

	public static boolean isEmptyOrNullString(String arg) {
		final boolean emptyString;

		if (null == arg || "".equals(arg.trim())) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		return emptyString;
	}

	public static String stripNonValidXMLCharacters(String in) {
		StringBuilder out = new StringBuilder();
		char current;

		if (in == null || ("".equals(in)))
			return "";
		for (int i = 0; i < in.length(); i++) {
			current = in.charAt(i);
			if ((current == 0x9) || (current == 0xA) || (current == 0xD)
					|| ((current >= 0x20) && (current <= 0xD7FF))
					|| ((current >= 0xE000) && (current <= 0xFFFD))
					|| ((current >= 0x10000) && (current <= 0x10FFFF)))
				out.append(current);
		}
		return out.toString();
	}

	public static JSONObject readJsonFromUrl(String url) throws IOException,
			ParseException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json;

			json = new JSONObject(jsonText);

			return json;
		} finally {
			is.close();
		}
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static String formatDate(
			Map<String, List<Item>> citiesByCountrySorted) {

		String newstring = null;

		for (Map.Entry<String, List<Item>> entry : citiesByCountrySorted
				.entrySet()) {

			String key = entry.getKey();

			try {
				Date date = new SimpleDateFormat("dd/mmm YYYY").parse(key);
				newstring = new SimpleDateFormat("MM-dd-YYYY").format(date);
				// 2011-01-18
			} catch (ParseException e) {
				LOG.info("Exception in formatDate method" + e.getMessage());
			}
			
		}
		return newstring;
	}

	public static java.sql.Timestamp getFormattedDate()
			throws java.text.ParseException {

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		/*
		 * formatting the current date.
		 */
		// final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM");
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return sqltDate;
	}

	public static Date getFormattedCurrentDate() {

		final Date date = new Date();
		Date parsedUtilDate = null;
		try {
			final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
			parsedUtilDate = formater.parse(formater.format(date));
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method"
					+ exception.getMessage());
			// return ApplicationConstants.NOTAPPLICABLE;
		}

		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return parsedUtilDate;
	}

	public static String getCurrentDate() {
		String currentDate = null;
		try {
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = new Date();
			currentDate = df.format(date1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentDate;
	}

	public static String formattedDate(String enteredDate)
			throws java.text.ParseException {
		LOG.info("Inside the method formattedDate");
		String cDate = null;
		if (null != enteredDate && !"".equals(enteredDate)) {
			// dd-MMM-yyyy
			DateFormat oldFormatter = new SimpleDateFormat(
					"E MMM dd HH:mm:ss Z yyyy");
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			cDate = formatter.format(convertedDate);
		}
		LOG.info("Exit method formattedDate");
		return cDate;
	}

	/* public static void main(String[] args) { } */

/*	public static Item getValues(Item item) {
		Item items = null;
		if (item.getId() > 0) {
			items.setId(item.getId());
			items.setImage(item.getImage());
			items.setTitle(item.getTitle());
			items.setDescription(item.getDescription());
		}
		return items;
	}*/

	public static String getMP4RssFeedVideo(long videoId) {
		LOG.info("Inside method getMP4RssFeedVideo : " + videoId);

		String strVideo = null;
		// Fetch MP4 video by passing video id to below URL

		String url = "http://api.brightcove.com/services/library?command=find_video_by_id&video_fields=name,length,FLVURL&media_delivery=http&token=x95LXczyNI5-G9kX0cjsHM9edPFzaKFTE4PANJ7L2rQfuF-swGUxJg.."
				+ "&video_id=" + videoId;

		try {

			InputStream is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);

			strVideo = json.getString("FLVURL");
		} catch (Exception e) {

			LOG.error("Inside Utility : getMP4RssFeedVideo :  " + e.getMessage());
		}
		LOG.info("Exit Utility : getMP4RssFeedVideo : " + videoId);
		return strVideo;
	}
	
	
	
	public static String emailBody(List<Item> newsItemList) {
		LOG.info("Inside Utility : emailBody ");
				
		StringBuilder emailBody = new StringBuilder();
		emailBody.append("Hi,");
		emailBody.append("</br></br>");

		if ( null != newsItemList  &&  ! newsItemList.isEmpty()) {
			emailBody.append(PropertiesReader.getPropertyValue(ApplicationConstants.MAIL_CONTENT_FIRST_LINE));
			emailBody.append("</br></br>");
			emailBody.append("For the Time Stamp  &nbsp;<b>: &nbsp;&nbsp;" + Utility.getCurrentDateandTime() + "</b>.\n");
			emailBody.append("</br></br>");
			emailBody.append("<table cellspacing='0' cellpadding='0' border='1'><tr bgcolor='#FFFF99'><th>&nbsp;Sl No.&nbsp;</th><th>&nbsp;HubCiti(s)&nbsp;</th><th>&nbsp;NewsType(s)&nbsp;</th><th>&nbsp;Reason&nbsp;</th>");
			emailBody.append("</tr>");

			for (int i = 0; i < newsItemList.size(); i++) {
				Item objItem = newsItemList.get(i);
				
				emailBody.append("<tr>");
				
				emailBody.append("<td align=\"center\">");
				emailBody.append(objItem.getId());
				emailBody.append("</td>");
				
				emailBody.append("<td align=\"center\">");
				emailBody.append(objItem.getHubCitiName());
				emailBody.append("</td>");
				
				emailBody.append("<td align=\"center\">");
				emailBody.append(objItem.getFeedType());
				emailBody.append("</td>");
				
				emailBody.append("<td align=\"left\">");
				emailBody.append(objItem.getMessage());
				emailBody.append("</td>");
				
				emailBody.append("</tr>");
			}
			emailBody.append("</table>");
		} /*else {

			emailBody.append("News  are not available for the date  " + Utility.getCurrentDateandTime() + ".\n");
			emailBody.append("</br></br>");
			
		}*/

		emailBody.append("</br></br>");
		emailBody.append("Regards,</br>");
//		emailBody.append("ScanSee Team");
		emailBody.append("HubCiti Team");

		LOG.info("Inside Utility : emailBody : " + emailBody.toString()); 
		return emailBody.toString();
	}
	
	
	/**
	 * @return Current Date & Time (Jul 24, 2015 4:00 AM.)
	 */
	public static String getCurrentDateandTime() {
		// Make a new Date object. It will be initialized to the current time.
        Date now = new Date();
		String currentDate = null;
		try {
			
			currentDate = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(now);
			
		} catch (Exception e) {
			LOG.error("Inside Utility :  getCurrentDateandTime : " + e.getMessage());
		}
		return currentDate;
	}

}
